#!/usr/bin/env python

from qiskit.providers.jobstatus import JobStatus
from rmfim.helper import get_backend
import glob, numpy


def driver():
    backend = get_backend()
    for fname in glob.glob('*dat'):
        print(f'{fname}:')
        jobids = numpy.loadtxt(fname, dtype=str)
        for jid in jobids:
            job = backend.retrieve_job(jid)
            if job.status() != JobStatus.ERROR and \
                    job.status() != JobStatus.CANCELLED :
                continue

            print(f'{fname} failed job {jid}')


if __name__ == "__main__":
    driver()
