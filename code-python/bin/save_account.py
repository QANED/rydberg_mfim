#!/usr/bin/env python3
from qiskit import IBMQ

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-t", "--token", type=str, default=None,
        help="token for your IBM qc account.")
args = parser.parse_args()

IBMQ.save_account(args.token, overwrite=True)
