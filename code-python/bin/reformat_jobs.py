#!/usr/bin/env python
import pickle


def reformat_jobs():
    with open('jobs_database.pkl', 'rb') as f:
        jobs = pickle.load(f)

    from qiskit import IBMQ
    IBMQ.load_account()

    jobs = {job.job_id(): job.result() for job in jobs}
    with open('jobs_database.pkl', 'wb') as f:
        pickle.dump(jobs, f)



if __name__ == "__main__":
    reformat_jobs()
