#!/usr/bin/env python
# coding: utf-8

import qiskit, pickle
from qiskit.ignis.verification import randomized_benchmarking as rb
from qiskit import execute, QuantumCircuit, transpile

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from rmfim.emlib import Rzz_gate_schedule
from rmfim.helper import get_backend, get_qubits_list
from rmfim import conf
from rmfim.database import jobs



def random_benchmark2q_jobs(
        q_conn,
        length_vector,  # len(length_vector) gives # of circuits for each seed
        backend,
        nseeds=5,
        shots=8192,
        basis_gates = ['u1', 'u2', 'u3', 'cx'],
        with_calib=False,
        dononinterleaved=True,  # perform interleaved jobs or not
        ):
    '''random benchmarking cnot ot pulse zz gate.
    '''
    jobs_list = [[], []]
    jobfiles = ['jobs_rb.dat']
    if with_calib:
        jobfiles.append('jobs_rbpulse.dat')
    else:
        jobfiles.append('jobs_rbcnot.dat')
    for selected_pair in q_conn:
        print(f'selected_pair: {selected_pair}')
        rb_pattern = [selected_pair]
        qc = QuantumCircuit(2)
        qc.id(0)
        interleaved_elem = [qc]

        rb_circs, xdata, circuits_interleaved = rb.randomized_benchmarking_seq(
                length_vector=length_vector,
                nseeds=nseeds,
                rb_pattern=rb_pattern,
                interleaved_elem=interleaved_elem)

        # specify interleaved elements
        qc_seeds = []
        for h in range(len(circuits_interleaved)):
            qc_tests = []
            for j in range(len(circuits_interleaved[h])):
                qc_test = QuantumCircuit(
                        *circuits_interleaved[h][j].qregs,
                        *circuits_interleaved[h][j].cregs)
                qc_test.name = circuits_interleaved[h][j].name
                for i in circuits_interleaved[h][j][:]:
                    if type(i[0]) is qiskit.circuit.library.standard_gates.i.IGate:
                        qc_test.rzz(-2.0, selected_pair[0], selected_pair[1])
                        qc_test.barrier()
                        qc_test.rzz(2.0, selected_pair[0], selected_pair[1])
                    else:
                        qc_test.data.append(i)
                if with_calib:
                    qc_test.add_calibration('rzz',
                            selected_pair,
                            Rzz_gate_schedule(selected_pair[0],
                            selected_pair[1],-2.0, backend),
                            [-2.0])
                    qc_test.add_calibration('rzz',
                            selected_pair,
                            Rzz_gate_schedule(selected_pair[0],
                            selected_pair[1],2.0, backend),
                            [2.0])
                qc_tests.append(qc_test)
            qc_seeds.append(qc_tests)

        if dononinterleaved:
            print('non-interleaved rb')
            # Execute the non-interleaved RB circuits as a reference
            for rb_seed, rb_circ_seed in enumerate(rb_circs):
                print('Compiling seed %d' % rb_seed)
                rb_circ_transpile = transpile(rb_circ_seed, basis_gates=basis_gates)
                print('QC seed %d' % rb_seed)
                job = execute(rb_circ_transpile, shots=shots, backend=backend)
                print(job.job_id())
                jobs_list[0].append(job)

        print('interleaved rb')
        for rb_seed, rb_circ_seed in enumerate(qc_seeds):
            print('Compiling seed %d' % rb_seed)
            rb_circ_transpile = transpile(rb_circ_seed, basis_gates=basis_gates)
            print('QC seed %d' % rb_seed)
            job = execute(rb_circ_transpile, shots=shots, backend=backend)
            print(job.job_id())
            jobs_list[1].append(job)

    # save job id's
    for i, jobs in enumerate(jobs_list):
        if len(jobs) > 0:
            ids = [job.job_id() for job in jobs]
            np.savetxt(jobfiles[i], ids, fmt='%s')


def random_benchmark2q_pp(
        q_conn,
        length_vector,  # len(length_vector) gives # of circuits for each seed
        backend,
        nseeds=5,
        mode=0,         # -1: cnot error rate ana;ysis; other: pulse zz gate analysis.
        ):
    from qiskit.ignis.verification import InterleavedRBFitter
    '''post-processing.
    '''
    # jobs_rb.dat provides reference data
    jobfiles = ['jobs_rb.dat']
    if mode == -1:
        label = 'rbcnot'
    else:
        label = 'rbpulse'

    jobfiles.append(f'jobs_{label}.dat')
    print(f'job files: {jobfiles}')

    ids_list = []
    # jobs_rb.dat provides reference data
    for fn in jobfiles:
         ids_list.append(np.loadtxt(fn, dtype=str))

    results_list = []
    for i, ids in enumerate(ids_list):
        print(f'jobs set {i} dim: {len(ids)}')
        results = []
        for j, s in enumerate(ids):
            print(f'adding result {i} {j}/{len(ids)}')
            results.append(jobs.get_result_by_id(s))
        results_list.append(results)
    print('results done.')

    target_errs = []
    with PdfPages(f'{label}.pdf') as pdf:
        for i, q_pair in enumerate(q_conn):
            rb_pattern = [q_pair]
            for j, results in enumerate(results_list):
                plt.figure(figsize=(16, 12))
                for k, result in enumerate(results[i*nseeds:(i+1)*nseeds]):
                    if k == 0:
                        rbfit = rb.fitters.RBFitter(result,
                                [length_vector],
                                rb_pattern)
                    else:
                        rbfit.add_data([result])
                    ax = plt.subplot(3, 2, k+1)
                    rbfit.plot_rb_data(0, ax=ax, add_label=True,
                            show_plt=False)
                    ax.set_title(f'after seed {k}')
                ax = plt.subplot(3, 2, 6)
                ax.set_title(f"pair {q_pair} proctol {j}")
                plt.tight_layout()
                pdf.savefig()
                plt.close()
            rb_fit_interleaved = InterleavedRBFitter(
                    results_list[0][i*nseeds:(i+1)*nseeds],
                    results_list[1][i*nseeds:(i+1)*nseeds],
                    [length_vector],
                    rb_pattern,
                    )
            plt.figure(figsize=(16, 12))
            ax = plt.subplot(1, 1, 1)
            rb_fit_interleaved.plot_rb_data(ax=ax, add_label=True,
                    show_plt=False)
            pdf.savefig()
            plt.close()
            target_errs.append(
                    rb_fit_interleaved._fit_interleaved[0]['epc_est']/2)

    print(f'{label[2:]} gate errors')
    for qq, err in zip(q_conn, target_errs):
        print(f'pair: {qq}, error: {err:.5f}')
    print(f'mean error: {np.mean(target_errs):.10f}')


def driver():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--mode", type=int, default=0,
            help="mode. +/-1: rb for cnot; +/-2: rb for pulse zz." +\
            " psoitive: submit job; negative: analysis. dflt: 1.")
    parser.add_argument("--skiprb", action="store_false",
            help="skip the reference random benchmark.")
    args = parser.parse_args()

    backend = get_backend()
    qubits_list = get_qubits_list()
    if 'pairs' in conf.data:
        q_conn = conf.data['pairs']
    else:
        q_conn = []
        for i in range(len(qubits_list)-1):
            q_conn.append([qubits_list[i], qubits_list[i+1]])
    print(f'pairs: {q_conn}')
    length_vector = np.arange(1, 200, 20)

    if args.mode > 0:
        if args.mode == 1:
            with_calib = False
        else:
            with_calib = True

        random_benchmark2q_jobs(q_conn,
                length_vector,
                backend,
                dononinterleaved=args.skiprb,
                with_calib=with_calib,
                )
    else:
        random_benchmark2q_pp(q_conn,
                length_vector,
                backend,
                mode=args.mode,
                )


if __name__ == "__main__":
    driver()
