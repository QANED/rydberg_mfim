#!/usr/bin/env python
# coding: utf-8

from mitiq import zne
import warnings
from qiskit.pulse.library import GaussianSquare
from qiskit.pulse import Play, DriveChannel, ControlChannel, ShiftPhase
from qiskit import pulse
from qiskit.compiler import transpile, schedule
from scipy.special import erf
import math
from qiskit import QuantumCircuit, QuantumRegister
import random
import mitiq
import itertools
import qiskit
import numpy as np
warnings.filterwarnings('ignore')
from rmfim.helper import get_qubits_list, get_scale_factors, get_flag_dd


def cx_pulse_instructions(qc: int, qt: int, backend):
    
    """
    Reference qisjit textbook 
    
    Retrieve the CNOT pulse schedule for the given
    qubit pair from the backend defaults.

    Args:
      qc: control qubit index
      qt: target qubit index
    """
    backend_config = backend.configuration()
    inst_sched_map = backend.defaults().instruction_schedule_map

    if [qc, qt] not in backend_config.coupling_map:
        print('Qubit pair has no direct cross resonance!')
    else:
        cx = inst_sched_map.get('cx', qubits=[qc, qt])
        return cx


def Extract_GaussianSquare(q1, q2, backend):
    """Retrieve the Gaussiacross resonance pulse waveform
    for the qubit pair from the cx (CNOT) schedule.

    Args:
      q1: control qubit index
      q2: target qubit index
    """
    cx = cx_pulse_instructions(q1, q2, backend)
    cx1 = cx_pulse_instructions(q2, q1, backend)

    # get longer cnot gate pulse schedule
    if cx.instructions[-1][0] < cx1.instructions[-1][0]:
        cx = cx1
    Dr_gs = []  # get the gaussiansquare pulse of drive channel
    cr_gs = []  # get the gaussiansquare pulse of control channel
    control_channel = ''  # get the corresponding control channel for q1 and q2
    Drive_Channel = ''    # get the corresponding drive channel with gaussiansquare pulse
    another_Drive_Channel = ''  # get another drive channel

    # look for them

    N = 1
    for i in range(len(cx.instructions)):
        if type(cx.instructions[i][1]) is ShiftPhase and type(cx.instructions[i][1].channel) is DriveChannel and cx.instructions[i][1].phase == -math.pi/2 and N == 1:

            another_Drive_Channel = cx.instructions[i][1].channels[0]

        if type(cx.instructions[i][1].channels[0]) is ControlChannel and type(cx.instructions[i][1]) is Play and type(cx.instructions[i][1].pulse) is GaussianSquare:

            cr_gs.append(cx.instructions[i][1].pulse)

            control_channel = cx.instructions[i][1].channels[0]

        if type(cx.instructions[i][1].channels[0]) is DriveChannel and type(cx.instructions[i][1]) is Play and type(cx.instructions[i][1].pulse) is GaussianSquare:

            Dr_gs.append(cx.instructions[i][1].pulse)

            Drive_Channel = cx.instructions[i][1].channels[0]
        if type(cx.instructions[i][1]) is Play:
            N = 0

    return control_channel, cr_gs, Drive_Channel, Dr_gs, another_Drive_Channel


# When driving channel is phase shifted, the corresponding control channel need to be phase shifted.
def Get_Shift_phase_CRTL_Chan(q1, q2, backend):
    """Get the driving channels' corresponding control channels

    Args:
      q1: qubit
      q2: qubit
    out_put:
      control_chan1: the corresponding control channel of driving channel with
    """
    cx1 = cx_pulse_instructions(q1, q2, backend)
    cx2 = cx_pulse_instructions(q2, q1, backend)
    if cx1.instructions[-1][0] > cx2.instructions[-1][0]:
        cx1, cx2 = cx2, cx1
    control_chan0 = []
    control_chan1 = []
    for i in range(len(cx2.instructions)):
        if type(cx2.instructions[i][1]) is ShiftPhase and type(cx2.instructions[i][1].channel) is ControlChannel and cx2.instructions[i][1].phase == -math.pi/2:
            control_chan0.append(cx2.instructions[i][1].channel)
        if type(cx2.instructions[i][1]) is ShiftPhase and type(cx2.instructions[i][1].channel) is ControlChannel and cx2.instructions[i][1].phase == -math.pi:
            control_chan1.append(cx2.instructions[i][1].channel)
        if type(cx2.instructions[i][1]) is Play:
            break

    return control_chan0, control_chan1


def Rzz_gate_schedule(q0, q1, theta, backend):
    """
    Referenced the paper John P. T. Stenger, Nicholas T. Bronn, Daniel J. Egger, and David Pekker
    Phys. Rev. Research 3, 033171 
    
    Args:
      q1: qubit
      q2: qubit
      theta: rotating angle
      backend: quantum hardware Device
      
    out_put:
        
      Return Rzz Pulse
    """
    
    uchan, cr_pulse, Dchan, dr_pulse, another_Dchan = Extract_GaussianSquare(
        q0, q1, backend)
    
    control_chan0, control_chan1 = Get_Shift_phase_CRTL_Chan(q0, q1, backend)

    Frac = 2*np.abs(theta)/math.pi

    Y_q = Dchan.index

    X_q = another_Dchan.index

    inst_sched_map = backend.defaults().instruction_schedule_map
    Y90p = inst_sched_map.get('u2', P0=0, P1=0, qubits=[Y_q]).instructions
    X_180 = inst_sched_map.get('x', qubits=[X_q]).instructions[0][1].pulse

    # find out Y_90 pulse

    for Y in Y90p:

        if type(Y[1]) is Play:

            Y_pulse = Y[1].pulse

    ###

    drive_samples = Y_pulse.duration  # The duration of Y pulse

    cr_samples = cr_pulse[0].duration  # The duration gaussiansquare pulse

    cr_width = cr_pulse[0].width  # gaussiansquare pulse's width

    cr_sigma = cr_pulse[0].sigma  # gaussiansquare pulse's standard error

    cr_amp = np.abs(cr_pulse[0].amp)

    number_std = (cr_samples-cr_width)/cr_sigma

    Area_g = cr_amp*cr_sigma*np.sqrt(2*np.pi)*erf(number_std) 

    Area_pi_2 = cr_width*cr_amp+Area_g

    dr_sigma = dr_pulse[0].sigma

    Area_theta = Frac * Area_pi_2

    if Area_theta > Area_g:

        New_width = (Area_theta-Area_g)/cr_amp

        new_duration = math.ceil((New_width+number_std*cr_sigma)/16)*16

        cr_pulse[0] = GaussianSquare(
            duration=new_duration, amp=cr_pulse[0].amp, sigma=cr_sigma, width=New_width)

        cr_pulse[1] = GaussianSquare(
            duration=new_duration, amp=-cr_pulse[0].amp, sigma=cr_sigma, width=New_width)

        dr_pulse[0] = GaussianSquare(
            duration=new_duration, amp=dr_pulse[0].amp, sigma=dr_sigma, width=New_width)

        dr_pulse[1] = GaussianSquare(
            duration=new_duration, amp=-dr_pulse[0].amp, sigma=dr_sigma, width=New_width)

    else:

        New_amp_cr =  cr_pulse[0].amp*Area_theta/Area_g
        
        New_amp_dr= dr_pulse[0].amp*Area_theta/Area_g
        
        new_duration = number_std * cr_sigma
        
        cr_pulse[0] = GaussianSquare(duration=int(new_duration), amp=New_amp_cr, sigma=cr_sigma, width=0)
        
        cr_pulse[1] = GaussianSquare(duration=int(new_duration), amp=-New_amp_cr, sigma=cr_sigma, width=0)
        
        dr_pulse[0] = GaussianSquare(duration=int(new_duration), amp=New_amp_dr, sigma=dr_sigma, width=0)
        
        dr_pulse[1] = GaussianSquare(duration=int(new_duration), amp=-New_amp_dr, sigma=dr_sigma, width=0)

    # Set up the Rzz schedule
    if theta < 0:

        RZZ_schedule = pulse.Schedule(name="RZZ gate pulse")

        # Y_-90 pulse

        RZZ_schedule |= ShiftPhase(-math.pi, Dchan)

        for chan in control_chan1:

            RZZ_schedule |= ShiftPhase(-math.pi, chan)

        RZZ_schedule |= Play(Y_pulse, Dchan)

        RZZ_schedule |= ShiftPhase(-math.pi, Dchan) << int(drive_samples)

        for chan in control_chan1:

            RZZ_schedule |= ShiftPhase(-math.pi, chan) << int(drive_samples)

         ###

         # Cross resonant pulses and X rotation echo pulse

        RZZ_schedule |= Play(dr_pulse[0], Dchan) << int(drive_samples)
        RZZ_schedule |= Play(cr_pulse[0], uchan) << int(drive_samples)

        RZZ_schedule |= Play(X_180, another_Dchan) << int(
            new_duration+drive_samples)

        RZZ_schedule |= Play(dr_pulse[1], Dchan) << int(
            new_duration+2*drive_samples)
        RZZ_schedule |= Play(cr_pulse[1], uchan) << int(
            new_duration+2*drive_samples)

        ###

        # X_180 pulse

        RZZ_schedule |= Play(X_180, another_Dchan) << int(
            2*new_duration+2*drive_samples)

        # Y_90 pulse

        RZZ_schedule |= Play(Y_pulse, Dchan) << int(
            2*new_duration+2*drive_samples)

        return RZZ_schedule

    else:

        RZZ_schedule = pulse.Schedule(name="RZZ gate pulse")

        # Y_90 pulse

        RZZ_schedule |= Play(Y_pulse, Dchan)

        ###

        # Cross resonant pulses and X rotation echo pulse

        RZZ_schedule |= Play(dr_pulse[0], Dchan) << int(drive_samples)
        RZZ_schedule |= Play(cr_pulse[0], uchan) << int(drive_samples)

        RZZ_schedule |= Play(X_180, another_Dchan) << int(
            new_duration+drive_samples)

        RZZ_schedule |= Play(dr_pulse[1], Dchan) << int(
            new_duration+2*drive_samples)
        RZZ_schedule |= Play(cr_pulse[1], uchan) << int(
            new_duration+2*drive_samples)

        ###

        # X_180 pulse

        RZZ_schedule |= Play(X_180, another_Dchan) << int(
            2*new_duration+2*drive_samples)

        # Y_-90 pulse

        RZZ_schedule |= ShiftPhase(-math.pi,
                                   Dchan) << int(2*new_duration+2*drive_samples)

        for chan in control_chan1:

            RZZ_schedule |= ShiftPhase(-math.pi,
                                       chan) << int(2*new_duration+2*drive_samples)

        RZZ_schedule |= Play(Y_pulse, Dchan) << int(
            2*new_duration+2*drive_samples)

        RZZ_schedule |= ShiftPhase(-math.pi,
                                   Dchan) << int(2*new_duration+3*drive_samples)

        for chan in control_chan1:

            RZZ_schedule |= ShiftPhase(-math.pi,
                                       chan) << int(2*new_duration+3*drive_samples)

        return RZZ_schedule


def post_selection(bit_str):
    """
    This function takes in a bitstring, and will return False iff
    there is a 1 next to another 1 in the given bitstring.
    This does NOT consider a bitstring with 1s on either end as next to one another,
    i.e. given a string of the form '10...01', this function should return False
    """
    new_str = '0' + bit_str[0:len(bit_str)]
    for i in range(0, len(bit_str)+1):
        if (new_str[i] == '1') & (new_str[i] == new_str[i-1]):
            return False
    return True


def post_selected_expval(counts, qubit):
    """
    This function takes in the counts from a circuit execution, as well as a qubit index,
    and outputs <Z> for that qubit after post-selection
    """
    copy_counts = counts.copy()

    total = []

    for x in counts.keys():
        if not post_selection(x):
            copy_counts.pop(x)

    for x in copy_counts:
        total.append(copy_counts[x])
    total = sum(total)

    one_counts = 0
    for x in copy_counts.keys():
        if x[qubit] == '1':
            one_counts += copy_counts[x]

    if total == 0:
        return -1

    prob = one_counts/total
    expval = 2*prob - 1

    return expval


# Define the functions used to build the Trotter circuit.
def h_Z(qc, h, dt, qubits_list, cond):
    """
    Define Z feild 's time evolving unitary.
    
    qubits_list: can qubit list or integer.
    
    dt: time step
    
    h: Z field strength.
        
    qc: qiskit circuit
    
    """
    
    if cond == 'per':
        if type(qubits_list) == int:
            num = qubits_list
            for i in range(num):
                qc.rz(2*h*dt, i)
        # j=0
        else:
            for i in qubits_list:
                qc.rz(2*h*dt, i)
                
    if cond == 'op':
        if type(qubits_list) == int:
            num = qubits_list
            for i in range(num):
                if i == 0 or i == (num-1):
                    qc.rz(h*dt, i)
                else:
                    qc.rz(2*h*dt, i)
        # j=0
        else:
            for i in qubits_list:
                if i == qubits_list[0] or i == qubits_list[-1]:
                    qc.rz(h*dt, i)
                else:
                    qc.rz(2*h*dt, i)


def h_Z_c(qc, h, dt, qubits_list, cond):
    num = len(qubits_list)
    if cond == 'per':
        for i in range(num):
            qc.rz(2*h*dt, i)

    if cond == 'op':
        for i in range(num):
            if i == 0 or i == (num-1):
                qc.rz(h*dt, i)
            else:
                qc.rz(2*h*dt, i)


def X_Rabi(qc, Omega, dt, qubits_list):  # Rabi coupling
    if type(qubits_list) == int:
        num = qubits_list
        for i in range(num):
            qc.rx(2*Omega*dt, i)
    else:
        for i in qubits_list:
            qc.rx(2*Omega*dt, i)


def X_Rabi_c(qc, Omega, dt, qubits_list):  # Rabi coupling
    num = len(qubits_list)
    for i in range(num):
        qc.rx(2*Omega*dt, i)


def ZZ(qc, V, dt, qubits_list, cond):  # nearest neighbor coupling
    if type(qubits_list) == int:
        num = qubits_list
        for i in range(1, num-1, 2):
            qc.rzz(-2*V*dt, i, i+1)
        if cond == 'per':
            qc.rzz(-2*V*dt, 0, num-1)
        for i in range(0, num-1, 2):
            qc.rzz(-2*V*dt, i, i+1)
    else:
        for i, j in enumerate(qubits_list[1:-1:2]):
            qc.rzz(-2*V*dt, j, qubits_list[2*i+2])
        if cond == 'per':
            qc.rzz(-2*V*dt, qubits_list[0], qubits_list[-1])
        for i, j in enumerate(qubits_list[:-1:2]):
            qc.rzz(-2*V*dt, j, qubits_list[2*i+1])


# # just Rzx-based Rzz Gate trotter circuits
def Trotter_circuit_5_site_Rzz(h, Omega, V, T, n, qubits_list, cond, backend):
    Trotter_circuits = []
    dt = T/n
    backend_config = backend.configuration()
    for i in range(n):
        Trotter_circuit = QuantumCircuit(
                backend_config.n_qubits, len(qubits_list))

        # initial state
        for j in qubits_list[1::2]:
            Trotter_circuit.x(j)
        Trotter_circuit.barrier()

        for k in range(i):
            # Rabi coupling from the second term of Hamiltonian
            X_Rabi(Trotter_circuit, Omega, dt, qubits_list)
            h_Z(Trotter_circuit, h, dt, qubits_list, cond)  # Z field
            ZZ(Trotter_circuit, V, dt, qubits_list, cond)
            # Nearest neighbor hopping term
            Trotter_circuit.barrier()

        j = 0
        for a in qubits_list:
            Trotter_circuit.measure(a, j)
            j += 1

        for a, b in enumerate(qubits_list[1:-1:2]):
            Trotter_circuit.add_calibration(
                    'rzz', [b, qubits_list[2*a+2]],
                    Rzz_gate_schedule(b, qubits_list[2*a+2], -2*V*dt, backend),
                    [-2*V*dt])

        if cond == 'per':
            Trotter_circuit.add_calibration('rzz',
                    [qubits_list[0], qubits_list[-1]],
                    Rzz_gate_schedule(qubits_list[0],
                    qubits_list[-1], -2*V*dt, backend),
                    [-2*V*dt])

        for a, b in enumerate(qubits_list[:-1:2]):
            Trotter_circuit.add_calibration(
                    'rzz', [b, qubits_list[2*a+1]],
                    Rzz_gate_schedule(b, qubits_list[2*a+1], -2*V*dt, backend),
                    [-2*V*dt])

        Trotter_circuit = transpile(Trotter_circuit, backend)
        Trotter_circuits.append(Trotter_circuit)

    return Trotter_circuits


### Twirled RZZ Randomized Folding

def apply_pauli(circ, num, qb):
    if (num == 0):
        circ.i(qb)
    elif (num == 1):
        circ.x(qb)
    elif (num == 2):
        circ.y(qb)
    else:
        circ.z(qb)
    return circ


def Twirled_for_cnot(circ, qb0, qb1):
    """
    Pauli Twirling for Rzz gate. However, in order to get random Rzz gate folding in mitiq,
    we replace Rzz with Cnot gate first.
    
    """
    M = np.ones((4, 4))

    M[0, 1] = -1
    M[0, 2] = -1
    M[1, 0] = -1
    M[1, 3] = -1
    M[2, 0] = -1
    M[2, 3] = -1
    M[3, 1] = -1
    M[3, 2] = -1

    paulis = [(i, j) for i in range(0, 4) for j in range(0, 4)]
    paulis.remove((0, 0))
    paulis_map = [(i, j) for i in range(0, 4) for j in range(0, 4)]
    paulis_map.remove((0, 0))
    num = random.randrange(len(paulis))

    apply_pauli(circ, paulis[num][0], qb0)
    apply_pauli(circ, paulis[num][1], qb1)

    # angle=M[paulis[num][0],paulis[num][1]]*angle

    #circ.rzz(angle, qb0, qb1)
    circ.cx(qb0, qb1)

    apply_pauli(circ, paulis_map[num][0], qb0)
    apply_pauli(circ, paulis_map[num][1], qb1)


def C_not(qc, V, dt, qubits_list, cond):  # nearest neighbor coupling
    """
    Two qubit gate with Rzz's Pauli Twurling.
    
    Used for Trotter_circuit_5_site_c function.
    """
    num = len(qubits_list)
    for i in range(1, num-1, 2):
        Twirled_for_cnot(qc, i, i+1)
    if cond == 'per':
        Twirled_for_cnot(qc, 0, num-1)
    for i in range(0, num-1, 2):
        Twirled_for_cnot(qc, i, i+1)


# Define the functions used to build the Trotter circuit.
def Trotter_circuit_5_site_c(h, Omega, V,
        dt, nstart, nend,
        qubits_list, cond,
        ):
    """ 
    In order to get random Rzz gate folding in mitiq, we replace Rzz with Cnot gate first.
    
    Note: (2021 Oct - 2022 Jan ) Mitiq's random gate folding didn't work with Rzz, Rzx, Rxx and so on. 
    Maybe it can work with Rzz, Rzx, Rxx now. 
    
    """
    q_len = len(qubits_list)
    Trotter_circuits = []

    for i in range(nstart, nend):
        Trotter_circuit = QuantumCircuit(q_len, q_len)

        # Initial state setting: Neel state
        for j in range(1, q_len, 2):
            Trotter_circuit.x(j)

        Trotter_circuit.barrier()

        for k in range(i):

            # Rabi coupling from the second term of Hamiltonian
            X_Rabi_c(Trotter_circuit, Omega, dt, qubits_list)
            h_Z_c(Trotter_circuit, h, dt, qubits_list, cond)  # Z field
            # Trotter_circuit.barrier()

            # Nearest neighbor hopping term
            C_not(Trotter_circuit, V, dt, qubits_list, cond)
            Trotter_circuit.barrier()

        for i in range(q_len):
            Trotter_circuit.measure(i, i)
        Trotter_circuits.append(Trotter_circuit)

    return Trotter_circuits


def TrotterExecutor_random_rzz(backend,
        Single_err,
        Ave_Rzz_err,
        dt=1,         # time step size
        nstart=0,      # [starting step, ending step)
        nend=20,
        Omega=0.2*1.2,
        h=-2,
        V=-1,
        qubits_list=get_qubits_list(),
        cond='op',
        n_Times=1,
        scale_factors=get_scale_factors(),
        do_dd=get_flag_dd(),   # dynamical decoupling
        shots=8192,
        jmode=0,   # job mode. 0: analysis only; 1: submit job
        ):
    ## To run random gate folding trotter circuit with Rzx-based Rzz gate and Pauli Twirling.
    
    print(f'dt: {dt:.4f}, nstart: {nstart}, nend: {nend} ')
    backend_config = backend.configuration()
    folded_circuits = []
    Rzz_circuits = []

    for i in range(n_Times):
        circuits = Trotter_circuit_5_site_c(
                h, Omega, V, dt, nstart, nend, qubits_list, cond)
        Rzz_circuits = Rzz_circuits + circuits

    # (nend - nstart)*n_Times circuits: 20*10
    print(f'len(Rzz_circuits): {len(Rzz_circuits)}')

    folded_circuits = []
    fidelity = {"single": Single_err, "CNOT": Ave_Rzz_err}

    for i, cnot_circuit in enumerate(Rzz_circuits):
        print(f'zne scaling: {i}/{len(Rzz_circuits)}')
        folded_circuits.append([zne.scaling.fold_gates_at_random(
                cnot_circuit, scale, fidelities=fidelity)
                for scale in scale_factors])

    folded_circuits = list(itertools.chain(*folded_circuits))
    print(f'len(folded_circuits): {len(folded_circuits)}')
    
    ### The process to replace cnot with Rzz gate

    dict_connection = {}
    for k in qubits_list:
        dict_connection[k] = 1

    X = qiskit.circuit.library.standard_gates.x.XGate
    Y = qiskit.circuit.library.standard_gates.y.YGate
    Z = qiskit.circuit.library.standard_gates.z.ZGate
    I = qiskit.circuit.library.standard_gates.i.IGate

    rzz_folded_circuits = []
    raw_rzz_folded_circuits = []

    for h in range(len(folded_circuits)):
        print(f'raw_rzz_folded_circuits {h}/{len(folded_circuits)}')

        rzz_folded = QuantumCircuit(QuantumRegister(
            backend_config.n_qubits, 'q'), *folded_circuits[h].cregs)
        i = 0
        while i < len(folded_circuits[h][:]):
            j = folded_circuits[h][i]
            if i+1 < len(folded_circuits[h][:]):
                y = folded_circuits[h][i+1]
            if len(j[1]) == 2:
                sign = dict_connection[qubits_list[j[1][0].index]
                        ] * dict_connection[qubits_list[j[1][1].index]]
                rzz_folded.rzz(-2*sign*V*dt,
                        qubits_list[j[1][0].index], qubits_list[j[1][1].index])
                if len(y[1]) == 2 and (j[1][0].index, j[1][1].index) == (y[1][0].index, y[1][1].index):
                    rzz_folded.rzz(
                        2*sign*V*dt, qubits_list[j[1][0].index], qubits_list[j[1][1].index])
                    i = i+1
            else:
                qtype = type(j[0])
                if qtype in [I, X, Y, Z]:
                    if qtype in [X, Y]:
                        dict_connection[qubits_list[j[1][0].index]] = -1
                    else:
                        dict_connection[qubits_list[j[1][0].index]] = 1
                indx = j[1][0].index
                j[1][0] = qiskit.circuit.Qubit(QuantumRegister(
                    backend_config.n_qubits, 'q'), qubits_list[indx])
                rzz_folded.data.append(j)
            i = i+1
        raw_rzz_folded_circuits.append(rzz_folded)

        for a, b in enumerate(qubits_list[1:-1:2]):
            rzz_folded.add_calibration('rzz',
                    [b, qubits_list[2*a+2]],
                    Rzz_gate_schedule(b, qubits_list[2*a+2], -2*V*dt, backend),
                    [-2*V*dt])
            rzz_folded.add_calibration('rzz', [b, qubits_list[2*a+2]],
                    Rzz_gate_schedule(b, qubits_list[2*a+2], 2*V*dt, backend),
                    [2*V*dt])

        if cond == 'per':
            rzz_folded.add_calibration('rzz',
                    [qubits_list[0], qubits_list[-1]],
                    Rzz_gate_schedule(qubits_list[0], qubits_list[-1],
                    -2*V*dt, backend),
                    [-2*V*dt])

            rzz_folded.add_calibration('rzz',
                    [qubits_list[0], qubits_list[-1]],
                    Rzz_gate_schedule(qubits_list[0], qubits_list[-1],
                    2*V*dt, backend),
                    [2*V*dt])

        for a, b in enumerate(qubits_list[:-1:2]):
            rzz_folded.add_calibration('rzz',
                    [b, qubits_list[2*a+1]],
                    Rzz_gate_schedule(b, qubits_list[2*a+1], -2*V*dt, backend),
                    [-2*V*dt])

            rzz_folded.add_calibration('rzz',
                    [b, qubits_list[2*a+1]],
                    Rzz_gate_schedule(b, qubits_list[2*a+1], 2*V*dt, backend),
                    [2*V*dt])

        if do_dd:
            rzz_folded = DD_circuit(rzz_folded, backend, qubits_list)

        for a, b in enumerate(qubits_list[1:-1:2]):
            rzz_folded.add_calibration('rzz',
                    [b, qubits_list[2*a+2]],
                    Rzz_gate_schedule(b, qubits_list[2*a+2], -2*V*dt, backend),
                    [-2*V*dt])

            rzz_folded.add_calibration('rzz',
                    [b, qubits_list[2*a+2]],
                    Rzz_gate_schedule(b, qubits_list[2*a+2], 2*V*dt, backend),
                    [2*V*dt])

        if cond == 'per':
            rzz_folded.add_calibration('rzz',
                    [qubits_list[0], qubits_list[-1]],
                    Rzz_gate_schedule(qubits_list[0], qubits_list[-1],
                    -2*V*dt, backend),
                    [-2*V*dt])

            rzz_folded.add_calibration('rzz',
                    [qubits_list[0], qubits_list[-1]],
                    Rzz_gate_schedule(qubits_list[0], qubits_list[-1],
                    2*V*dt, backend),
                    [2*V*dt])

        for a, b in enumerate(qubits_list[:-1:2]):
            rzz_folded.add_calibration('rzz',
                    [b, qubits_list[2*a+1]],
                    Rzz_gate_schedule(b, qubits_list[2*a+1], -2*V*dt, backend),
                    [-2*V*dt])

            rzz_folded.add_calibration('rzz',
                    [b, qubits_list[2*a+1]],
                    Rzz_gate_schedule(b, qubits_list[2*a+1], 2*V*dt, backend),
                    [2*V*dt])

        rzz_folded = transpile(rzz_folded, backend,
                               optimization_level=0, scheduling_method='asap')

        rzz_folded_circuits.append(rzz_folded)
        print(f'it: {nstart+h//len(scale_factors)}' +
                f' ifold: {h%len(scale_factors)}')
        duration = schedule(rzz_folded, backend).duration
        t_ns = duration*backend.configuration().dt*1e9
        print(f'    circuit durartion: {t_ns:.4e} ns')

    print(f'len(rzz_folded_circuits): {len(rzz_folded_circuits)}')
    if jmode == 0:
        print('analysis only, quit here!')
        quit()

    job = qiskit.execute(
            experiments=rzz_folded_circuits,
            backend=backend,
            optimization_level=0,
            shots=shots,
            )
    print(f'TrotterExecutor_random_rzz job id: {job.job_id()}')
    return job


def ZZ_cnot(qc, V, dt, qubits_list, cond):  # nearest neighbor coupling for Rzz_cnot
    if cond == 'per':
        if type(qubits_list) == int:
            num = qubits_list
            for i in range(1, num-1, 2):
                qc.cx(i, i+1)
                qc.rz(-2*V*dt, i+1)
                qc.cx(i, i+1)
            qc.cx(num-1, 0)
            qc.rz(-2*V*dt, 0)
            qc.cx(num-1, 0)
            for i in range(0, num-1, 2):
                qc.cx(i, i+1)
                qc.rz(-2*V*dt, i+1)
                qc.cx(i, i+1)
        else:
            for i, j in enumerate(qubits_list[1:-1:2]):
                qc.cx(j, qubits_list[2*i+2])
                qc.rz(-2*V*dt, qubits_list[2*i+2])
                qc.cx(j, qubits_list[2*i+2])
            qc.cx(qubits_list[-1], qubits_list[0])
            qc.rz(-2*V*dt, qubits_list[0])
            qc.cx(qubits_list[-1], qubits_list[0])
            for i, j in enumerate(qubits_list[:-1:2]):
                qc.cx(j, qubits_list[2*i+1])
                qc.rz(-2*V*dt, qubits_list[2*i+1])
                qc.cx(j, qubits_list[2*i+1])
    if cond == 'op':
        if type(qubits_list) == int:
            num = qubits_list
            for i in range(1, num-1, 2):
                qc.cx(i, i+1)
                qc.rz(-2*V*dt, i+1)
                qc.cx(i, i+1)
            for i in range(0, num-1, 2):
                qc.cx(i, i+1)
                qc.rz(-2*V*dt, i+1)
                qc.cx(i, i+1)
        else:
            for i, j in enumerate(qubits_list[1:-1:2]):
                qc.cx(j, qubits_list[2*i+2])
                qc.rz(-2*V*dt, qubits_list[2*i+2])
                qc.cx(j, qubits_list[2*i+2])
            for i, j in enumerate(qubits_list[:-1:2]):
                qc.cx(j, qubits_list[2*i+1])
                qc.rz(-2*V*dt, qubits_list[2*i+1])
                qc.cx(j, qubits_list[2*i+1])


def Trotter_circuit_5_site_no_custom(h, Omega, V,
        dt, nstart, nend,
        qubits_list, cond):
    """

    Parameters
    ----------
    h : Z field strength.
    Omega : Rabi coupling strength.
    V : h/2 ZZ coupling.
    dt : time step.
    nstart : start from nth trotter step.
    nend : end at nth trotter step.
    qubits_list : what qubits list 
    cond : 'Per' or 'op' Periodic boundary or open b

    Returns
    -------
    Trotter_circuits : a list of trotter circuits.

    """
    Trotter_circuits = []
    q_len = len(qubits_list)
    for i in range(nstart, nend):
        Trotter_circuit = QuantumCircuit(q_len, q_len)
        # Initial state setting, afm
        for j in range(1, q_len, 2):
            Trotter_circuit.x(j)

        Trotter_circuit.barrier()
        for k in range(i):
            # Rabi coupling from the second term of Hamiltonian
            X_Rabi(Trotter_circuit, Omega, dt, q_len)
            h_Z(Trotter_circuit, h, dt, q_len, cond)  # Z field
            # Trotter_circuit.barrier()
            # Nearest neighbor hopping term
            ZZ_cnot(Trotter_circuit, V, dt, q_len, cond)
            Trotter_circuit.barrier()

        j = 0
        for i in range(q_len):
            Trotter_circuit.measure(i, j)
            j += 1

        Trotter_circuits.append(Trotter_circuit)
    return Trotter_circuits


def TwirledCNOT(circ, qb0, qb1,
        do_twirl=True,
        ):
    """
    Pauli Twirling for cnot gate

    Parameters
    ----------
    circ : qiskit circuit
    qb0 : int
        qubit 0.
    qb1 : int
        qubit 1.
    do_twirl : boolen, optional
        decide whether you want to do Pauli twirling or not. The default is True.

    """
    def apply_pauli(circ, num, qb):
        if (num == 0):
            circ.i(qb)
        elif (num == 1):
            circ.x(qb)
        elif (num == 2):
            circ.y(qb)
        else:
            circ.z(qb)
        return circ

    paulis = [(i, j) for i in range(0, 4) for j in range(0, 4)]
    paulis.remove((0, 0))
    paulis_map = [(0, 1), (3, 2), (3, 3), (1, 1), (1, 0), (2, 3), (2, 2),
                  (2, 1), (2, 0), (1, 3), (1, 2), (3, 0), (3, 1), (0, 2), (0, 3)]
    num = random.randrange(len(paulis))

    if do_twirl:
        apply_pauli(circ, paulis[num][0], qb0)
        apply_pauli(circ, paulis[num][1], qb1)

    circ.cnot(qb0, qb1)

    if do_twirl:
        apply_pauli(circ, paulis_map[num][0], qb0)
        apply_pauli(circ, paulis_map[num][1], qb1)


def ZZ_cnot_Twirled(qc, V, dt, qubits_list, cond,
        do_twirl=True,
        ):  # nearest neighbor coupling
    """
    R_zz: cnot gate with Pauli Twirling
    
    """
    if type(qubits_list) == int:
        num = qubits_list
        for i in range(1, num-1, 2):
            TwirledCNOT(qc, i, i+1, do_twirl=do_twirl)
            qc.rz(-2*V*dt, i+1)
            TwirledCNOT(qc, i, i+1, do_twirl=do_twirl)
        if cond == 'per':
            TwirledCNOT(qc, num-1, 0, do_twirl=do_twirl)
            qc.rz(-2*V*dt, 0)
            TwirledCNOT(qc, num-1, 0, do_twirl=do_twirl)
            qc.barrier()
        for i in range(0, num-1, 2):
            TwirledCNOT(qc, i, i+1, do_twirl=do_twirl)
            qc.rz(-2*V*dt, i+1)
            TwirledCNOT(qc, i, i+1, do_twirl=do_twirl)
    else:
        for i, j in enumerate(qubits_list[1:-1:2]):
            TwirledCNOT(qc, j, qubits_list[2*i+2], do_twirl=do_twirl)
            qc.rz(-2*V*dt, qubits_list[2*i+2])
            TwirledCNOT(qc, j, qubits_list[2*i+2], do_twirl=do_twirl)
        if cond == 'per':
            TwirledCNOT(qc, qubits_list[-1], qubits_list[0], do_twirl=do_twirl)
            qc.rz(-2*V*dt, qubits_list[0])
            TwirledCNOT(qc, qubits_list[-1], qubits_list[0], do_twirl=do_twirl)
            qc.barrier()
        for i, j in enumerate(qubits_list[:-1:2]):
            # qc.rzz(-2*V*dt,j,qubits_list[2*i+1])
            TwirledCNOT(qc, j, qubits_list[2*i+1], do_twirl=do_twirl)
            qc.rz(-2*V*dt, qubits_list[2*i+1])
            TwirledCNOT(qc, j, qubits_list[2*i+1], do_twirl=do_twirl)


def Trotter_circuit_5_site_no_custom_Twirled(h, Omega, V,
        dt, nstart, nend,
        qubits_list, cond,
        do_twirl=True,
        ):
    q_len = len(qubits_list)
    Trotter_circuits = []
    for i in range(nstart, nend):
        Trotter_circuit = QuantumCircuit(q_len, q_len)

        # Initial state setting
        for j in range(1, q_len, 2):
            Trotter_circuit.x(j)

        ###
        Trotter_circuit.barrier()
        for k in range(i):
            # Rabi coupling from the second term of Hamiltonian
            X_Rabi(Trotter_circuit, Omega, dt, q_len)
            h_Z(Trotter_circuit, h, dt, q_len, cond)  # Z field
            # Trotter_circuit.barrier()

            # Nearest neighbor hopping term
            ZZ_cnot_Twirled(Trotter_circuit, V, dt, q_len, cond,
                    do_twirl=do_twirl,
                    )
            Trotter_circuit.barrier()

        j = 0
        for i in range(q_len):
            Trotter_circuit.measure(i, j)
            j += 1
        Trotter_circuits.append(Trotter_circuit)

    return Trotter_circuits


def TrotterCNOTExecutor(backend,
        Single_err,
        Ave_cnot_err,
        shots=8192,
        qubits_list=get_qubits_list(),
        scale_factors=get_scale_factors(),
        do_dd=get_flag_dd(),   # dynamical decoupling
        Omega = 0.2*1.2,
        h=-2,
        V=-1,
        dt=1.0,
        nstart=0,
        nend=20,
        n_Times=1,
        cond='op',
        do_twirl=True,
        jmode=0,  # job mode
        ):
    #  ## To run random gate folding trotter circuit with 2-cnot Rzz gate and Pauli Twirling. 

    print(f'dt: {dt:.4f}, nstart: {nstart}, nend: {nend} ')
    folded_circuits = []
    Cnot_circuits = []

    for i in range(n_Times):
        circuits = Trotter_circuit_5_site_no_custom_Twirled(
                h, Omega, V, dt, nstart, nend, qubits_list, cond,
                do_twirl=do_twirl,
                )
        Cnot_circuits = Cnot_circuits + circuits

    print(f'len(Cnot_circuits): {len(Cnot_circuits)}')

    fidelity = {"single": Single_err, "CNOT": Ave_cnot_err}

    for i, circuit in enumerate(Cnot_circuits):
        print(f'zne scaling: {i}/{len(Cnot_circuits)}')
        folded_circuits.append([mitiq.zne.scaling.fold_gates_at_random(
                circuit, scale, fidelities=fidelity)
                for scale in scale_factors])

    folded_circuits = list(itertools.chain(*folded_circuits))
    print(f'len(folded_circuits): {len(folded_circuits)}')

    qr = QuantumRegister(len(qubits_list), 'q')
    init_layout = {}

    for i, j in enumerate(qubits_list):
        init_layout[qr[i]] = j

    if backend.configuration().coupling_map and do_dd:
        for i in range(len(folded_circuits)):
            print(f'DD_circuit: {i}/{len(folded_circuits)}')
            folded_circuits[i] = DD_circuit(
                    folded_circuits[i], backend, qubits_list,
                    initial_layout=init_layout)
            print(f'it: {nstart+i//len(scale_factors)}' +
                    f' ifold: {i%len(scale_factors)}')
            duration = schedule(folded_circuits[i], backend).duration
            t_ns = duration*backend.configuration().dt*1e9
            print(f'    circuit durartion: {t_ns:.4e} ns')

    if jmode == 0:
        print('analysis only, quit here.')
        quit()

    job = qiskit.execute(
            experiments=folded_circuits,
            backend=backend,
            optimization_level=0,
            shots=shots
            )
    print(f'TrotterCNOTExecutor job id: {job.job_id()}')
    return job


def DD_circuit(circuit, backend, qubits_list, initial_layout=None):
    """

    Parameters
    ----------
    circuit : qiskit circuit.
    backend : quantum hardware backend.
    qubits_list : qubit list.
    initial_layout : layerout for qubit. The default is None.

    Returns
    -------
    DD_cir : the circuit w/ dynamical decoupling.

    """

    circuit = transpile(circuit, backend, optimization_level=0,
            scheduling_method='asap', initial_layout=initial_layout)

    DD_cir = QuantumCircuit(*circuit.qregs, *circuit.cregs)
    for operation in circuit:
        if type(operation[0]) == qiskit.circuit.delay.Delay:
            if operation[0].duration >= 400:
                q_index = operation[1][0].index
                t = (operation[0].duration-320)//4
                t = math.ceil((t)/16)*16
                DD_cir.delay(t, [q_index], 'dt')
                DD_cir.x(q_index)
                DD_cir.delay(2*t, [q_index], 'dt')
                DD_cir.z(q_index)
                DD_cir.x(q_index)
                DD_cir.z(q_index)
            else:
                DD_cir.data.append(operation)
        else:
            DD_cir.data.append(operation)
    DD_cir = transpile(DD_cir, backend, optimization_level=0,
            scheduling_method='asap',
            #initial_layout=initial_layout,
            )
    return DD_cir


# Extrapolation_process
def mitiq_Extrapolation_1(
        results,  # nreps of experiments
        scale_factors=get_scale_factors(),
        qubits_list=get_qubits_list(),
        meas_filter=None,
        is_qasm=False,
        extraporder=1,
        nextrappoints=0,
        ):
    N_q = len(qubits_list)
    nreps = len(results)
    print(f'repitition of expts: {nreps}')
    num_result = len(results[0].get_counts())
    Original_num = num_result//len(scale_factors)
    if meas_filter is not None:
        mit_results = []
        for i, result in enumerate(results):
            print(f"get meas_filter results {i}/{nreps}")
            mres = meas_filter.apply(result, method='pseudo_inverse')
            mit_results.append(mres)
    else:
        mit_results = results

    Ave_result_dict_list = []
    for step in range(num_result):
        print(f'calculate avg: {step}/{num_result}')
        RaW_avg = {}
        for h in range(nreps):
            copy_counts = mit_results[h].get_counts(step).copy()
            for x in copy_counts.keys():
                if x in RaW_avg:
                    RaW_avg[x] = RaW_avg[x]+copy_counts[x]
                else:
                    RaW_avg[x] = copy_counts[x]
        Ave_result_dict_list.append(RaW_avg.copy())

    zero_noise_values = {}
    for k in range(N_q):
        expectation_values = []
        for t in range(num_result):
            expectation_values.append(
                post_selected_expval(Ave_result_dict_list[t], k))
        zero_noise_values[k] = []
        if is_qasm: #see whether backend is real quantum hardware or not.
            for i in range(Original_num):
                zero_noise_values[k].append(
                        expectation_values[i*len(scale_factors)])
        else:  # device
            if nextrappoints <= 0 or nextrappoints > len(scale_factors):
                nextrappoints = len(scale_factors)
            if extraporder <= 0:
                extraporder = 1
            if extraporder >= nextrappoints:
                extraporder = nextrappoints - 1
            fac = mitiq.zne.inference.PolyFactory(
                    scale_factors[:nextrappoints], order=extraporder)
            for i in range(Original_num):
                zero_noise_values[k].append(
                        fac.extrapolate(scale_factors[:nextrappoints],
                        expectation_values[i*len(scale_factors):
                        i*len(scale_factors)+nextrappoints],
                        order=extraporder,
                        ))

    return zero_noise_values
