# author:  Yongxin Yao (yxphysics@gmail.com)
import numpy
from rmfim.database import jobs


def add_jobs(
        fname,
        ):
    job_ids = numpy.loadtxt(fname, dtype=str).tolist()
    _ = jobs.get_results_by_ids(job_ids)


def driver():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--fname', type=str,
            help='file name which contains the job ids to be added',
            required=True,
            )

    args = parser.parse_args()
    add_jobs(
            args.fname,
            )
