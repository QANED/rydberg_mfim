# author:  Yongxin Yao (yxphysics@gmail.com)
import datetime, pickle
from rmfim.helper import get_backend
from qiskit.providers.jobstatus import JobStatus


def retrieve_jobs(
        ymdhm_start,  # start [year, month, day, hour, minute]
        ymdhm_end,    # end year, month, day, hour, minute]
        limit=300,    # limit of number of jobs to be retrieved
        fname='jobs',
        ):
    backend = get_backend()
    backend_job_limit = backend.job_limit()
    maximum_jobs = backend_job_limit.maximum_jobs
    active_jobs = backend_job_limit.active_jobs
    # basic info
    print(f'max jobs: {maximum_jobs}, max active jobs: {active_jobs}')
    print(f'remaining jobs can be submitted: {backend.remaining_jobs_count()}')

    start_datetime = datetime.datetime(*ymdhm_start, 0, 1)
    end_datetime = datetime.datetime(*ymdhm_end, 0, 1)
    jobs = backend.jobs(
            limit=limit,
            start_datetime=start_datetime,
            end_datetime=end_datetime,
            status=JobStatus.DONE,
            )
    print(f'number of jobs: {len(jobs)}')
    # to dict
    jobs = {job.job_id(): job.result() for job in jobs}
    with open(f'{fname}.pkl', 'wb') as f:
        pickle.dump(jobs, f)



def driver():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--ymdhm_start', nargs=5, type=int,
            help='<Required> Set start year month day hour minute',
            required=True,
            )
    parser.add_argument('-e', '--ymdhm_end', nargs=5, type=int,
            help='<Required> Set start year month day hour minute',
            required=True,
            )
    parser.add_argument("-l", "--limit", type=int, default=300,
            help="upper limit of number of jobs. dflt: 300")
    parser.add_argument("-f", "--fname", type=str, default='jobs',
            help="job file name. dflt: jobs")

    args = parser.parse_args()
    retrieve_jobs(
            ymdhm_start=args.ymdhm_start,
            ymdhm_end=args.ymdhm_end,
            limit=args.limit,
            fname=args.fname,
            )
