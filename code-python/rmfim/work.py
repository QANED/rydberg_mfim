#!/usr/bin/env python
# coding: utf-8

from mitiq import zne
import scipy.linalg as la
from numpy.core.fromnumeric import mean
from qiskit.ignis.verification import InterleavedRBFitter
from qiskit.ignis.verification import randomized_benchmarking as rb
from IPython.display import display, clear_output
import time
from qiskit.ignis.mitigation.measurement import (
    tensored_meas_cal,
    TensoredMeasFitter,
    TensoredFilter)
from qiskit.circuit.gate import Gate
import warnings
from qiskit.visualization import SchedStyle
from qiskit.pulse.library import drag, GaussianSquare, Drag
from qiskit.pulse import Play, Schedule, DriveChannel, ControlChannel, Waveform, ShiftPhase
from qiskit import pulse
from qiskit.ignis.mitigation.measurement import (complete_meas_cal, tensored_meas_cal,
                                                 CompleteMeasFitter, TensoredMeasFitter)
from scipy.special import erf
import math
from qiskit.tools.visualization import *
from qiskit import execute
import qiskit.ignis.mitigation as mit
import qiskit.quantum_info as qi
import qiskit.circuit.library as circuit_library
from qiskit import QuantumCircuit
import random
import atexit
from qiskit.tools.monitor import job_monitor
from qiskit import IBMQ
import sys
import os
import argparse
import mitiq
import itertools
from qiskit.providers.aer import AerSimulator
import matplotlib.pyplot as plt
from qiskit.test.mock import *
import pandas as pd
from qiskit.providers.aer.noise.errors.standard_errors import depolarizing_error
import qiskit.providers.aer.noise as noise
from qiskit.providers.aer.noise import NoiseModel
from qiskit.ignis.mitigation.measurement import complete_meas_cal, CompleteMeasFitter
from qiskit import Aer
import qiskit
from qiskit import *
import numpy as np
warnings.filterwarnings('ignore')


IBMQ.load_account()
provider = IBMQ.get_provider(hub='',
        group='',
        project='')

backend = provider.get_backend('ibmq_guadalupe')
properties = backend.properties()

backend_config = backend.configuration()
ham_params = backend_config.hamiltonian['vars']
dt = backend_config.dt
print(f"Sampling time: {dt:.0e} s")

backend_defaults = backend.defaults()
inst_sched_map = backend_defaults.instruction_schedule_map
inst_sched_map.instructions


qubits_list = [1, 2, 3, 5, 8, 11, 14, 13, 12, 10, 7, 4]
q_conn = []


# Parameters used for the Trotter cirucit
Omega = 0.2*1.2
h = -2
V = -1
T = 40  # Total time
TT = T

n = 40
n40 = 64
NN = n
NN40 = n40
shots = 8192


qr = QuantumRegister(len(qubits_list), 'q')
init_layout = {}
for i, j in enumerate(qubits_list):
    init_layout[qr[i]] = j

cond = 'op'
Trotter_circuit_no_custom = Trotter_circuit_5_site_no_custom(
    h, Omega, V, T, n, qubits_list, cond)
Trotter_circuit_no_custom_Twirled = Trotter_circuit_5_site_no_custom_Twirled(
    h, Omega, V, T, n, qubits_list, cond)
Trotter_circuit_no_custom_40 = Trotter_circuit_5_site_no_custom(
    h, Omega, V, T, n40, qubits_list, cond)
Trotter_circuit_Rzz = Trotter_circuit_5_site_Rzz(
    h, Omega, V, T, n, qubits_list, cond)

provider = IBMQ.get_provider(hub='ibm-q', group='open', project='main')

shots = 8192

Trotter_result_cnot = execute(
    Trotter_circuit_no_custom, backend, shots=shots, initial_layout=init_layout)

Trotter_result_Rzz = execute(Trotter_circuit_Rzz, backend, shots=shots)

# # The result of Trotter circuit

# In[ ]:


shots = 8192

n = 40

dt = T/n
#real_device_job_rzz_golab = TrotterExecutor(backend, shots, n, qubits_list,cond)
#real_device_job_rzz = TrotterExecutor_random_rzz( backend, n, qubits_list, dt, V, cond, 15)

#scale_factors = [1.0, 1.5, 2.0]
#real_device_dict_rzz = mitiq_Extrapolation_1(real_device_job_rzz[0], backend, scale_factors, qubits_list, meas_filter=meas_filter, Num=15)


#print('Rzz gate:',real_device_job_rzz[0].job_id())

real_device_job_rzz10 = TrotterExecutor_random_rzz(
    backend, n, qubits_list, dt, V, cond, 10)

print('Rzz gate:', real_device_job_rzz10[0].job_id())

scale_factors = [1.0, 1.5, 2.0]
real_device_dict_rzz10 = mitiq_Extrapolation_1(
    real_device_job_rzz10[0], backend, scale_factors, qubits_list, meas_filter=meas_filter, Num=10)


# In[ ]:


real_device_job_cnot_Twirled = TrotterCNOTExecutor(
    Trotter_circuit_no_custom_Twirled, backend, 8192, qubits_list)
scale_factors = [1.0, 1.5, 2.0]
real_device_dict_cnot = mitiq_Extrapolation_1(
    real_device_job_cnot_Twirled, backend, scale_factors, qubits_list, meas_filter=meas_filter, Num=10)
print('Cnot gate:', real_device_job_cnot_Twirled.job_id())


# In[ ]:


result_cnot = Trotter_result_cnot.result()

results_sim = Trotter_result_sim.result()

results_sim_40 = Trotter_result_sim_40.result()

result_Rzz = Trotter_result_Rzz.result()


# In[ ]:


q_n = len(qubits_list)
counts_cnots_list = meas_fitter.filter.apply(
    result_cnot, method='pseudo_inverse').get_counts()
counts_Rzz_list = meas_fitter.filter.apply(
    result_Rzz, method='pseudo_inverse').get_counts()
Sz_sim = []
Sz__cnots = []
Sz__Rzz = []
for i in range(NN):

    tot_cnots = np.zeros(q_n)
    tot_sim = np.zeros(q_n)
    tot_Rzz = np.zeros(q_n)

    counts_cnots = counts_cnots_list[i]
    counts_Rzz = counts_Rzz_list[i]
    counts_sim = results_sim.get_counts(i)
    for j in range(2**q_n):
        n = 2**q_n

        if j in counts_cnots.int_outcomes():
            a = bin(n+j)[3:]
            c = ' '
            tot_cnots += (2*np.array(c.join(a).split()).astype(int)-1) * \
                counts_cnots.int_outcomes()[j]

        if j in counts_sim.int_outcomes():
            a = bin(n+j)[3:]
            c = ' '
            tot_sim += (2*np.array(c.join(a).split()).astype(int)-1) * \
                counts_sim.int_outcomes()[j]

        if j in counts_Rzz.int_outcomes():
            a = bin(n+j)[3:]
            c = ' '
            tot_Rzz += (2*np.array(c.join(a).split()).astype(int)-1) * \
                counts_Rzz.int_outcomes()[j]

    Sz__cnots.append(tot_cnots/8192)
    Sz_sim.append(tot_sim/8192)
    Sz__Rzz.append(tot_Rzz/8192)

Sz__Rzz = np.array(Sz__Rzz)
Sz__cnots = np.array(Sz__cnots)
Sz_sim = np.array(Sz_sim)


# In[ ]:


Sz_sim40 = []
for i in range(n40):
    tot_sim40 = np.zeros(q_n)
    counts_sim40 = results_sim_40.get_counts(i)
    for j in range(2**q_n):
        n = 2**q_n
        if j in counts_sim40.int_outcomes():
            a = bin(n+j)[3:]
            c = ' '
            tot_sim40 += (2*np.array(c.join(a).split()).astype(int)-1) * \
                counts_sim40.int_outcomes()[j]
    Sz_sim40.append(tot_sim40/15000)
Sz_sim40 = np.array(Sz_sim40)


# # ED

# In[ ]:


def flip(n, i, L):
    # Flip the i site spin in n-state with system size L
    ii = 2**(L-i-1)
    nn = 2**L
    if int(bin(n+nn)[3:][i]) == 1:
        return binaryToDecimal(int(bin(n-ii)[2:]))
    if int(bin(n+nn)[3:][i]) == 0:
        return binaryToDecimal(int(bin(n+ii)[2:]))


def binaryToDecimal(binary):
    # change the binary number into decimal number
    binary1 = binary
    decimal, i, n = 0, 0, 0
    while(binary != 0):
        dec = binary % 10
        decimal = decimal + dec * pow(2, i)
        binary = binary//10
        i += 1
    return decimal


def spin_x(i, L):
    n = 2**L
    X = np.zeros((n, n))
    for j in range(n):
        X[j, flip(j, i, L)] = 1
    return X


def spin_y(i, L):
    n = 2**L
    Y = np.zeros((n, n), dtype=complex)
    for j in range(n):
        Y[j, flip(j, i, L)] = 1j*(-1)**(int(bin(j+n)[3:][i]))
    return Y


def spin_z(i, L):
    n = 2**L
    Z = np.zeros((n, n))
    # a=2**L-1
    for j in range(n):
        Z[j, j] = 2*int(bin(j+n)[2:][::-1][i])-1
    return Z


# In[ ]:


L = 12

Omega = 0.2*1.2
h = 2  # 2*2
V = 1


H0 = 0
for i in range(L):
    H0 = H0+Omega*spin_x(i, L)  # Rabi coupling

H1 = 0
for i in range(L):
    H1 = H1+h*spin_z(i, L)


if cond == 'per':
    H1 = 0
    for i in range(L):
        H1 = H1+h*spin_z(i, L)

    V0 = 0
    for i in range(L):
        n_f = spin_z((i) % L, L)
        n_b = spin_z((i+1) % L, L)
        V0 = V0+V*np.dot(n_f, n_b)

if cond == 'op':
    H1 = 0
    H1 = H1+h*spin_z(0, L)/2
    H1 = H1+h*spin_z(L-1, L)/2
    for i in range(L-2):
        H1 = H1+h*spin_z(i+1, L)

    V0 = 0
    for i in range(L-1):
        n_f = spin_z((i) % L, L)
        n_b = spin_z((i+1) % L, L)
        V0 = V0+V*np.dot(n_f, n_b)


H = H0+H1+V0


# In[ ]:


binaryToDecimal(10101010101)


# In[ ]:


M = H
vals, vecs = la.eigh(M)

inds = np.argsort(vals)
e = vals[inds]
v = vecs[:, inds]


# In[ ]:


Nt = 40-1
T = 40
dt = T/Nt

A = np.eye(len(e))*e
U = np.dot(v, np.dot(np.exp(-1j*A*dt)*np.eye(len(e)), np.conj(np.transpose(v))))

f = 1365

wavefunc = np.zeros((len(e), Nt+1), dtype=complex)
n = np.zeros(len(e), dtype=complex)
n[f] = 1
wavefunc[:, 0] = n
for i in range(Nt):
    wavefunc[:, i+1] = np.dot(U, wavefunc[:, i])


# In[ ]:


def Zi_t_p(tf, dt, f, i):

    Nt = tf/dt
    Nt = int(Nt)

    Zt = spin_z(i, L)
    Zt1 = [np.real(np.dot(np.conjugate(wavefunc[:, i]), np.dot(
        Zt, wavefunc[:, i]))) for i in range(Nt+1)]
    Zt1 = np.stack(Zt1, axis=0)

    return Zt1


# In[ ]:


Zi = np.zeros((L, Nt+1))

for i in range(L):

    Zi[i, :] = np.real(Zi_t_p(T, dt, f, i))


# # Plots and Result

# In[ ]:


for i in range(L):
    t40 = np.linspace(0, T, 64)
    t = np.linspace(0, T, NN)
    plt.title("Qubit"+str(L-i))
    #plt.plot(t,real_device_dict_cnot_Twirled[i],color='r',label='Twirl, two cx and Rand ZNE')
    plt.plot(t, Sz__cnots[:, i], color='b', label='Cnot_gates')
    plt.plot(t, Sz_sim[:, i], color='g',
             label='Trotter simulation w/ statevector')
    #plt.plot(t40,Sz_sim40[:,i],color='g',linestyle = 'dashdot',label='Trotter simulation 40 steps w/ statevector')
    plt.plot(t, Sz__Rzz[:, i], color='y', label='Pulse gate')
    plt.plot(t, real_device_dict_cnot[i], color='m',
             label='Two Cnot gate with Random Twirling ZNE, DD')
    #plt.plot(t,real_device_dict_rzz[i],color='c',label='with 15 Random Twirling ZNE, DD and pulse gate')
    plt.plot(t, real_device_dict_rzz10[i], color='r',
             label='with 10 Random Twirling ZNE, DD and pulse gate')
    #plt.plot(t,real_device_dict_cnot_Twirled[i],color='r',label='Two Cnot gate with ZNE and twirling')
    #plt.plot(t,real_device_dict_Twirled[i],color='b',label='Pulse gate with DD, ZNE and twirling')
    #plt.plot(t,real_device_dict_Rand_Rzz_Twirled[i],color='m',label='Pulse gate, Rand ZNE and twirling')
    plt.plot(t, Zi[i, :], color='k', label='ED')
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.grid(True)
    plt.legend()
    plt.show()


# In[ ]:


def g(d, ei, ef):
    """
    integration is written by myself with the Simpson's rule and 3/8 Simpson's rule. Unlike Scipy's, this code can directly deal
with array with initial point and final point.
    # d: the array before integrated
    # ei: start point
    # ef: ending point
    """
    N = d.shape[0]-1
    h = (ef-ei)/N
    return ig(d, N+1, h)


def ig(d, N, h):
    # using the Simpson's rule
    """
    The integration is written by myself with the Simpson's rule and 3/8 Simpson's rule. Unlike Scipy's, this code can directly deal
with array with initial point and final point.
    """
    if N == 1:
        return 0
    elif N == 0:
        return 0
    elif N == 2:
        return (d[0]+d[1])*h/2
    elif N == 3:
        return (d[0]+4*d[1]+d[2])*h/3
    elif N == 4:
        return (d[0]+3*d[1]+3*d[2]+d[3])*3*h/8
    elif (N+1) % 2 == 0:
        return ((d[N-1]+d[0]+sum(d[1::2])*4+sum(d[2:N-1:2])*2))*h/3
    elif (N+1) % 2 == 1:
        return (sum(d[1:N-3:2])*4+sum(d[2:N-5:2])*2+d[0]+d[N-4])*h/3+ig(d[-4:], 4, h)


# In[ ]:


Int_D_cnot_t = []
Int_D_Rzz_t = []
Int_D_Rzz_fold_t = []
Int_D_cnot_fold_t = []
for k, j in enumerate(t):
    # print(k,j)
    D_cnot = 0
    D_Rzz = 0
    D_Rzz_fold = 0
    D_cnot_fold = 0
    for i in range(L):
        # plt.title("Qubit"+str(L-i))
        D_cnot = D_cnot+np.abs(Sz_sim[:k+1, i]-Sz__cnots[:k+1, i])**2
        D_Rzz = D_Rzz+np.abs(Sz_sim[:k+1, i]-Sz__Rzz[:k+1, i])**2
        D_Rzz_fold = D_Rzz_fold + \
            np.abs(Sz_sim[:k+1, i] -
                   np.array(real_device_dict_rzz10[i][:k+1]))**2
        D_cnot_fold = D_cnot_fold + \
            np.abs(Sz_sim[:k+1, i]-np.array(real_device_dict_cnot[i][:k+1]))**2

    D_cnot = D_cnot/L
    D_Rzz = D_Rzz/L
    D_Rzz_fold = D_Rzz_fold/L
    D_cnot_fold = D_cnot_fold/L

    # if k==1 or k==0:
    #  print(D_cnot)
    #  print(D_Rzz)
    #  print(D_Rzz_fold)
    #  print(D_cnot_fold)
    if j == 0:
        Int_D_cnot = 0
        Int_D_Rzz = 0
        Int_D_Rzz_fold = 0
        Int_D_cnot_fold = 0
    else:
        Int_D_cnot = g(D_cnot, 0, j)/j
        Int_D_Rzz = g(D_Rzz, 0, j)/j
        Int_D_Rzz_fold = g(D_Rzz_fold, 0, j)/j
        Int_D_cnot_fold = g(D_cnot_fold, 0, j)/j

    Int_D_cnot_t.append(Int_D_cnot)
    Int_D_Rzz_t.append(Int_D_Rzz)
    Int_D_Rzz_fold_t.append(Int_D_Rzz_fold)
    Int_D_cnot_fold_t.append(Int_D_cnot_fold)

plt.plot(t, Int_D_cnot_t, color='b', label='Cnot_gates')
plt.plot(t, Int_D_Rzz_t, label='Pulse gate')
plt.plot(t, Int_D_Rzz_fold_t, color='r',
         label='with 10 Random Twirling ZNE, DD and pulse gate')
plt.plot(t, Int_D_cnot_fold_t, color='m',
         label='Two Cnot gate with Random Twirling ZNE, DD')
plt.grid()
plt.legend()
plt.show()


# # Save data point

# In[ ]:


f = open(path_dir.replace('.', '_Sz_cnot.'), 'w')
f.write('The time step:'+str(dt)+'\n')

f.write('Sz_cnot:\n')

for i in range(L):
    f.write(str(Sz__cnots[:, i]))
    f.write('\n')

f.close()

f = open(path_dir.replace('.', '_Trotter_simulation_statevector.'), 'w')
f.write('The time step:'+str(dt)+'\n')

f.write('Trotter simulation w/ statevector:\n')
for i in range(L):
    f.write(str(Sz_sim[:, i]))
    f.write('\n')
f.close()

f = open(path_dir.replace('.', '_Trotter_simulation_40_steps_statevector.'), 'w')
f.write('The time step:'+str(dt)+'\n')

f.write('Trotter simulation 40 steps w/ statevector:\n')

for i in range(L):
    f.write(str(Sz_sim40[:, i]))
    f.write('\n')
f.close()

f = open(path_dir.replace('.', '_Two_Cnot_gate_with_ZNE.'), 'w')
f.write('The time step:'+str(dt)+'\n')
f.write('Two Cnot gate with ZNE:\n')

for i in range(L):
    f.write(str(real_device_dict_cnot[i]))
    f.write('\n')
f.close()

f = open(path_dir.replace('.', '_ED.'), 'w')
f.write('The time step:'+str(dt)+'\n')

f.write('ED:\n')
for i in range(L):
    f.write(str(Zi[i, :]))
    f.write('\n')
f.close()


# # Device data

# In[ ]:


for i in qubits_list:
    print('t1:', properties.t1(i)*(1e6), 'us')
    print('t2:', properties.t2(i)*(1e6), 'us')


# In[ ]:


for i in q_conn:
    print('cx error rate:', i, ' ', properties.gate_error('cx', i))
for i in qubits_list:
    print('x error rate:', i, ' ', properties.gate_error('x', i))
for i in Tw_cnot_err:
    print('Two cnot error rate:', i)
for i in Rzz_err:
    print('Rzz pulse error rate:', i)


# # Retrieve Job

# To get raw data, we can use retrieve_job. For example, you want to gat raw data of job 614b5b33d67404cfc327df8f. You can backendretrieve_job('614b5b33d67404cfc327df8f') to get job result again.

# In[ ]:


result = backend.retrieve_job('job id')


# In[ ]:
