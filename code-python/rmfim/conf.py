import atexit, json


def load_data():
    with open('config.json', 'r') as f:
        data = json.load(f)
    return data


def save_data(data):
    with open('config.json', 'w') as f:
        json.dump(data, f, indent=4)



data = load_data()
atexit.register(save_data, data)
