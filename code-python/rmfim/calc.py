# Author: Yongxin Yao (yxphysics@gmail.com)
import numpy, os
from qiskit import execute
from qiskit.ignis.mitigation.measurement import tensored_meas_cal

from rmfim.helper import get_backend, get_qubits_list
from rmfim import conf
from rmfim.emlib import TrotterExecutor_random_rzz, TrotterCNOTExecutor


def run_calib_circ(
        backend,
        qubits_list=get_qubits_list(),
        shots=8192,
        ):
    cal_circuits, state_labels = tensored_meas_cal(
            mit_pattern=[[i] for i in qubits_list])
    print(f'len(cal_circuits): {len(cal_circuits)}')
    job = execute(cal_circuits, backend, shots=shots)
    print(f'cal_circ job id: {job.job_id()}')

    with open('cal_circ.jobid', 'a') as f:
        f.write(f'{job.job_id()}\n')


def TrotterExecutor_random_rzz_driver(backend,
        n_Times=20,
        jmode=0,
        ):
    single_errs = [backend.properties().gate_error('x', [i]) for
            i in backend.configuration().meas_map[0]]
    single_err = numpy.mean(single_errs)
    rzz_err, dt, nstart, nend = [conf.data[key]
            for key in ["rzz_err_avg", "dt", "nstart", "nend"]]
    print(f'single_err: {single_err:.4f}, rzz_err: {rzz_err:.4f}')

    with open('TrotterExecutor_random_rzz_jobids.dat', 'a') as f:
        for i in range(n_Times):
            print(f'submitting TrotterExecutor_random_rzz {i}/{n_Times}')
            job = TrotterExecutor_random_rzz(backend,
                    single_err,
                    rzz_err,
                    dt=dt,
                    nstart=nstart,
                    nend=nend,
                    jmode=jmode,
                    )
            f.write(f'{job.job_id()}\n')


def TrotterCNOTExecutor_driver(backend,
        n_Times=20,
        jmode=0,
        ):
    single_errs = [backend.properties().gate_error('x', [i]) for
            i in backend.configuration().meas_map[0]]
    single_err = numpy.mean(single_errs)
    # cnot_errs = [backend.properties().gate_error('cx', i) for
    #         i in backend.configuration().coupling_map]
    # cnot_err = numpy.mean(cnot_errs)
    cnot_err, dt, nstart, nend = [conf.data[key]
            for key in ["cnot_err_avg", "dt", "nstart", "nend"]]

    print(f'single_err: {single_err:.4f}, cnot_err: {cnot_err:.4f}')

    with open('TrotterCNOTExecutor_jobids.dat', 'a') as f:
        for i in range(n_Times):
            print(f'submitting TrotterCNOTExecutor {i}/{n_Times}')
            job = TrotterCNOTExecutor(backend,
                    single_err,
                    cnot_err,
                    dt=dt,
                    nstart=nstart,
                    nend=nend,
                    jmode=jmode,
                    )
            f.write(f'{job.job_id()}\n')


def driver():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--mode", type=int, default=0,
            help="job mode. 0: ro calibration; 1: cnot gate imp;" + \
                    " 2: pulse gate imp. dlft: 0")
    parser.add_argument("-n", "--ncpu", type=int, default=1,
            help="number of cpus for parallel run. dflt: 1")
    parser.add_argument("-r", "--nrep", type=int, default=20,
            help="number of repititions for the job. dflt: 20")
    parser.add_argument("-j", "--jmode", type=int, default=0,
            help="job mode. dflt: 0(analysis only), 1(submit jobs)")

    args = parser.parse_args()

    backend = get_backend()
    backend_job_limit = backend.job_limit()
    maximum_jobs = backend_job_limit.maximum_jobs
    active_jobs = backend_job_limit.active_jobs
    print(f'max jobs: {maximum_jobs}, max active jobs: {active_jobs}')
    print(f'remaining jobs can be submitted: {backend.remaining_jobs_count()}')
    # disable parallel
    os.environ["QISKIT_NUM_PROCS"] = str(args.ncpu)

    if args.mode == 0:
        # calibration
        run_calib_circ(backend)
    elif args.mode == 1:
        # CNOT gate
        TrotterCNOTExecutor_driver(backend,
                n_Times=args.nrep,
                jmode=args.jmode,
                )
    else:
        # Pulse gate
        TrotterExecutor_random_rzz_driver(backend,
                n_Times=args.nrep,
                jmode=args.jmode,
                )


if __name__ == '__main__':
    driver()
