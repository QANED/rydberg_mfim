from qiskit.providers.jobstatus import JobStatus
from rmfim.helper import get_backend


def driver():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-j", "--jobids", nargs='*', type=str,
            help="<Required> failed job ids, separated by space.",
            required=True,
            )

    backend = get_backend()
    args = parser.parse_args()
    for jid in args.jobids:
        job = backend.retrieve_job(jid)
        if job.status() != JobStatus.ERROR and \
                job.status() != JobStatus.CANCELLED :
            print(f'job {jid} statsus {job.status()}, skipped.')
            continue
        qobj = job.qobj()
        job = backend.run(qobj,
                job_name=job.name(),
                job_tags=job.tags())
        print(f"please replace {jid} with {job.job_id()}")

