import numpy, os, h5py
import matplotlib.pyplot as plt
from rmfim.helper import (get_meas_fitter,
        get_scale_factors,
        get_qubits_list,
        get_step_size,
        )



def save_raw_mitiq_data(
        results,  # nreps of experiments
        dname,    # dataset name
        scale_factors=get_scale_factors(),
        qubits_list=get_qubits_list(),
        meas_filter=None,
        stepsize=get_step_size(),
        do_postselect=True,
        task=0,   # 0: site-wise <z>; 1: Loschmidt echo
        ):
    print(f'generating data {dname}')
    nq = len(qubits_list)
    # repitions of extrapolation experiments.
    nreps = len(results)
    print(f'repitition of expts: {nreps}')
    num_result = len(results[0].get_counts())
    # number of extrapolation points
    npts = len(scale_factors)
    # number of time mesh
    nt = num_result//npts
    # main results
    if task == 0:
        data = numpy.zeros((nt, nq, nreps, npts))
    else:
        data = numpy.zeros((nt, nreps, npts))
    # accumulative way to take care of the memory issue
    irep0 = 0
    with h5py.File('analysis_result.h5', 'a') as f:
        if dname in f:
            data0 = f[dname][()]
            if task == 0:
                irep0 = data0.shape[2]
                data[:, :, :irep0, :] = data0
            else:
                irep0 = data0.shape[1]
                data[:, :irep0, :] = data0

    if task in [1, 2]:
        init_key = '0'*(nq%2) + '10'*(nq//2)
        if task == 2:
            key_list = []
            for i, s in enumerate(init_key):
                s = '1' if s == '0' else '0'
                key_list.append(init_key[:i] + s + init_key[i+1:])
            print(f'key list: {[init_key] + key_list}')

    for irep, result in enumerate(results):
        if irep < irep0:  # already calculated
            continue
        elif irep-irep0 >= stepsize:
            break
        if meas_filter is not None:
            print(f"get meas_filter results {irep}/{nreps}")
            result = meas_filter.apply(result, method='pseudo_inverse')

        print(f'calculate avg irep {irep}/{nreps}')
        for it in range(nt):
            for ipt in range(npts):
                ires = it*npts + ipt
                counts = result.get_counts(ires)
                # post-selection
                if task == 0:
                    vals = numpy.zeros(nq)
                isum = 0
                for key in counts:
                    if '11' not in key or not do_postselect:
                        if task == 0:
                            ids = [i for i, s in enumerate(key) if s == '1']
                            vals[ids] += counts[key]
                        isum += counts[key]
                if task == 0:
                    vals /= isum
                    vals = 2*vals - 1
                    data[it, :, irep, ipt] = vals
                else:
                    # Loschmidt echo
                    data[it, irep, ipt] = counts.get(init_key, 0)/isum
                    if task == 2:
                        for key in key_list:
                            data[it, irep, ipt] += counts.get(key, 0)/isum

        with h5py.File('analysis_result.h5', 'a') as f:
            if dname in f:
                del f[dname]
            if task == 0:
                f[dname] = data[:,:,:irep+1,:]
            else:
                f[dname] = data[:,:irep+1,:]


def plot(
        dname_list,
        T=10,
        nrow=3,
        ncol=4,
        fname='zt',
        labels=None,
        tname_list=None,
        ):
    if labels is None:
        labels = dname_list

    res = []
    tlist = []
    with h5py.File('analysis_result.h5', 'r') as f:
        for i, dname in enumerate(dname_list):
            res.append(f[dname][()])
            if i == 0:
                ts = numpy.arange(0, T, T/res[0].shape[1])
            if tname_list is None or tname_list[i] == '0':
                tlist.append(ts)
            else:
                tlist.append(f[tname_list[i]][()])

    nsite = res[0].shape[0]
    plt.figure(figsize=(16, 12))
    for i in range(nsite):
        ax = plt.subplot(nrow, ncol, i+1)
        for j, data in enumerate(res):
            ax.plot(tlist[j], data[i, :], '-o', label=labels[j])
        ax.legend()
    plt.tight_layout()
    plt.savefig(f'{fname}.pdf')


def plot_driver():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dlist', nargs='*', type=str,
            help='<Required> data set name list',
            required=True,
            )
    parser.add_argument('-l', '--labels', nargs='*', type=str,
            default=None,
            help='data set label list, must match dlist in length. dflt: None',
            )
    parser.add_argument('-t', '--tlist', nargs='*', type=str,
            default=None,
            help='time label list, must match dlist in length,' +\
                    ' missing if 0. dflt: None',
            )
    args = parser.parse_args()
    plot(args.dlist,
            labels=args.labels,
            tname_list=args.tlist,
            )



def driver():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--ncpu", type=int, default=1,
            help="number of cpus for parallel run. dflt: 1")
    parser.add_argument("-t", "--task", type=int, default=0,
            help="flag for analysis task. 0: <z_i>; "+\
                    "1: Loschmidt echo; "+\
                    "2: Loschmidt echo with 1-flip. dflt: 0")
    parser.add_argument("-m", "--mode", type=int, default=0,
            help="analysis mode. 0: cnot version; others: pulse. dflt: 0")
    parser.add_argument("--norocalib", action="store_true",
            help="skip readout calibration. dflt: False")
    parser.add_argument("--nopostselect", action="store_true",
            help="post-selection. dflt: False")
    args = parser.parse_args()

    if args.mode == 0:
        jfile = 'TrotterCNOTExecutor_jobids.dat'
        dname = 'cnot'
    else:
        jfile = 'TrotterExecutor_random_rzz_jobids.dat'
        dname = 'pulse'

    if args.norocalib:
        meas_filter = None
        dname += '_noroc'
    else:
        meas_filter = get_meas_fitter()
        if meas_filter is None:
            dname += '_noroc'
        else:
            dname += '_roc'

    if args.nopostselect:
        dname += '_nops'
    else:
        dname += '_ps'
    if args.task != 0:
        dname += f'_t{args.task}'

    os.environ["QISKIT_NUM_PROCS"] = str(args.ncpu)
    if args.ncpu <= 1:
        os.environ["QISKIT_IN_PARALLEL"] = "TRUE"

    job_ids = numpy.loadtxt(jfile, dtype=str)

    from rmfim.database import jobs
    results = jobs.get_results_by_ids(job_ids)
    save_raw_mitiq_data(
            results,  # nreps of experiments
            dname,  # dataset name
            scale_factors=get_scale_factors(),
            qubits_list=get_qubits_list(),
            meas_filter=meas_filter,
            do_postselect=not args.nopostselect,
            task=args.task,
            )



if __name__ == "__main__":
    driver()
