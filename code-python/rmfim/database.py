import pickle, os, atexit
from rmfim import conf



class database:
    def __init__(self):
        self._modified = False
        self._path = conf.data.get('dpath', './')
        if os.path.isfile(f'{self._path}/jobs_database.pkl'):
            with open(f'{self._path}/jobs_database.pkl', 'rb') as f:
                self._jobs = pickle.load(f)
        else:
            self._jobs = {}

    def get_result_by_id(self, job_id):
        if job_id not in self._jobs:
            self.add_job(job_id)
        return self._jobs[job_id]

    def get_results_by_ids(self, job_ids):
        return [self.get_result_by_id(job_id) for job_id in job_ids]

    def add_job(self, job_id):
        from rmfim.helper import get_backend
        from qiskit.providers.jobstatus import JobStatus

        backend = get_backend()
        job = backend.retrieve_job(job_id)
        if job.status() != JobStatus.DONE:
            raise ValueError(f'job {job_id} is not done.')
        else:
            # store results
            self._jobs[job.job_id()] = job.result()
            self._modified = True
            print(f'added job {job_id}')

    def update(self):
        if self._modified:
            with open(f'{self._path}/jobs_database.pkl', 'wb') as f:
                pickle.dump(self._jobs, f)


def update_jobs(jobs):
    jobs.update()


jobs = database()
atexit.register(update_jobs, jobs)
