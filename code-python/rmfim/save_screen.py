import numpy


jobs = numpy.loadtxt('all.dat', dtype=str)
jobs = jobs.reshape([11, 2, 5])

numpy.savetxt('jobs_ref.dat', [s for js in jobs for s in js[0]], fmt='%s')
numpy.savetxt('jobs_noil.dat', [s for js in jobs for s in js[1]], fmt='%s')
