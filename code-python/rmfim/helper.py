import numpy, os
from rmfim import conf
from qiskit.ignis.mitigation.measurement import (
        tensored_meas_cal,
        TensoredMeasFitter,
        )


def get_backend():
    from qiskit import IBMQ
    if IBMQ.active_account() is None:
        IBMQ.load_account()
    provider = IBMQ.get_provider(hub='ibm-q-bnl',
            group='c2qa-projects',
            project='yao-benchmark-of')

    system = conf.data.get('backend', 'ibmq_guadalupe')
    print(f'backend: {system}')
    backend = provider.get_backend(system)
    print(f"Sampling time: {backend.configuration().dt:.0e} s")

    return backend


def get_qubits_list():
    return conf.data["qubits_list"]


def get_scale_factors():
    return conf.data["scale_factors"]


def get_flag_dd():
    '''flag to perform dynamical decoupling
    '''
    f_dd = conf.data.get("dd", 1)
    return f_dd > 0


def get_step_size():
    '''flag to perform dynamical decoupling
    '''
    step_size = conf.data.get("step_size", 20)
    return step_size


def get_meas_fitter(
        qubits_list=get_qubits_list(),
        ):
    from rmfim.database import jobs
    # get job id
    if os.path.isfile('cal_circ.jobid'):
        job_id = numpy.loadtxt('cal_circ.jobid', dtype=str)[()]
        if not isinstance(job_id, str):
            job_id = job_id[-1]
        cal_results = jobs.get_result_by_id(job_id)
        _, state_labels = tensored_meas_cal(mit_pattern=[[i]
                for i in qubits_list])
        meas_fitter = TensoredMeasFitter(cal_results, state_labels)
        res = meas_fitter.filter
    else:
        print('no cal_circ.jobid found. no meas_fitter.')
        res = None

    return res
