# author:  Yongxin Yao (yxphysics@gmail.com)
import datetime, numpy
from rmfim.helper import get_backend
from qiskit.providers.jobstatus import JobStatus



def retrieve_jobs(
        ymdhm_start=[2022,5,23,0,0],  # start [year, month, day, hour, minute]
        ymdhm_end=[2022,5,27,0,0],    # end year, month, day, hour, minute]
        limit=800,    # limit of number of jobs to be retrieved
        fname='jobs',
        ):
    backend = get_backend()
    backend_job_limit = backend.job_limit()
    maximum_jobs = backend_job_limit.maximum_jobs
    active_jobs = backend_job_limit.active_jobs
    # basic info
    print(f'max jobs: {maximum_jobs}, max active jobs: {active_jobs}')
    print(f'remaining jobs can be submitted: {backend.remaining_jobs_count()}')

    start_datetime = datetime.datetime(*ymdhm_start, 0, 1)
    end_datetime = datetime.datetime(*ymdhm_end, 0, 1)
    jobs = backend.jobs(
            limit=limit,
            start_datetime=start_datetime,
            end_datetime=end_datetime,
            status=JobStatus.DONE,
            )
    jobs = jobs[::-1]
    njobs = len(jobs)
    npair = njobs//10
    print(f'number of jobs: {len(jobs)} npairs {npair}')
    job_ids = [[], []]
    for i in range(npair):
        for j in range(2):
            for k in range(5):
                # exclude the first readout calibration job.
                job_ids[j].append(jobs[i*10 + 5*j + k + 1].job_id())
    jobfiles = ['jobs_rb.dat', 'jobs_rbpulse.dat']
    # save job id's
    for i, jibs in enumerate(job_ids):
        if len(jobs) > 0:
            numpy.savetxt(jobfiles[i], jibs, fmt='%s')



if __name__ == "__main__":
    retrieve_jobs()
