import pickle
import random
import atexit
from qiskit import IBMQ
import sys
import os
import argparse
import mitiq
import itertools
from qiskit.test.mock import *
import pandas as pd
from qiskit.providers.aer.noise.errors.standard_errors import depolarizing_error
import qiskit.providers.aer.noise as noise
from qiskit.providers.aer.noise import NoiseModel
from qiskit.ignis.mitigation.measurement import complete_meas_cal, CompleteMeasFitter
from qiskit import Aer
import qiskit
from scipy import integrate
from qiskit.circuit.gate import Gate
from qiskit import *
# Qiskit module
from qiskit import QuantumCircuit
import qiskit.circuit.library as circuit_library
import qiskit.quantum_info as qi
import qiskit.ignis.mitigation as mit

# Qiskit tools for running and monitoring jobs
from qiskit import execute
from qiskit.tools.monitor import job_monitor

from qiskit.tools.visualization import *
import matplotlib.pyplot as plt
import math
import numpy as np
from scipy.special import erf
# Other imports
from qiskit.ignis.mitigation.measurement import (complete_meas_cal, tensored_meas_cal,
                                                 CompleteMeasFitter, TensoredMeasFitter)
# Qiskit for pulse
from qiskit import pulse

from qiskit import *
from qiskit.pulse import Play, Schedule, DriveChannel, ControlChannel, Waveform, ShiftPhase
from qiskit.pulse.library import drag, GaussianSquare, Drag
from qiskit.visualization import SchedStyle
from qiskit.providers.aer import AerSimulator

import qiskit.ignis.mitigation as mit

# Suppress warnings
import warnings
warnings.filterwarnings('ignore')


# Fake backend
#from qiskit.test.mock import FakeAthens


# import cma

mitiq.about()

# IBMQ.save_account()
IBMQ.load_account()

provider = IBMQ.get_provider(hub='ibm-q-bnl', group='c2qa-projects',
                             project='yao-benchmark-of')
# backend = provider.get_backend('ibmq_toronto')
backend = provider.get_backend('ibmq_montreal')
# backend = provider.get_backend('ibmq_casablanca')
properties = backend.properties()
backend_config = backend.configuration()
ham_params = backend_config.hamiltonian['vars']
dt = backend_config.dt
print(f"Sampling time: {dt*1e9} ns")

backend_defaults = backend.defaults()
inst_sched_map = backend_defaults.instruction_schedule_map
inst_sched_map.instructions


def cx_pulse_instructions(qc: int, qt: int):
    """Retrieve the CNOT pulse schedule for the given
    qubit pair from the backend defaults.

    Args:
      qc: control qubit index
      qt: target qubit index
    """
    if [qc, qt] not in backend_config.coupling_map:
        print('Qubit pair has no direct cross resonance!')
    else:
        cx = inst_sched_map.get('cx', qubits=[qc, qt])
        return cx


def Extract_GaussianSquare(q1, q2):
    """Retrieve the Gaussiacross resonance pulse waveform
    for the qubit pair from the cx (CNOT) schedule.

    Args:
      q1: control qubit index
      q2: target qubit index
    """
    cx = cx_pulse_instructions(q1, q2)
    cx1 = cx_pulse_instructions(q2, q1)

    # get longer cnot gate pulse schedule
    if cx.instructions[-1][0] < cx1.instructions[-1][0]:
        cx = cx1
    Dr_gs = []  # get the gaussiansquare pulse of drive channel
    cr_gs = []  # get the gaussiansquare pulse of control channel
    control_channel = ''  # get the corresponding control channel for q1 and q2
    Drive_Channel = ''    # get the corresponding drive channel with gaussiansquare pulse
    another_Drive_Channel = ''  # get another drive channel

    # look for them

    N = 1
    for i in range(len(cx.instructions)):
        if type(cx.instructions[i][1]) is ShiftPhase and type(cx.instructions[i][1].channel) is DriveChannel\
                and cx.instructions[i][1].phase == -math.pi/2 and N == 1:

            another_Drive_Channel = cx.instructions[i][1].channels[0]

        if type(cx.instructions[i][1].channels[0]) is ControlChannel and type(cx.instructions[i][1]) is Play\
                and type(cx.instructions[i][1].pulse) is GaussianSquare:

            cr_gs.append(cx.instructions[i][1].pulse)

            control_channel = cx.instructions[i][1].channels[0]

        if type(cx.instructions[i][1].channels[0]) is DriveChannel and type(cx.instructions[i][1]) is Play \
                and type(cx.instructions[i][1].pulse) is GaussianSquare:

            Dr_gs.append(cx.instructions[i][1].pulse)

            Drive_Channel = cx.instructions[i][1].channels[0]
        if type(cx.instructions[i][1]) is Play:
            N = 0

    return control_channel, cr_gs, Drive_Channel, Dr_gs, another_Drive_Channel


# When driving channel is phase shifted, the corresponding control channel need to be phase shifted.
def Get_Shift_phase_CRTL_Chan(q1, q2):
    """Get the driving channels' corresponding control channels

    Args:
      q1: qubit
      q2: qubit
    out_put:
      control_chan1: the corresponding control channel of driving channel with
    """
    cx1 = cx_pulse_instructions(q1, q2)
    cx2 = cx_pulse_instructions(q2, q1)
    if cx1.instructions[-1][0] > cx2.instructions[-1][0]:
        cx1, cx2 = cx2, cx1
    control_chan0 = []
    control_chan1 = []
    for i in range(len(cx2.instructions)):
        if type(cx2.instructions[i][1]) is ShiftPhase and type(cx2.instructions[i][1].channel) is ControlChannel\
                and cx2.instructions[i][1].phase == -math.pi/2:
            control_chan0.append(cx2.instructions[i][1].channel)
        if type(cx2.instructions[i][1]) is ShiftPhase and type(cx2.instructions[i][1].channel) is ControlChannel\
                and cx2.instructions[i][1].phase == -math.pi:
            control_chan1.append(cx2.instructions[i][1].channel)
        if type(cx2.instructions[i][1]) is Play:
            break

    return control_chan0, control_chan1


def Rzz_gate_schedule(q0, q1, theta):
    """
    Referenced the paper John P. T. Stenger, Nicholas T. Bronn, Daniel J. Egger, and David Pekker
    Phys. Rev. Research 3, 033171

    Args:
      q1: qubit
      q2: qubit
      theta: rotating angle
      backend: quantum hardware Device

    out_put:

      Return Rzz Pulse
    """

    uchan, cr_pulse, Dchan, dr_pulse, another_Dchan = Extract_GaussianSquare(
        q0, q1)
    #Y_chan, X_chan = Get_XY_chan(q0,q1)
    control_chan0, control_chan1 = Get_Shift_phase_CRTL_Chan(q0, q1)

    Frac = 2*np.abs(theta)/math.pi

    Y_q = Dchan.index

    X_q = another_Dchan.index

    Y90p = inst_sched_map.get('u2', P0=0, P1=0, qubits=[Y_q]).instructions

    X_180 = inst_sched_map.get('x', qubits=[X_q]).instructions[0][1].pulse

    # find out Y_90 pulse

    for Y in Y90p:

        if type(Y[1]) is Play:

            Y_pulse = Y[1].pulse

    ###

    drive_samples = Y_pulse.duration  # The duration of Y pulse

    cr_samples = cr_pulse[0].duration  # The duration gaussiansquare pulse

    cr_width = cr_pulse[0].width  # gaussiansquare pulse's width

    cr_sigma = cr_pulse[0].sigma  # gaussiansquare pulse's standard error

    cr_amp = np.abs(cr_pulse[0].amp)

    number_std = (cr_samples-cr_width)/cr_sigma

    Area_g = cr_amp*cr_sigma*np.sqrt(2*np.pi)*erf(number_std)

    Area_pi_2 = cr_width*cr_amp+Area_g

    dr_sigma = dr_pulse[0].sigma

    Area_theta = Frac * Area_pi_2

    if Area_theta > Area_g:

        New_width = (Area_theta-Area_g)/cr_amp

        new_duration = math.ceil((New_width+number_std*cr_sigma)/16)*16

        cr_pulse[0] = GaussianSquare(
            duration=new_duration, amp=cr_pulse[0].amp, sigma=cr_sigma, width=New_width)

        cr_pulse[1] = GaussianSquare(
            duration=new_duration, amp=-cr_pulse[0].amp, sigma=cr_sigma, width=New_width)

        dr_pulse[0] = GaussianSquare(
            duration=new_duration, amp=dr_pulse[0].amp, sigma=dr_sigma, width=New_width)

        dr_pulse[1] = GaussianSquare(
            duration=new_duration, amp=-dr_pulse[0].amp, sigma=dr_sigma, width=New_width)

    else:

        New_amp_cr = cr_pulse[0].amp*Area_theta/Area_g

        New_amp_dr = dr_pulse[0].amp*Area_theta/Area_g

        new_duration = number_std * cr_sigma

        cr_pulse[0] = GaussianSquare(duration=int(
            new_duration), amp=New_amp_cr, sigma=cr_sigma, width=0)

        cr_pulse[1] = GaussianSquare(duration=int(
            new_duration), amp=-New_amp_cr, sigma=cr_sigma, width=0)

        dr_pulse[0] = GaussianSquare(duration=int(
            new_duration), amp=New_amp_dr, sigma=dr_sigma, width=0)

        dr_pulse[1] = GaussianSquare(duration=int(
            new_duration), amp=-New_amp_dr, sigma=dr_sigma, width=0)


    # Set up the Rzz schedule
    if theta < 0:

        RZZ_schedule = pulse.Schedule(name="RZZ gate pulse")

        # Y_-90 pulse


        RZZ_schedule |= ShiftPhase(-math.pi, Dchan)

        for chan in control_chan1:

            RZZ_schedule |= ShiftPhase(-math.pi, chan)

        RZZ_schedule |= Play(Y_pulse, Dchan)

        RZZ_schedule |= ShiftPhase(-math.pi, Dchan) << int(drive_samples)

        for chan in control_chan1:

            RZZ_schedule |= ShiftPhase(-math.pi, chan) << int(drive_samples)

         ###

         # Cross resonant pulses and X rotation echo pulse

        RZZ_schedule |= Play(dr_pulse[0], Dchan) << int(drive_samples)
        RZZ_schedule |= Play(cr_pulse[0], uchan) << int(drive_samples)

        RZZ_schedule |= Play(X_180, another_Dchan) << int(
            new_duration+drive_samples)

        RZZ_schedule |= Play(dr_pulse[1], Dchan) << int(
            new_duration+2*drive_samples)
        RZZ_schedule |= Play(cr_pulse[1], uchan) << int(
            new_duration+2*drive_samples)

        ###

        # X_180 pulse

        RZZ_schedule |= Play(X_180, another_Dchan) << int(
            2*new_duration+2*drive_samples)

        # Y_90 pulse

        RZZ_schedule |= Play(Y_pulse, Dchan) << int(
            2*new_duration+2*drive_samples)

        return RZZ_schedule

    else:

        RZZ_schedule = pulse.Schedule(name="RZZ gate pulse")

        # Y_90 pulse

        RZZ_schedule |= Play(Y_pulse, Dchan)

        ###

        # Cross resonant pulses and X rotation echo pulse

        # print(int(drive_samples))

        RZZ_schedule |= Play(dr_pulse[0], Dchan) << int(drive_samples)
        RZZ_schedule |= Play(cr_pulse[0], uchan) << int(drive_samples)

        RZZ_schedule |= Play(X_180, another_Dchan) << int(
            new_duration+drive_samples)

        RZZ_schedule |= Play(dr_pulse[1], Dchan) << int(
            new_duration+2*drive_samples)
        RZZ_schedule |= Play(cr_pulse[1], uchan) << int(
            new_duration+2*drive_samples)

        ###

        # X_180 pulse

        RZZ_schedule |= Play(X_180, another_Dchan) << int(
            2*new_duration+2*drive_samples)

        # Y_-90 pulse

        RZZ_schedule |= ShiftPhase(-math.pi,
                                   Dchan) << int(2*new_duration+2*drive_samples)

        for chan in control_chan1:

            RZZ_schedule |= ShiftPhase(-math.pi,
                                       chan) << int(2*new_duration+2*drive_samples)

        RZZ_schedule |= Play(Y_pulse, Dchan) << int(
            2*new_duration+2*drive_samples)

        RZZ_schedule |= ShiftPhase(-math.pi,
                                   Dchan) << int(2*new_duration+3*drive_samples)

        for chan in control_chan1:

            RZZ_schedule |= ShiftPhase(-math.pi,
                                       chan) << int(2*new_duration+3*drive_samples)

        return RZZ_schedule

# Define the functions used to build the Trotter circuit.


def apply_pauli(circ, num, qb):
    if (num == 0):
        circ.i(qb)
    elif (num == 1):
        circ.x(qb)
    elif (num == 2):
        circ.y(qb)
    else:
        circ.z(qb)
    return circ


def Twirled_for_cnot(circ, qb0, qb1):

    """
    Pauli Twirling for Rzz gate. However, in order to get random Rzz gate folding in mitiq,
    we replace Rzz with Cnot gate first.

    """
    M = np.ones((4, 4))

    M[0, 1] = -1
    M[0, 2] = -1
    M[1, 0] = -1
    M[1, 3] = -1
    M[2, 0] = -1
    M[2, 3] = -1
    M[3, 1] = -1
    M[3, 2] = -1

    paulis = [(i, j) for i in range(0, 4) for j in range(0, 4)]
    paulis.remove((0, 0))
    paulis_map = [(i, j) for i in range(0, 4) for j in range(0, 4)]
    paulis_map.remove((0, 0))
    num = random.randrange(len(paulis))

    apply_pauli(circ, paulis[num][0], qb0)
    apply_pauli(circ, paulis[num][1], qb1)

    # angle=M[paulis[num][0],paulis[num][1]]*angle

    #circ.rzz(angle, qb0, qb1)
    circ.cx(qb0, qb1)

    apply_pauli(circ, paulis_map[num][0], qb0)
    apply_pauli(circ, paulis_map[num][1], qb1)


def C_not(qc, V, dt, qubits_list, cond):  # nearest neighbor coupling

    """
    Two qubit gate with Rzz's Pauli Twurling.

    Used for Trotter_circuit_5_site_c function.
    """

    num = len(qubits_list)

    for i in range(1, num-1, 2):

        Twirled_for_cnot(qc, i, i+1)

    if cond == 'per':

        Twirled_for_cnot(qc, 0, num-1)

    for i in range(0, num-1, 2):

        Twirled_for_cnot(qc, i, i+1)

# Define the functions used to build the Trotter circuit.


def h_Z_c(qc, h, dt, qubits_list, cond): # Z field

    num = len(qubits_list)

    if cond == 'per':

        for i in range(num):

            qc.rz(2*h*dt, i)

    if cond == 'op':

        for i in range(num):

            if i == 0 or i == (num-1):

                qc.rz(h*dt, i)

            else:

                qc.rz(2*h*dt, i)


def X_Rabi_c(qc, Omega, dt, qubits_list):  # Rabi coupling

    num = len(qubits_list)

    for i in range(num):

        qc.rx(2*Omega*dt, i)


def Corr_Trotter_circuit_YY_site_MG_min(h, Omega, V, T, n, L, si, sj, qubits_list, cond):

    # To calculate <PYP_i>_M=-1

    q_len = len(qubits_list)

    corr_circuits = []

    dt = T/n

    for i in range(n):

        corr_circuit = QuantumCircuit(q_len, q_len)

        corr_circuit.x(range(1, q_len, 2))

        if si in range(1, q_len, 2):

            pass

        else:

            corr_circuit.x(si)

        corr_circuit.h(si)

        corr_circuit.s(si)

        ###

        corr_circuit.barrier()

        for k in range(i):

            # Rabi coupling from the second term of Hamiltonian
            X_Rabi_c(corr_circuit, Omega, dt, qubits_list)

            h_Z_c(corr_circuit, h, dt, qubits_list, cond)  # Z field

            # Nearest neighbor hopping term
            C_not(corr_circuit, V, dt, qubits_list, cond)

            corr_circuit.barrier()

        if sj == 'even':

            corr_circuit.sdg(range(0, q_len, 2))

            corr_circuit.h(range(0, q_len, 2))

        elif sj == 'odd':

            corr_circuit.sdg(range(1, q_len, 2))

            corr_circuit.h(range(1, q_len, 2))

        for i in range(q_len):

            corr_circuit.measure(i, i)

        # Trotter_circuit = transpile(Trotter_circuit, backend) # compile into specific backend

        corr_circuits.append(corr_circuit)

    return corr_circuits


def Corr_Trotter_circuit_YY_site_MG_plus(h, Omega, V, T, n, L, si, sj, qubits_list, cond):

    # To calculate <PYP_i>_M=+1

    q_len = len(qubits_list)

    corr_circuits = []

    dt = T/n

    for i in range(n):

        corr_circuit = QuantumCircuit(q_len, q_len)

        corr_circuit.x(range(1, q_len, 2))

        if si in range(1, q_len, 2):

            corr_circuit.x(si)

        corr_circuit.h(si)

        corr_circuit.s(si)

        ###

        corr_circuit.barrier()

        for k in range(i):

            # Rabi coupling from the second term of Hamiltonian
            X_Rabi_c(corr_circuit, Omega, dt, qubits_list)

            h_Z_c(corr_circuit, h, dt, qubits_list, cond)  # Z field

            # Nearest neighbor hopping term
            C_not(corr_circuit, V, dt, qubits_list, cond)

            corr_circuit.barrier()

        if sj == 'even':

            corr_circuit.sdg(range(0, q_len, 2))

            corr_circuit.h(range(0, q_len, 2))

        elif sj == 'odd':

            corr_circuit.sdg(range(1, q_len, 2))

            corr_circuit.h(range(1, q_len, 2))

        for i in range(q_len):

            corr_circuit.measure(i, i)

        # Trotter_circuit = transpile(Trotter_circuit, backend) # compile into specific backend

        corr_circuits.append(corr_circuit)

    return corr_circuits


def Corr_Trotter_circuit_YY_site_plus(h, Omega, V, T, n, L, si, sj, qubits_list, cond):

    # To calculate <PYP_i>_+Y_j

    q_len = len(qubits_list)

    corr_circuits = []

    dt = T/n

    for i in range(n):

        corr_circuit = QuantumCircuit(q_len, q_len)

        corr_circuit.x(range(1, q_len, 2))

        corr_circuit.ry(np.pi/2, si)

        ###

        corr_circuit.barrier()

        for k in range(i):

            # Rabi coupling from the second term of Hamiltonian
            X_Rabi_c(corr_circuit, Omega, dt, qubits_list)

            h_Z_c(corr_circuit, h, dt, qubits_list, cond)  # Z field

            # Nearest neighbor hopping term
            C_not(corr_circuit, V, dt, qubits_list, cond)

            corr_circuit.barrier()

        if sj == 'even':

            corr_circuit.sdg(range(0, q_len, 2))

            corr_circuit.h(range(0, q_len, 2))

        elif sj == 'odd':

            corr_circuit.sdg(range(1, q_len, 2))

            corr_circuit.h(range(1, q_len, 2))

        for i in range(q_len):

            corr_circuit.measure(i, i)

        # Trotter_circuit = transpile(Trotter_circuit, backend) # compile into specific backend

        corr_circuits.append(corr_circuit)

    return corr_circuits


def Corr_Trotter_circuit_YY_site_min(h, Omega, V, T, n, L, si, sj, qubits_list, cond):

    # To calculate <PYP_i>_-Y_j

    q_len = len(qubits_list)

    corr_circuits = []

    dt = T/n

    for i in range(n):

        corr_circuit = QuantumCircuit(q_len, q_len)

        corr_circuit.x(range(1, q_len, 2))
        corr_circuit.ry(-np.pi/2, si)

        ###

        corr_circuit.barrier()

        for k in range(i):

            # Rabi coupling from the second term of Hamiltonian
            X_Rabi_c(corr_circuit, Omega, dt, qubits_list)

            h_Z_c(corr_circuit, h, dt, qubits_list, cond)  # Z field

            # Nearest neighbor hopping term
            C_not(corr_circuit, V, dt, qubits_list, cond)

            corr_circuit.barrier()

        if sj == 'even':

            corr_circuit.sdg(range(0, q_len, 2))

            corr_circuit.h(range(0, q_len, 2))

        elif sj == 'odd':

            corr_circuit.sdg(range(1, q_len, 2))

            corr_circuit.h(range(1, q_len, 2))

        for i in range(q_len):

            corr_circuit.measure(i, i)

        # Trotter_circuit = transpile(Trotter_circuit, backend) # compile into specific backend

        corr_circuits.append(corr_circuit)

    return corr_circuits


# number of sites
L = 19

Omega = 0.24 #(2 for chaotic regime and 0.24 for Many body scars regime)

V = -1

h = 2*V

T = 30  #  Total time (4.8 for chaotic regime and 30 for Many body scars regime)

TT = T

n = 30  # 20
NN = n

cond = 'op'

# machine dependent
qubits_list =[
        10,
        12,
        15,
        18,
        21,
        23,
        24,
        25,
        22,
        19,
        16,
        14,
        11,
        8,
        5,
        3,
        2,
        1,
        0,
        ]


assert(len(qubits_list) == L)


def DD_circuit(circuit, backend, qubits_list, initial_layout=None, name=False):

    """

      Parameters
      ----------
      circuit : qiskit circuit.
      backend : quantum hardware backend.
      qubits_list : qubit list.
      initial_layout : layerout for qubit. The default is None.

      Returns
      -------
      DD_cir : the circuit w/ dynamical decoupling.

    """

    if initial_layout:

        circuit = transpile(circuit, backend, optimization_level=0,
                            scheduling_method='asap', initial_layout=initial_layout)

    else:

        circuit = transpile(
            circuit, backend, optimization_level=0, scheduling_method='asap')


    circuit = transpile(circuit, backend, optimization_level=0,
            scheduling_method='asap', initial_layout=initial_layout)

    if name:

        DD_cir = QuantumCircuit(
            *circuit.qregs, *circuit.cregs, name=circuit.name)

    else:

        DD_cir = QuantumCircuit(*circuit.qregs, *circuit.cregs)

    for operation in circuit[:]:

        if type(operation[0]) == qiskit.circuit.delay.Delay:

            if operation[0].duration >= 400:

                q_index = operation[1][0].index

                if q_index in qubits_list:

                    t = (operation[0].duration-320)//4

                    t = math.ceil((t)/16)*16

                    DD_cir.delay(t, [q_index], 'dt')

                    DD_cir.x(q_index)

                    DD_cir.delay(2*t, [q_index], 'dt')

                    DD_cir.z(q_index)

                    DD_cir.x(q_index)

                    DD_cir.z(q_index)

                else:

                    DD_cir.data.append(operation)

            else:

                DD_cir.data.append(operation)

        else:

            DD_cir.data.append(operation)

    return DD_cir


def folding_zne_pulse(circuits, backend, V, dt, scale_factors, qubits_list,
        maxncirc=100):

    ## fold the Rzx-Rzz gate

    folded_circuits = []

    for circuit in circuits:

        folded_circuits.append([mitiq.zne.scaling.fold_gates_at_random(
            circuit, scale) for scale in scale_factors])

    folded_circuits = list(itertools.chain(*folded_circuits))

    dict_connection = {}

    for k in qubits_list:

        dict_connection[k] = 1

    X = qiskit.circuit.library.standard_gates.x.XGate

    Y = qiskit.circuit.library.standard_gates.y.YGate

    Z = qiskit.circuit.library.standard_gates.z.ZGate

    I = qiskit.circuit.library.standard_gates.i.IGate

    rzz_folded_circuits = []

    raw_rzz_folded_circuits = []

    for h in range(len(folded_circuits)):

        rzz_folded = QuantumCircuit(QuantumRegister(
            backend_config.n_qubits, 'q'), *folded_circuits[h].cregs)

        # for i,j in enumerate(folded_circuits[h][:]):
        i = 0

        while i < len(folded_circuits[h][:]):

            j = folded_circuits[h][i]

            if i+1 < len(folded_circuits[h][:]):

                y = folded_circuits[h][i+1]

            if len(j[1]) == 2:

                sign = dict_connection[qubits_list[j[1][0].index]
                                       ] * dict_connection[qubits_list[j[1][1].index]]

                rzz_folded.rzz(-2*sign*V*dt,
                               qubits_list[j[1][0].index], qubits_list[j[1][1].index])

                if len(y[1]) == 2 and (j[1][0].index, j[1][1].index) == (y[1][0].index, y[1][1].index):

                    rzz_folded.rzz(
                        2*sign*V*dt, qubits_list[j[1][0].index], qubits_list[j[1][1].index])

                    i = i+1

            else:

                qtype = type(j[0])

                if qtype in [I, X, Y, Z]:

                    if qtype in [X, Y]:

                        dict_connection[qubits_list[j[1][0].index]] = -1

                    else:

                        dict_connection[qubits_list[j[1][0].index]] = 1

                # elif qtype == qiskit.circuit.measure.Measure:

                    # qiskit.circuit.measure.Measure

                indx = j[1][0].index

                j[1][0] = qiskit.circuit.Qubit(QuantumRegister(
                    backend_config.n_qubits, 'q'), qubits_list[indx])

                rzz_folded.data.append(j)

            i = i+1

        raw_rzz_folded_circuits.append(rzz_folded)

        #rzz_folded = dd(rzz_folded,backend,qubits_list)

        for a, b in enumerate(qubits_list[1:-1:2]):

            rzz_folded.add_calibration('rzz', [
                                       b, qubits_list[2*a+2]], Rzz_gate_schedule(b, qubits_list[2*a+2], -2*V*dt), [-2*V*dt])

            rzz_folded.add_calibration('rzz', [
                                       b, qubits_list[2*a+2]], Rzz_gate_schedule(b, qubits_list[2*a+2], 2*V*dt), [2*V*dt])

        if cond == 'per':

            rzz_folded.add_calibration('rzz', [qubits_list[0], qubits_list[-1]], Rzz_gate_schedule(
                qubits_list[0], qubits_list[-1], -2*V*dt), [-2*V*dt])

            rzz_folded.add_calibration('rzz', [qubits_list[0], qubits_list[-1]], Rzz_gate_schedule(
                qubits_list[0], qubits_list[-1], 2*V*dt), [2*V*dt])

        for a, b in enumerate(qubits_list[:-1:2]):

            rzz_folded.add_calibration('rzz', [
                                       b, qubits_list[2*a+1]], Rzz_gate_schedule(b, qubits_list[2*a+1], -2*V*dt), [-2*V*dt])

            rzz_folded.add_calibration('rzz', [
                                       b, qubits_list[2*a+1]], Rzz_gate_schedule(b, qubits_list[2*a+1], 2*V*dt), [2*V*dt])

        rzz_folded = DD_circuit(rzz_folded, backend, qubits_list)

        for a, b in enumerate(qubits_list[1:-1:2]):

            rzz_folded.add_calibration('rzz', [
                                       b, qubits_list[2*a+2]], Rzz_gate_schedule(b, qubits_list[2*a+2], -2*V*dt), [-2*V*dt])

            rzz_folded.add_calibration('rzz', [
                                       b, qubits_list[2*a+2]], Rzz_gate_schedule(b, qubits_list[2*a+2], 2*V*dt), [2*V*dt])

        if cond == 'per':

            rzz_folded.add_calibration('rzz', [qubits_list[0], qubits_list[-1]], Rzz_gate_schedule(
                qubits_list[0], qubits_list[-1], -2*V*dt), [-2*V*dt])

            rzz_folded.add_calibration('rzz', [qubits_list[0], qubits_list[-1]], Rzz_gate_schedule(
                qubits_list[0], qubits_list[-1], 2*V*dt), [2*V*dt])

        for a, b in enumerate(qubits_list[:-1:2]):

            rzz_folded.add_calibration('rzz', [
                                       b, qubits_list[2*a+1]], Rzz_gate_schedule(b, qubits_list[2*a+1], -2*V*dt), [-2*V*dt])

            rzz_folded.add_calibration('rzz', [
                                       b, qubits_list[2*a+1]], Rzz_gate_schedule(b, qubits_list[2*a+1], 2*V*dt), [2*V*dt])

        rzz_folded = transpile(rzz_folded, backend,
                               optimization_level=0, scheduling_method='asap')

        rzz_folded_circuits.append(rzz_folded)


    rzz_folded_circuits_list = []
    ncirc = len(rzz_folded_circuits)
    for w in range(0, ncirc, maxncirc):
        n1 = min(ncirc, w+maxncirc)
        rzz_folded_circuits_list.append(rzz_folded_circuits[w:w+n1])

    return rzz_folded_circuits_list


meas_cal_circuits, metadata = mit.expval_meas_mitigator_circuits(
    L, method='tensored')

qr = QuantumRegister(len(qubits_list), 'q')

il = {}

for i, j in enumerate(qubits_list):

    il[qr[i]] = j


scale_factors = [1.0, 1.5, 2.0]

nums = 8  # Repeat different Twirling
nums = 1
itime = 10

Tot_r = 0

dt = T/NN
shots = 8192

# total number of circuits
nctot = NN*3*nums*4*2*(L//2)
print(f'expected total number of circuits: {nctot}')

for j in ['even', 'odd']:  # simultaneous measurements

    for i in range(1, L, 2):  # even sites
        print(f"j = {j} i = {i}")

        # Circuits for real part (Project to Y_minus)
        path = './MG_minus_pulse_id'+str(i)+j+'.dat'
        if not os.path.isfile(path) or \
                len(np.loadtxt(path, dtype=str, ndmin=1)) < itime:
            min_cirs = []
            for num in range(nums):
                corr_cirs_min = Corr_Trotter_circuit_YY_site_MG_min(
                    h, Omega, V, T, n, L, i, j, qubits_list, cond)
                min_cirs += corr_cirs_min
            print(f'num min_cirs = {len(min_cirs)}')
            corr_cirs_min_lst = folding_zne_pulse(
                min_cirs, backend, V, dt, scale_factors, qubits_list)
            print(f'num corr_cirs_min_lst = {len(corr_cirs_min_lst)}')

            # Execute Circuits for real part (Project to Y_minus)
            jids = []
            for cirs_lst in corr_cirs_min_lst:
                corr_cirs_job = execute(
                    cirs_lst, backend, optimization_level=0, shots=shots)

                print('MG_Pulse_minus'+str(i)+j+':', corr_cirs_job.job_id())
                jids.append(corr_cirs_job.job_id())
            # Save jobs' id for real part (Project to Y_minus)
            with open(path, 'a') as f:
                f.write('### new jobs\n')
                for jid in jids:
                    f.write(jid+'\n')
            del min_cirs, corr_cirs_min_lst

        # Circuits for real part (Project to Y_plus)
        path = './MG_Plus_pulse_id'+str(i)+j+'.dat'
        if not os.path.isfile(path) or \
                len(np.loadtxt(path, dtype=str, ndmin=1)) < itime:
            plus_cirs = []
            for num in range(nums):
                corr_cirs_plus = Corr_Trotter_circuit_YY_site_MG_plus(
                    h, Omega, V, T, n, L, i, j, qubits_list, cond)
                plus_cirs += corr_cirs_plus
            print(f'num plus_cirs = {len(plus_cirs)}')
            corr_cirs_plus_lst = folding_zne_pulse(
                plus_cirs, backend, V, dt, scale_factors, qubits_list)
            print(f'num corr_cirs_plus_lst = {len(corr_cirs_plus_lst)}')

            # Execute Circuits for real part (Project to Y_plus)
            jids = []
            for cirs_lst in corr_cirs_plus_lst:
                corr_cirs_i_job = execute(
                    cirs_lst, backend, optimization_level=0, shots=shots)
                print('MG_Pulse_Plus'+str(i)+j+':', corr_cirs_i_job.job_id())
                jids.append(corr_cirs_i_job.job_id())
            # Save jobs' id for real part (Project to Y_plus)
            with open(path, 'a') as f:
                f.write('### new jobs\n')
                for jid in jids:
                    f.write(jid+'\n')
            del plus_cirs, corr_cirs_plus_lst


        # Circuits for imag part (minus)
        path = './G_minus_pulse_id'+str(i)+j+'.dat'
        if not os.path.isfile(path) or \
                len(np.loadtxt(path, dtype=str, ndmin=1)) < itime:
            G_min_cirs = []
            for num in range(nums):
                corr_cirs_G_min = Corr_Trotter_circuit_YY_site_min(
                    h, Omega, V, T, n, L, i, j, qubits_list, cond)
                G_min_cirs += corr_cirs_G_min
            print(f'num G_min_cirs = {len(G_min_cirs)}')
            corr_cirs_G_min_lst = folding_zne_pulse(
                G_min_cirs, backend, V, dt, scale_factors, qubits_list)
            print(f'num corr_cirs_G_min_lst = {len(corr_cirs_G_min_lst)}')

            # Execute Circuits for imag part (mius)
            jids = []
            for cirs_lst in corr_cirs_G_min_lst:
                corr_cirs_G_job = execute(
                    cirs_lst, backend, optimization_level=0, shots=shots)
                print('G_minus'+str(i)+str(j)+':', corr_cirs_G_job.job_id())
                jids.append(corr_cirs_G_job.job_id())

            # Save jobs' id for imag part (minus)
            with open(path, 'a') as f:
                f.write('### new jobs\n')
                for jid in jids:
                    f.write(jid+'\n')
            del G_min_cirs, corr_cirs_G_min_lst

        # Circuits for imag part (plus)
        path = './G_Plus_pulse_id'+str(i)+j+'.dat'
        if not os.path.isfile(path) or \
                len(np.loadtxt(path, dtype=str, ndmin=1)) < itime:
            G_plus_cirs = []
            for num in range(nums):
                corr_cirs_G_plus = Corr_Trotter_circuit_YY_site_plus(
                    h, Omega, V, T, n, L, i, j, qubits_list, cond)
                G_plus_cirs += corr_cirs_G_plus
            print(f'num G_plus_cirs = {len(G_plus_cirs)}')
            corr_cirs_G_plus_lst = folding_zne_pulse(
                G_plus_cirs, backend, V, dt, scale_factors, qubits_list)
            print(f'num corr_cirs_G_plus_lst = {len(corr_cirs_G_plus_lst)}')

            # Execute Circuits for imag part (plus)
            jids = []
            for cirs_lst in corr_cirs_G_plus_lst:
                corr_cirs_G_i_job = execute(
                    cirs_lst, backend, optimization_level=0, shots=shots)
                print('G_plus'+str(i)+str(j)+':', corr_cirs_G_i_job.job_id())
                jids.append(corr_cirs_G_i_job.job_id())

            # Save jobs' id for imag part (plus)
            with open(path, 'a') as f:
                f.write('### new jobs\n')
                for jid in jids:
                    f.write(jid+'\n')
            del G_plus_cirs, corr_cirs_G_plus_lst
