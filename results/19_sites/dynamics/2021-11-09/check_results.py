import h5py, numpy
from scipy import integrate
from mitiq import zne
from rmfim.helper import get_scale_factors, get_qubits_list
import matplotlib.pyplot as plt



def check_results():
    # get data
    with h5py.File('analysis_result.h5', 'r') as f:
        # data_cnot0 = f['/cnot_noroc'][()]
        data_puls0 = f['/pulse_noroc'][()]
        ts = f['/trotter_0.5000_20/t_list'][()]
        data_ref = f['/trotter_0.5000_20/sz_list'][()]

    # data_cnot = get_zne_ve(data_cnot0)
    data_puls = get_zne_ve(data_puls0)

    # moving average
    # merrs_cnot = get_moving_avgerr(data_cnot, data_ref, ts)
    merrs_puls = get_moving_avgerr(data_puls, data_ref, ts)
    q_list = get_qubits_list()

    # plot

    plt.figure(figsize=(16, 12))
    for i, yref in enumerate(data_ref):
        ax = plt.subplot(4, 5, i+1)
        ax.annotate(f'q-{q_list[i]}', (3, 0.0))
        ax.plot(ts, yref, '-+', label='sv')
        #ax.errorbar(ts, data_cnot[i, :, 0], yerr=data_cnot[i, :, 1],
        #        marker='o', label='cnot')
        ax.errorbar(ts, data_puls[i, :, 0], yerr=data_puls[i, :, 1],
                marker='*', label='puls')
        ax.legend()
    plt.tight_layout()

    # moving average
    plt.figure(figsize=(8, 6))
    #plt.plot(ts, merrs_cnot, '-o', label='cnot')
    plt.plot(ts, merrs_puls, '-*', label='pulse')
    plt.legend()

    plt.show()
    # plt.savefig(f'{fname}.pdf')


def get_zne_ve(data,
        scale_factors=get_scale_factors(),
        nextrappoints=3,
        extraporder=1,
        ):

    maxpoints = len(scale_factors)
    if nextrappoints is None or nextrappoints > maxpoints:
        nextrappoints = maxpoints
    scale_factors = scale_factors[:nextrappoints]
    fac = zne.inference.PolyFactory(scale_factors, order=extraporder)

    nt, nq = data.shape[:2]
    res = numpy.zeros((nq, nt, 2))
    for iq in range(nq):
        for it in range(nt):
            if extraporder > 0:
                points = numpy.mean(data[it, iq, :, :nextrappoints], axis=0)
                val, error, _, _, _ = fac.extrapolate(scale_factors, points,
                        order=extraporder,
                        full_output=True,
                        )
            else:
                val = numpy.mean(data[it, iq, :, 0])
                error = numpy.std(data[it, iq, :, 0])
            res[iq, it, :] = val, error
    return res


def get_moving_avgerr(data, data_ref, ts):
    nt = data.shape[1]
    errs = numpy.mean((data[:,:,0]-data_ref)**2, axis=0)
    errs_int = [errs[0]]
    errs_int += [integrate.simps(errs[:k+1], ts[:k+1])/k
        for k in range(1, nt)]
    return errs_int


if __name__ == "__main__":
    check_results()
