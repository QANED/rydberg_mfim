import os, numpy
from rmfim.database import jobs
from rmfim.helper import get_qubits_list
from qiskit.ignis.mitigation.measurement import (
        tensored_meas_cal,
        TensoredMeasFitter,
        )


qubits_list = get_qubits_list()
# get job id
if os.path.isfile('cal_circ.jobid'):
    job_id = numpy.loadtxt('cal_circ.jobid', dtype=str)[()]
    if not isinstance(job_id, str):
        job_id = job_id[-1]
    cal_results = jobs.get_result_by_id(job_id)
    _, state_labels = tensored_meas_cal(mit_pattern=[[i]
            for i in qubits_list])
    meas_fitter = TensoredMeasFitter(cal_results, state_labels)
    res = meas_fitter.filter
    probs = [a.diagonal() for a in res._cal_matrices]
    for q, prob in zip(qubits_list, probs):
        print(q, prob)
    print(f' mean ro fidelity: {numpy.mean(probs)}')
    print(f'fidelity max: {numpy.max(probs)} min {numpy.min(probs)}')
