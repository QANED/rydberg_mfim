import h5py, numpy
from scipy import integrate
from mitiq import zne
from rmfim.helper import get_scale_factors
import matplotlib.pyplot as plt



def check_results():
    # get data
    with h5py.File('analysis_result.h5', 'r') as f:
        data_cnot_0 = f['/cnot_noroc_nops'][:,:,:,:3]
        data_puls_0 = f['/pulse_noroc_nops'][:,:,:,:3]
        data_cnot00 = f['/cnot_roc'][:,:,:,:3]
        data_puls00 = f['/pulse_roc'][:,:,:,:3]
        data_cnot10 = f['/cnot_noroc_ps'][:,:,:,:3]
        data_puls10 = f['/pulse_noroc_ps'][:,:,:,:3]


    with h5py.File('../2021-10-25/analysis_result.h5', 'r') as f:
        data_cnot_1 = f['/cnot_noroc_nops'][:,:,:,:3]
        data_puls_1 = f['/pulse_noroc_nops'][:,:,:,:3]
        data_cnot11 = f['/cnot_noroc_ps'][:,:,:,:3]
        data_puls11 = f['/pulse_noroc_ps'][:,:,:,:3]
        data_cnot01 = f['/cnot_roc'][:,:,:,:3]
        data_puls01 = f['/pulse_roc'][:,:,:,:3]
        ts = f['/trotter_0.5000_40/t_list'][()]
        data_ref = f['/trotter_0.5000_40/sz_list'][()]

    with h5py.File('../2021-11-14/analysis_result.h5', 'r') as f:
        data_cnot_2 = f['/cnot_noroc_nops'][:,:,:,:3]
        data_puls_2 = f['/pulse_noroc_nops'][:,:,:,:3]
        data_cnot12 = f['/cnot_noroc_ps'][:,:,:,:3]
        data_puls12 = f['/pulse_noroc_ps'][:,:,:,:3]
        data_cnot02 = f['/cnot_roc_ps'][:,:,:,:3]
        data_puls02 = f['/pulse_roc_ps'][:,:,:,:3]

    data_cnot1 = numpy.concatenate((data_cnot_0, data_cnot_1, data_cnot_2),
            axis=0)
    data_puls1 = numpy.concatenate((data_puls_0, data_puls_1, data_puls_2),
            axis=0)
    data_cnot0 = numpy.concatenate((data_cnot00, data_cnot01, data_cnot02),
            axis=0)
    data_puls0 = numpy.concatenate((data_puls00, data_puls01, data_puls02),
            axis=0)
    data_cnot2 = numpy.concatenate((data_cnot10, data_cnot11, data_cnot12),
            axis=0)
    data_puls2 = numpy.concatenate((data_puls10, data_puls11, data_puls12),
            axis=0)


    stagger = numpy.ones(data_ref.shape[0])
    stagger[1::2] *= -1
    data_cnot1 = numpy.einsum('ijkl,j->ikl', data_cnot1, stagger)/stagger.shape[0]
    data_puls1 = numpy.einsum('ijkl,j->ikl', data_puls1, stagger)/stagger.shape[0]
    data_cnot0 = numpy.einsum('ijkl,j->ikl', data_cnot0, stagger)/stagger.shape[0]
    data_puls0 = numpy.einsum('ijkl,j->ikl', data_puls0, stagger)/stagger.shape[0]
    data_cnot2 = numpy.einsum('ijkl,j->ikl', data_cnot2, stagger)/stagger.shape[0]
    data_puls2 = numpy.einsum('ijkl,j->ikl', data_puls2, stagger)/stagger.shape[0]

    data_cnot_raw = get_zne_ve(data_cnot1, extraporder=0)
    data_puls_raw = get_zne_ve(data_puls1, extraporder=0)
    data_cnot_ps = get_zne_ve(data_cnot2, extraporder=0)
    data_puls_ps = get_zne_ve(data_puls2, extraporder=0)

    data_cnot = get_zne_ve(data_cnot0)
    data_puls = get_zne_ve(data_puls0)

    # data_cnot_raw = get_zne_ve_jackknife(data_cnot1, extraporder=0)
    # data_puls_raw = get_zne_ve_jackknife(data_puls1, extraporder=0)
    # data_cnot = get_zne_ve_jackknife(data_cnot0)
    # data_puls = get_zne_ve_jackknife(data_puls0)

    # moving average
    # merrs_cnot = get_moving_avgerr(data_cnot, data_ref, ts)
    # merrs_puls = get_moving_avgerr(data_puls, data_ref, ts)

    # plot
    plt.figure(figsize=(8, 6))
    # state vector data: (z0-z1+z2-...)/N
    zstag = numpy.einsum('ij,i->j', data_ref, stagger)/stagger.shape[0]
    plt.plot(ts, zstag, '-+', label='sv')
    plt.ylabel('stagger z')
    plt.xlabel('steps')

    # pulse gate
    plt.errorbar(ts, data_cnot_raw[:, 0], yerr=data_cnot_raw[:, 1], marker='^', label='cnot: no EM ')
    plt.errorbar(ts, data_puls_raw[:, 0], yerr=data_puls_raw[:, 1], marker='^', label='puls: no EM ')
    plt.errorbar(ts, data_cnot[:, 0], yerr=data_cnot[:, 1], marker='*', label='cnot: EM ')
    plt.errorbar(ts, data_puls[:, 0], yerr=data_puls[:, 1], marker='*', label='pulse: EM ')
    plt.errorbar(ts, data_cnot_ps[:, 0], yerr=data_cnot_ps[:, 1], marker='o', label='cnot: ps ')
    plt.errorbar(ts, data_puls_ps[:, 0], yerr=data_puls_ps[:, 1], marker='o', label='puls: ps ')
    plt.legend()

    plt.show()
    # plt.savefig(f'{fname}.pdf')

    # data for I-Chi.
    data = numpy.asarray([ts, zstag, data_cnot[:, 0], data_puls[:, 0], data_cnot[:, 1], data_puls[:, 1], data_cnot_raw[:, 0], data_cnot_raw[:, 1], data_puls_raw[:, 0], data_puls_raw[:, 1], data_cnot_ps[:, 0], data_cnot_ps[:, 1], data_puls_ps[:, 0], data_puls_ps[:, 1]])
    numpy.savetxt('19q_zpi_zne.dat', data.T, fmt='%10.6f', delimiter='  ',
            header='step     ed        z_pi_cnot(zne, pauli twirling)  z_pi_pulse(zne, pauli twirling, dynamical decoupling)  err-bar_cnot  err-bar_pulse z_pi_cnot_no_em  err-bar_cnot_no_em z_pi_pulse_no_em  err-bar_pulse_no_em  z_pi_cnot_ps  err-bar_cnot_ps z_pi_pulse_ps  err-bar_pulse_ps')


def get_zne_ve(data,
        scale_factors=get_scale_factors(),
        nextrappoints=3,
        extraporder=1,
        ):

    maxpoints = len(scale_factors)
    if nextrappoints is None or nextrappoints > maxpoints:
        nextrappoints = maxpoints
    scale_factors = scale_factors[:nextrappoints]
    fac = zne.inference.PolyFactory(scale_factors, order=extraporder)

    nt = data.shape[0]
    res = numpy.zeros((nt, 2))
    for it in range(nt):
        if extraporder > 0:
            points = numpy.mean(data[it, :, :nextrappoints], axis=0)
            spread = numpy.std(data[it, :, :nextrappoints], axis=0)
            # define polynomial fucntion
            if extraporder == 1:
                func = lambda x, a, b: a*x + b
            elif extraporder == 2:
                func = lambda x, a, b, c: a*x**2 + b*x + c
            else:
                raise ValueError('extraporder != 1 or 2.')
            from scipy.optimize import curve_fit
            best_fit_ab, covar = curve_fit(func, scale_factors, points,
                               sigma = spread,
                               absolute_sigma = True,
                               )
            sigma_ab = numpy.sqrt(numpy.diagonal(covar))
            val = best_fit_ab[-1]
            error = sigma_ab[-1]
        else:
            val = numpy.mean(data[it, :, 0])
            error = numpy.std(data[it, :, 0])
        res[it, :] = val, error
    return res


def get_zne_ve_jackknife(data,
        scale_factors=get_scale_factors(),
        nextrappoints=3,
        extraporder=1,
        ):

    maxpoints = len(scale_factors)
    if nextrappoints is None or nextrappoints > maxpoints:
        nextrappoints = maxpoints
    scale_factors = scale_factors[:nextrappoints]
    fac = zne.inference.PolyFactory(scale_factors, order=extraporder)

    nt, nq, nrep = data.shape[:3]
    res = numpy.zeros((nq, nt, nrep))
    for iq in range(nq):
        for it in range(nt):
            for irep in range(nrep):
                sample = numpy.concatenate(
                        (data[it, iq, :irep, :nextrappoints],
                        data[it, iq, irep+1:, :nextrappoints]), axis=0)
                if extraporder > 0:
                    points = numpy.mean(sample, axis=0)
                    val = fac.extrapolate(scale_factors, points,
                            order=extraporder,
                            )
                else:
                    val = numpy.mean(sample[:, 0])
                res[iq, it, irep] = val
    vagerr = numpy.zeros((nq, nt, 2))
    # https://en.wikipedia.org/wiki/Jackknife_resampling
    vagerr[:, :, 0] = numpy.mean(res[:,:,:], axis=2)
    vagerr[:, :, 1] = numpy.std(res[:,:,:], axis=2)*numpy.sqrt(nrep)
    return vagerr


def get_moving_avgerr(data, data_ref, ts):
    nt = data.shape[1]
    errs = numpy.mean((data[:,:,0]-data_ref)**2, axis=0)
    errs_int = [errs[0]]
    errs_int += [integrate.simps(errs[:k+1], ts[:k+1])/k
        for k in range(1, nt)]
    return errs_int


def get_moving_avgerr2(data, data_ref, ts):
    nt = ts.shape[0]
    errs = (data-data_ref)**2
    errs_int = [errs[0]]
    errs_int += [integrate.simps(errs[:k+1], ts[:k+1])/k
        for k in range(1, nt)]
    return errs_int


if __name__ == "__main__":
    check_results()
