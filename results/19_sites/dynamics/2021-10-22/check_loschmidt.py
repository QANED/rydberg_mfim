import h5py, numpy
from scipy import integrate
from mitiq import zne
from rmfim.helper import get_scale_factors
import matplotlib.pyplot as plt



def check_results():
    # get data
    with h5py.File('analysis_result.h5', 'r') as f:
        data_cnot00 = f['/cnot_noroc_nops_t1'][:,:,:3]
        data_puls00 = f['/pulse_roc_ps_t1'][:,:,:3]

    with h5py.File('../2021-10-25/analysis_result.h5', 'r') as f:
        data_cnot01 = f['/cnot_noroc_nops_t1'][:,:,:3]
        data_puls01 = f['/pulse_roc_ps_t1'][:,:,:3]
        ts = f['/trotter_0.5000_40/t_list'][()]
        data_ref = f['/trotter_0.5000_40/loschmidt_list'][()]

    with h5py.File('../2021-11-14/analysis_result.h5', 'r') as f:
        data_cnot02 = f['/cnot_noroc_nops_t1'][:,:,:3]
        data_puls02 = f['/pulse_roc_ps_t1'][:,:,:3]

    data_cnot0 = numpy.concatenate((data_cnot00, data_cnot01, data_cnot02),
            axis=0)
    data_puls0 = numpy.concatenate((data_puls00, data_puls01, data_puls02),
            axis=0)

    data_cnot = get_zne_ve(data_cnot0)
    data_puls = get_zne_ve(data_puls0)

    # moving average
    merrs_cnot = get_moving_avgerr(data_cnot, data_ref, ts)
    merrs_puls = get_moving_avgerr(data_puls, data_ref, ts)

    # plot
    plt.figure(figsize=(8, 6))
    ax = plt.subplot(1, 1, 1)
    ax.plot(ts, data_ref, '-+', label='sv')
    ax.errorbar(ts, data_cnot[:, 0], yerr=data_cnot[:, 1],
            marker='o', label='cnot: no EM')
    ax.errorbar(ts, data_puls[:, 0], yerr=data_puls[:, 1],
            marker='*', label='puls: EM')
    ax.set_ylabel(r'$|<\Psi_t|\Psi_0>|^2$')
    # ax.set_yscale('log')
    ax.legend()

    # moving average
    plt.figure(figsize=(8, 6))
    plt.plot(ts, merrs_cnot, '-o', label='cnot')
    plt.plot(ts, merrs_puls, '-*', label='pulse')
    plt.legend()

    plt.show()
    # plt.savefig(f'{fname}.pdf')


def get_zne_ve(data,
        scale_factors=get_scale_factors(),
        nextrappoints=3,
        extraporder=1,
        ):

    maxpoints = len(scale_factors)
    if nextrappoints is None or nextrappoints > maxpoints:
        nextrappoints = maxpoints
    scale_factors = scale_factors[:nextrappoints]
    fac = zne.inference.PolyFactory(scale_factors, order=extraporder)

    nt, nq = data.shape[:2]
    res = numpy.zeros((nt, 2))
    for it in range(nt):
        if extraporder > 0:
            points = numpy.mean(data[it, :, :nextrappoints], axis=0)
            spread = numpy.std(data[it, :, :nextrappoints], axis=0)
            # define polynomial fucntion
            if extraporder == 1:
                func = lambda x, a, b: a*x + b
            elif extraporder == 2:
                func = lambda x, a, b, c: a*x**2 + b*x + c
            else:
                raise ValueError('extraporder != 1 or 2.')
            from scipy.optimize import curve_fit
            best_fit_ab, covar = curve_fit(func, scale_factors, points,
                               # sigma = spread,
                               # absolute_sigma = True,
                               )
            sigma_ab = numpy.sqrt(numpy.diagonal(covar))
            val = best_fit_ab[-1]
            error = sigma_ab[-1]
        else:
            val = numpy.mean(data[it, :, 0])
            error = numpy.std(data[it, :, 0])
        res[it, :] = val, error
    return res


def get_moving_avgerr(data, data_ref, ts):
    nt = data.shape[0]
    errs = (data[:,0]-data_ref)**2
    errs_int = [errs[0]]
    errs_int += [integrate.simps(errs[:k+1], ts[:k+1])/k
        for k in range(1, nt)]
    return errs_int


if __name__ == "__main__":
    check_results()
