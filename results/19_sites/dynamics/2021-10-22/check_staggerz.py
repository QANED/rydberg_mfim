import h5py, numpy
from scipy import integrate
from mitiq import zne
from rmfim.helper import get_scale_factors
import matplotlib.pyplot as plt



def check_results():
    # get data
    with h5py.File('analysis_result.h5', 'r') as f:
        data_cnot_0 = f['/cnot_noroc_nops'][:,:,:,:3]
        data_puls_0 = f['/pulse_noroc_nops'][:,:,:,:3]
        data_cnot00 = f['/cnot_roc'][:,:,:,:3]
        data_puls00 = f['/pulse_roc'][:,:,:,:3]

    with h5py.File('../2021-10-25/analysis_result.h5', 'r') as f:
        data_cnot_1 = f['/cnot_noroc_nops'][:,:,:,:3]
        data_puls_1 = f['/pulse_noroc_nops'][:,:,:,:3]
        data_cnot01 = f['/cnot_roc'][:,:,:,:3]
        data_puls01 = f['/pulse_roc'][:,:,:,:3]
        ts = f['/trotter_0.5000_40/t_list'][()]
        data_ref = f['/trotter_0.5000_40/sz_list'][()]

    with h5py.File('../2021-11-14/analysis_result.h5', 'r') as f:
        data_cnot_2 = f['/cnot_noroc_nops'][:,:,:,:3]
        data_puls_2 = f['/pulse_noroc_nops'][:,:,:,:3]
        data_cnot02 = f['/cnot_roc_ps'][:,:,:,:3]
        data_puls02 = f['/pulse_roc_ps'][:,:,:,:3]

    data_cnot1 = numpy.concatenate((data_cnot_0, data_cnot_1, data_cnot_2),
            axis=0)
    data_puls1 = numpy.concatenate((data_puls_0, data_puls_1, data_puls_2),
            axis=0)
    data_cnot0 = numpy.concatenate((data_cnot00, data_cnot01, data_cnot02),
            axis=0)
    data_puls0 = numpy.concatenate((data_puls00, data_puls01, data_puls02),
            axis=0)

    data_cnot_raw = get_zne_ve(data_cnot1, extraporder=0)
    data_puls_raw = get_zne_ve(data_puls1, extraporder=0)
    data_cnot = get_zne_ve(data_cnot0)
    data_puls = get_zne_ve(data_puls0)

    # moving average
    merrs_cnot = get_moving_avgerr(data_cnot, data_ref, ts)
    merrs_puls = get_moving_avgerr(data_puls, data_ref, ts)

   # plot
    plt.figure(figsize=(8, 6))
    # get staggered moments
    stagger = numpy.ones(data_ref.shape[0])
    stagger[1::2] *= -1
    # state vector data: (z0-z1+z2-...)/N
    zstag = numpy.einsum('ij,i->j', data_ref, stagger)/stagger.shape[0]
    plt.plot(ts, zstag, '-+', label='sv')
    plt.ylabel('stagger z')
    plt.xlabel('steps')
    # pulse gate
    zstag_raw = numpy.einsum('ij,i->j', data_cnot_raw[:, :, 0], stagger)/stagger.  shape[0]
    zstag_raw2 = numpy.einsum('ij,i->j', data_puls_raw[:, :, 0], stagger)/stagger.  shape[0]
    zstag20 = numpy.einsum('ij,i->j', data_cnot[:, :, 0], stagger)/stagger.shape[0]
    zstag21 = numpy.einsum('ij,i->j', data_puls[:, :, 0], stagger)/stagger.shape[0]
    errs_raw = numpy.mean(data_cnot_raw[:, :, 1], axis=0)
    errs_raw2 = numpy.mean(data_puls_raw[:, :, 1], axis=0)
    errs0 = numpy.mean(data_cnot[:, :, 1], axis=0)
    errs1 = numpy.mean(data_puls[:, :, 1], axis=0)
    plt.errorbar(ts, zstag_raw, yerr=errs_raw, marker='*', label='cnot: no EM ')
    plt.errorbar(ts, zstag_raw2, yerr=errs_raw2, marker='o', label='puls: no EM ')
    plt.errorbar(ts, zstag20, yerr=errs0, marker='*', label='cnot: EM ')
    plt.errorbar(ts, zstag21, yerr=errs1, marker='*', label='pulse: EM ')

    plt.legend()


    # moving average variance
    plt.figure(figsize=(8, 6))
    merrs_cnot = get_moving_avgerr2(zstag20, zstag, ts)
    merrs_puls = get_moving_avgerr2(zstag21, zstag, ts)
    plt.plot(ts, merrs_cnot, '-o', label='cnot')
    plt.plot(ts, merrs_puls, '-*', label='pulse')
    plt.legend()

    plt.show()
    # plt.savefig(f'{fname}.pdf')

    data = numpy.asarray([ts, zstag, zstag20, zstag21, errs0, errs1, zstag_raw, errs_raw, zstag_raw2, errs_raw2])
    numpy.savetxt('zpi.dat', data.T, fmt='%10.6f', delimiter='  ',
            header='step     ed        z_pi_cnot(zne, pauli twirling)       z_pi_pulse(zne, pauli twirling, dynamical decoupling)  err-bar_cnot  err-bar_pulse z_pi_cnot_no_em  err-bar_cnot_no_em  z_pi_puls_no_em  err-bar_puls_no_em')


def get_zne_ve(data,
        scale_factors=get_scale_factors(),
        nextrappoints=3,
        extraporder=1,
        ):

    maxpoints = len(scale_factors)
    if nextrappoints is None or nextrappoints > maxpoints:
        nextrappoints = maxpoints
    scale_factors = scale_factors[:nextrappoints]
    fac = zne.inference.PolyFactory(scale_factors, order=extraporder)

    nt, nq = data.shape[:2]
    res = numpy.zeros((nq, nt, 2))
    for iq in range(nq):
        for it in range(nt):
            if extraporder > 0:
                points = numpy.mean(data[it, iq, :, :nextrappoints], axis=0)
                val, error, _, _, _ = fac.extrapolate(scale_factors, points,
                        order=extraporder,
                        full_output=True,
                        )
            else:
                val = numpy.mean(data[it, iq, :, 0])
                error = numpy.std(data[it, iq, :, 0])
            res[iq, it, :] = val, error
    return res


def get_moving_avgerr(data, data_ref, ts):
    nt = data.shape[1]
    errs = numpy.mean((data[:,:,0]-data_ref)**2, axis=0)
    errs_int = [errs[0]]
    errs_int += [integrate.simps(errs[:k+1], ts[:k+1])/k
        for k in range(1, nt)]
    return errs_int


def get_moving_avgerr2(data, data_ref, ts):
    nt = ts.shape[0]
    errs = (data-data_ref)**2
    errs_int = [errs[0]]
    errs_int += [integrate.simps(errs[:k+1], ts[:k+1])/k
        for k in range(1, nt)]
    return errs_int


if __name__ == "__main__":
    check_results()
