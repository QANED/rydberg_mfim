import h5py, numpy
from scipy import integrate
from mitiq import zne
from rmfim.helper import get_scale_factors
import matplotlib.pyplot as plt



def check_results():
    # get data
    with h5py.File('analysis_result.h5', 'r') as f:
        data_cnot00 = f['/cnot_roc_ps_t1'][()]
        data_cnot10 = f['/cnot_roc_ps_t2'][()]
        data_puls00 = f['/pulse_roc_ps_t1'][()]
        data_puls10 = f['/pulse_roc_ps_t2'][()]
        ts = f['/trotter_0.5000_40/t_list'][()]
        data_ref = f['/trotter_0.5000_40/loschmidt_list'][()]

    with h5py.File('../20211018_2/analysis_result.h5', 'r') as f:
        data_cnot01 = f['/cnot_roc_ps_t1'][()]
        data_cnot11 = f['/cnot_roc_ps_t2'][()]
        data_puls01 = f['/pulse_roc_ps_t1'][()]
        data_puls11 = f['/pulse_roc_ps_t2'][()]


    # get a particular color in the color sequence
    import matplotlib as mpl
    green = mpl.rcParams['axes.prop_cycle']._left[2]['color']
    cnext = mpl.rcParams['axes.prop_cycle']._left[3]['color']

    data_cnot0 = numpy.concatenate((data_cnot00, data_cnot01), axis=0)
    data_cnot1 = numpy.concatenate((data_cnot10, data_cnot11), axis=0)
    data_puls0 = numpy.concatenate((data_puls00, data_puls01), axis=0)
    data_puls1 = numpy.concatenate((data_puls10, data_puls11), axis=0)

    data_cnot = get_zne_ve(data_cnot0)
    data_cnot2 = get_zne_ve(data_cnot1)
    data_puls = get_zne_ve(data_puls0)
    data_puls2 = get_zne_ve(data_puls1)

    # moving average
    merrs_cnot = get_moving_avgerr(data_cnot, data_ref, ts)
    merrs_puls = get_moving_avgerr(data_puls, data_ref, ts)

    # plot

    plt.figure(figsize=(8, 6))
    ax = plt.subplot(1, 1, 1)
    ax.plot(ts, data_ref, '-+', label='sv')
    ax.errorbar(ts, data_cnot[:, 0], yerr=data_cnot[:, 1],
            marker='o', label='cnot EM',
            # marker='*', label='pulse: no EM',
            color=green,
            )
    ax.errorbar(ts, data_puls[:, 0], yerr=data_puls[:, 1],
            marker='*', label='pulse: EM ',
            color=cnext,
            )
    ax.errorbar(ts, data_cnot2[:, 0], yerr=data_cnot2[:, 1],
            marker='h', label='cnot-1flip EM',
            # marker='*', label='pulse: no EM',
            color='blue',
            )
    ax.errorbar(ts, data_puls2[:, 0], yerr=data_puls2[:, 1],
            marker='+', label='pulse-1flip: EM ',
            color='blue',
            )
    ax.set_ylabel(r'$|<\Psi_t|\Psi_0>|^2$')
    ax.legend()

    # moving average
    plt.figure(figsize=(8, 6))
    # dummy plot to get color consistent
    plt.plot(ts, merrs_cnot, ':')

    plt.plot(ts, merrs_cnot,
            '-o', label='cnot: EM',
            # '-*', label='pulse: no EM', color=green,
            )
    plt.plot(ts, merrs_puls,
            #'-*', label='pulse',
            '-*', label='pulse: EM', color=cnext,
            )
    plt.legend()

    plt.show()
    # plt.savefig(f'{fname}.pdf')

    # data for I-Chi.
    data = numpy.asarray([ts, data_ref, data_cnot[:, 0], data_puls[:, 0], data_cnot[:, 1], data_puls[:, 1],
            data_cnot2[:, 0], data_puls2[:, 0], data_cnot2[:, 1], data_puls2[:, 1]])
    numpy.savetxt('12q_loschmidt.dat', data.T, fmt='%10.6f', delimiter='  ',
            header='step  ed  loschmidt_cnot  loschmidt_pulse  err-bar_cnot  err-bar_pulse  loschmidt_cnot_1flip loschmidt_pulse_1flip err-bar_cnot_1flip err-bar_pulse_1flip')


def get_zne_ve(data,
        scale_factors=get_scale_factors(),
        nextrappoints=3,
        extraporder=1,
        ):

    maxpoints = len(scale_factors)
    if nextrappoints is None or nextrappoints > maxpoints:
        nextrappoints = maxpoints
    scale_factors = scale_factors[:nextrappoints]
    fac = zne.inference.PolyFactory(scale_factors, order=extraporder)

    nt, nq = data.shape[:2]
    res = numpy.zeros((nt, 2))
    for it in range(nt):
        if extraporder > 0:
            points = numpy.mean(data[it, :, :nextrappoints], axis=0)
            spread = numpy.std(data[it, :, :nextrappoints], axis=0)
            # define polynomial fucntion
            if extraporder == 1:
                func = lambda x, a, b: a*x + b
            elif extraporder == 2:
                func = lambda x, a, b, c: a*x**2 + b*x + c
            else:
                raise ValueError('extraporder != 1 or 2.')
            from scipy.optimize import curve_fit
            best_fit_ab, covar = curve_fit(func, scale_factors, points,
                               sigma = spread,
                               absolute_sigma = True,
                               )
            sigma_ab = numpy.sqrt(numpy.diagonal(covar))
            val = best_fit_ab[-1]
            error = sigma_ab[-1]
        else:
            val = numpy.mean(data[it, :, 0])
            error = numpy.std(data[it, :, 0])
        res[it, :] = val, error
    return res


def get_moving_avgerr(data, data_ref, ts):
    nt = data.shape[0]
    errs = (data[:,0]-data_ref)**2
    errs_int = [errs[0]]
    errs_int += [integrate.simps(errs[:k+1], ts[:k+1])/k
        for k in range(1, nt)]
    return errs_int


if __name__ == "__main__":
    check_results()
