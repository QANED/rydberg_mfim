import h5py, numpy
from scipy import integrate
from mitiq import zne
from rmfim.helper import get_scale_factors
import matplotlib.pyplot as plt



def check_results():
    # get data
    with h5py.File('analysis_result.h5', 'r') as f:
        data_cnot_0 = f['/cnot_noroc_nops'][()]
        data_puls_0 = f['/pulse_noroc_nops'][()]
        data_cnot10 = f['/cnot_noroc_ps'][()]
        data_puls10 = f['/pulse_noroc_ps'][()]

        data_cnot00 = f['/cnot_roc'][()]
        data_puls00 = f['/pulse_roc'][()]
        ts = f['/trotter_0.5000_40/t_list'][()]
        data_ref = f['/trotter_0.5000_40/sz_list'][()]

    with h5py.File('../20211018_2/analysis_result.h5', 'r') as f:
        data_cnot_1 = f['/cnot_noroc_nops'][()]
        data_puls_1 = f['/pulse_noroc_nops'][()]
        data_cnot11 = f['/cnot_noroc_ps'][()]
        data_puls11 = f['/pulse_noroc_ps'][()]

        data_cnot01 = f['/cnot_roc'][()]
        data_puls01 = f['/pulse_roc'][()]


    # get a particular color in the color sequence
    import matplotlib as mpl
    green = mpl.rcParams['axes.prop_cycle']._left[2]['color']
    cnext = mpl.rcParams['axes.prop_cycle']._left[3]['color']

    data_cnot1 = numpy.concatenate((data_cnot_0, data_cnot_1), axis=0)
    data_puls1 = numpy.concatenate((data_puls_0, data_puls_1), axis=0)
    data_cnot2 = numpy.concatenate((data_cnot10, data_cnot11), axis=0)
    data_puls2 = numpy.concatenate((data_puls10, data_puls11), axis=0)

    data_cnot0 = numpy.concatenate((data_cnot00, data_cnot01), axis=0)
    data_puls0 = numpy.concatenate((data_puls00, data_puls01), axis=0)

    data_cnot_raw = get_zne_ve(data_cnot1, extraporder=0)
    data_puls_raw = get_zne_ve(data_puls1, extraporder=0)
    data_cnot_ps = get_zne_ve(data_cnot2, extraporder=0)
    data_puls_ps = get_zne_ve(data_puls2, extraporder=0)

    data_cnot = get_zne_ve(data_cnot0)
    data_puls = get_zne_ve(data_puls0)

    numpy.savetxt('12q_zlist_trotter.dat', data_ref, fmt='%10.6f')
    numpy.savetxt('12q_zlist_cnot_no_em.dat', data_cnot_raw[:,:,0], fmt='%10.6f')
    numpy.savetxt('12q_zlist_cnot_no_em_error.dat', data_cnot_raw[:,:,1], fmt='%10.6f')
    numpy.savetxt('12q_zlist_puls_no_em.dat', data_puls_raw[:,:,0], fmt='%10.6f')
    numpy.savetxt('12q_zlist_puls_no_em_error.dat', data_puls_raw[:,:,1], fmt='%10.6f')
    numpy.savetxt('12q_zlist_cnot_ps_no_em.dat', data_cnot_ps[:,:,0], fmt='%10.6f')
    numpy.savetxt('12q_zlist_cnot_ps_no_em_error.dat', data_cnot_ps[:,:,1], fmt='%10.6f')
    numpy.savetxt('12q_zlist_puls_ps_no_em.dat', data_puls_ps[:,:,0], fmt='%10.6f')
    numpy.savetxt('12q_zlist_puls_ps_no_em_error.dat', data_puls_ps[:,:,1], fmt='%10.6f')

    numpy.savetxt('12q_zlist_cnot_zne_pauli_twilling_dd.dat', data_cnot[:,:,0], fmt='%10.6f')
    numpy.savetxt('12q_zlist_cnot_zne_pauli_twilling_dd_error.dat', data_cnot[:,:,1], fmt='%10.6f')
    numpy.savetxt('12q_zlist_puls_zne_pauli_twilling_dd.dat', data_puls[:,:,0], fmt='%10.6f')
    numpy.savetxt('12q_zlist_puls_zne_pauli_twilling_dd_error.dat', data_puls[:,:,1], fmt='%10.6f')

    # moving average
    merrs_cnot = get_moving_avgerr(data_cnot, data_ref, ts)
    merrs_puls = get_moving_avgerr(data_puls, data_ref, ts)

    # plot

    plt.figure(figsize=(16, 12))
    for i, yref in enumerate(data_ref):
        ax = plt.subplot(3, 4, i+1)
        ax.plot(ts, yref, '-+', label='sv')
        ax.errorbar(ts, data_cnot_raw[i, :, 0], yerr=data_cnot_raw[i, :, 1],
                marker='*', label='cnot: no EM',
                color=green,
                )
        ax.errorbar(ts, data_puls_raw[i, :, 0], yerr=data_puls_raw[i, :, 1],
                marker='*', label='puls: no EM',
                color=green,
                )
        ax.errorbar(ts, data_cnot_ps[i, :, 0], yerr=data_cnot_ps[i, :, 1],
                marker='o', label='cnot: ps',
                color='blue',
                )
        ax.errorbar(ts, data_puls_ps[i, :, 0], yerr=data_puls_ps[i, :, 1],
                marker='*', label='puls: ps',
                color='blue',
                )

        ax.errorbar(ts, data_cnot[i, :, 0], yerr=data_cnot[i, :, 1],
                # marker='o', label='cnot',
                marker='*', label='cnot: EM',
                color=green,
                )
        ax.errorbar(ts, data_puls[i, :, 0], yerr=data_puls[i, :, 1],
                marker='*', label='pulse: EM ',
                color=cnext,
                )
        ax.legend()
    plt.tight_layout()

    # moving average
    plt.figure(figsize=(8, 6))
    # dummy plot to get color consistent
    plt.plot(ts, merrs_cnot, ':')

    plt.plot(ts, merrs_cnot,
            '-*', label='cnot: EM', color=green,
            )
    plt.plot(ts, merrs_puls,
            '-*', label='pulse: EM', color=cnext,
            )
    plt.legend()

    plt.show()
    # plt.savefig(f'{fname}.pdf')


def get_zne_ve(data,
        scale_factors=get_scale_factors(),
        nextrappoints=3,
        extraporder=1,
        ):

    maxpoints = len(scale_factors)
    if nextrappoints is None or nextrappoints > maxpoints:
        nextrappoints = maxpoints
    scale_factors = scale_factors[:nextrappoints]
    fac = zne.inference.PolyFactory(scale_factors, order=extraporder)

    nt, nq = data.shape[:2]
    res = numpy.zeros((nq, nt, 2))
    for iq in range(nq):
        for it in range(nt):
            if extraporder > 0:
                points = numpy.mean(data[it, iq, :, :nextrappoints], axis=0)
                spread = numpy.std(data[it, iq, :, :nextrappoints], axis=0)
                # define polynomial fucntion
                if extraporder == 1:
                    func = lambda x, a, b: a*x + b
                elif extraporder == 2:
                    func = lambda x, a, b, c: a*x**2 + b*x + c
                else:
                    raise ValueError('extraporder != 1 or 2.')
                from scipy.optimize import curve_fit
                best_fit_ab, covar = curve_fit(func, scale_factors, points,
                                   sigma = spread,
                                   absolute_sigma = True,
                                   )
                sigma_ab = numpy.sqrt(numpy.diagonal(covar))
                val = best_fit_ab[-1]
                error = sigma_ab[-1]
            else:
                val = numpy.mean(data[it, iq, :, 0])
                error = numpy.std(data[it, iq, :, 0])
            res[iq, it, :] = val, error
    return res


def get_moving_avgerr(data, data_ref, ts):
    nt = data.shape[1]
    errs = numpy.mean((data[:,:,0]-data_ref)**2, axis=0)
    errs_int = [errs[0]]
    errs_int += [integrate.simps(errs[:k+1], ts[:k+1])/k
        for k in range(1, nt)]
    return errs_int


if __name__ == "__main__":
    check_results()
