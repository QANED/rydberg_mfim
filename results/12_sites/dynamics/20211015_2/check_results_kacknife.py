import h5py, numpy
from scipy.optimize import curve_fit
from scipy import integrate
from mitiq import zne
from rmfim.helper import get_scale_factors
import matplotlib.pyplot as plt



def check_results():
    # get data

    path_cnot = '/cnot_roc'
    path_pulse = '/pulse_roc'

    path_cnot = '/cnot_noroc_nops'
    path_pulse = '/pulse_noroc_nops'

    with h5py.File('analysis_result.h5', 'r') as f:
        data_cnot01 = f[path_cnot][()]
        data_puls01 = f[path_pulse][()]
        ts = f['/trotter_0.5000_40/t_list'][()]
        data_ref = f['/trotter_0.5000_40/sz_list'][()]

    with h5py.File('../20211018_2/analysis_result.h5', 'r') as f:
        data_cnot02 = f[path_cnot][()]
        data_puls02 = f[path_pulse][()]

    data_cnot0 = numpy.concatenate((data_cnot01, data_cnot02), axis=0)
    data_puls0 = numpy.concatenate((data_puls01, data_puls02), axis=0)

    # zne data
    data_cnot, avgerr_cnot = get_zne_ve(data_cnot0,
            nextrappoints=1,
            )
    data_puls, avgerr_puls = get_zne_ve(data_puls0,
            nextrappoints=1,
            )
    # moving average
    mavgerr_cnot = get_moving_avgerr(data_cnot, data_ref, ts)
    mavgerr_puls = get_moving_avgerr(data_puls, data_ref, ts)

    # plot
    plt.figure(figsize=(16, 12))
    for i, yref in enumerate(data_ref):
        ax = plt.subplot(3, 4, i+1)
        ax.plot(ts, yref, '-+', label='sv')
        ax.errorbar(ts, avgerr_cnot[i, :, 0], yerr=avgerr_cnot[i, :, 1],
                marker='o', label='cnot')
        ax.errorbar(ts, avgerr_puls[i, :, 0], yerr=avgerr_puls[i, :, 1],
                marker='*', label='puls')
        ax.legend()
    plt.tight_layout()

    # moving average
    plt.figure(figsize=(8, 6))
    plt.errorbar(ts, mavgerr_cnot[:,0], yerr=mavgerr_cnot[:,1],
            marker='o', label='cnot')
    plt.errorbar(ts, mavgerr_puls[:,0], yerr=mavgerr_puls[:,1],
            marker='*', label='pulse')
    plt.legend()

    plt.show()
    # plt.savefig(f'{fname}.pdf')



def get_zne_ve(data,
        scale_factors=get_scale_factors(),
        nextrappoints=3,
        extraporder=1,
        ):

    maxpoints = len(scale_factors)
    if nextrappoints is None or nextrappoints > maxpoints:
        nextrappoints = maxpoints
    scale_factors = scale_factors[:nextrappoints]
    fac = zne.inference.PolyFactory(scale_factors, order=extraporder)

    nt, nq, nrep = data.shape[:3]
    res = numpy.zeros((nq, nt, nrep))
    for iq in range(nq):
        for it in range(nt):
            for irep in range(nrep):
                sample = numpy.concatenate(
                        (data[it, iq, :irep, :nextrappoints],
                        data[it, iq, irep+1:, :nextrappoints]), axis=0)
                if extraporder > 0:
                    points = numpy.mean(sample, axis=0)
                    val = fac.extrapolate(scale_factors, points,
                            order=extraporder,
                            )
                else:
                    val = numpy.mean(sample[:, 0])
                res[iq, it, irep] = val
    vagerr = numpy.zeros((nq, nt, 2))
    vagerr[:, :, 0] = numpy.mean(res[:,:,:], axis=2)
    vagerr[:, :, 1] = numpy.std(res[:,:,:], axis=2)
    return res, vagerr


def get_moving_avgerr(data, data_ref, ts):
    nt, nrep = data.shape[1:3]
    res = numpy.zeros((nrep, nt))
    for irep in range(nrep):
        errs = numpy.mean((data[:,:,irep]-data_ref)**2, axis=0)
        errs_int = [errs[0]]
        errs_int += [integrate.simps(errs[:k+1], ts[:k+1])/k
            for k in range(1, nt)]
        res[irep, :] = errs_int
    avrerr = numpy.zeros((nt, 2))
    avrerr[:, 0] = numpy.mean(res, axis=0)
    avrerr[:, 1] = numpy.std(res, axis=0)
    return avrerr


def extrapolate0(xs, ys):
    # not successful
    coefs, _ = zne.inference.mitiq_polyfit(xs, ys, 1)
    def fun(x, a, b, c):
        return a*x**b + c

    p0 = [coefs[0], 1.0, coefs[1]]
    popt, pcov = curve_fit(fun, xs, ys, p0=p0, maxfev=8000)

    return popt[-1], 0.1  #numpy.sqrt(pcov[-1, -1])


def extrapolate1(xs, ys, order):
    # not helpful
    coefs, _ = zne.inference.mitiq_polyfit(xs, ys, order)
    def fun(x, *args):
        return numpy.polyval(args, x)

    bounds = ([-numpy.inf for s in coefs], [numpy.inf for s in coefs])
    bounds[0][-1], bounds[1][-1] = -1, 1
    if coefs[-1] > 1:
        coefs[-1] = 1
    elif coefs[-1] < -1:
        coefs[-1] = -1

    popt, pcov = curve_fit(fun, xs, ys, p0=coefs, bounds=bounds)
    return popt[-1], numpy.sqrt(pcov[-1, -1])


if __name__ == "__main__":
    check_results()
