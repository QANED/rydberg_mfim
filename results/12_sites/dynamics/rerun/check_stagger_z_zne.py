import h5py, numpy
from rmfim.helper import get_scale_factors
import matplotlib.pyplot as plt



def check_results():
    # get data
    with h5py.File('analysis_result.h5', 'r') as f:
        data_cnot = f['/cnot_roc_ps'][()]
        data_puls = f['/pulse_roc_ps'][()]

    # staggered moment
    stagger = numpy.ones(data_cnot.shape[1])
    stagger[1::2] *= -1
    stagger *= -1
    data_cnot = numpy.einsum('ijkl,j->ikl', data_cnot, stagger)/stagger.shape[0]
    data_puls = numpy.einsum('ijkl,j->ikl', data_puls, stagger)/stagger.shape[0]

    # zne extrapolation
    data_cnot = get_zne_ve(data_cnot)
    data_puls = get_zne_ve(data_puls)

    # time mesh, up to a constant to be set.
    ts = numpy.arange(data_puls.shape[0])

    # plot
    plt.figure(figsize=(8, 6))
    # state vector data: (z0-z1+z2-...)/N
    # zstag = numpy.einsum('ij,i->j', data_ref, stagger)/stagger.shape[0]
    # plt.plot(ts, zstag, '-+', label='sv')
    plt.ylabel(r'$z_\pi$')
    plt.xlabel('steps')

    # pulse gate
    plt.errorbar(ts, data_cnot[:, 0], yerr=data_cnot[:, 1], marker='*', label='cnot: EM ')
    plt.errorbar(ts, data_puls[:, 0], yerr=data_puls[:, 1], marker='*', label='pulse: EM ')

    plt.legend()

    plt.show()
    # plt.savefig(f'{fname}.pdf')

    # data for I-Chi.
    # data = numpy.asarray([ts, zstag, data_cnot[:, 0], data_puls[:, 0], data_cnot[:, 1], data_puls[:, 1], data_cnot_raw[:, 0], data_cnot_raw[:, 1], data_puls_raw[:, 0], data_puls_raw[:, 1]])
    # numpy.savetxt('12q_zpi_zne.dat', data.T, fmt='%10.6f', delimiter='  ',
    #        header='step     ed        z_pi_cnot(zne, pauli twirling)  z_pi_pulse(zne, pauli twirling, dynamical decoupling)  err-bar_cnot  err-bar_pulse z_pi_cnot_no_em  err-bar_cnot_no_em z_pi_pulse_no_em  err-bar_pulse_no_em')


def get_zne_ve(data,
        scale_factors=get_scale_factors(),
        nextrappoints=3,
        extraporder=1,
        ):

    maxpoints = len(scale_factors)
    if nextrappoints is None or nextrappoints > maxpoints:
        nextrappoints = maxpoints
    scale_factors = scale_factors[:nextrappoints]

    nt = data.shape[0]
    res = numpy.zeros((nt, 2))
    for it in range(nt):
        if extraporder > 0:
            points = numpy.mean(data[it, :, :nextrappoints], axis=0)
            spread = numpy.std(data[it, :, :nextrappoints], axis=0)
            # define polynomial fucntion
            if extraporder == 1:
                func = lambda x, a, b: a*x + b
            elif extraporder == 2:
                func = lambda x, a, b, c: a*x**2 + b*x + c
            else:
                raise ValueError('extraporder != 1 or 2.')
            from scipy.optimize import curve_fit
            best_fit_ab, covar = curve_fit(func, scale_factors, points,
                               sigma = spread,
                               absolute_sigma = True,
                               )
            sigma_ab = numpy.sqrt(numpy.diagonal(covar))
            val = best_fit_ab[-1]
            error = sigma_ab[-1]

        else:
            val = numpy.mean(data[it, :, 0])
            error = numpy.std(data[it, :, 0])
        res[it, :] = val, error
    return res



if __name__ == "__main__":
    check_results()
