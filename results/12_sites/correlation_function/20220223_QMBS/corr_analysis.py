import numpy, h5py, os
from scipy.optimize import curve_fit
import qiskit.ignis.mitigation as mit
from rmfim.database import jobs
from rmfim.helper import get_backend, get_qubits_list, get_scale_factors
import matplotlib.pyplot as plt


def stabilizer_coeff_pauli(stabilizer):
    """Return the 1 or -1 coeff and Pauli label."""
    coeff = 1
    pauli = coeff
    if stabilizer[0] == '-':
        coeff = -1
    if stabilizer[0] in ['+', '-']:
        pauli = stabilizer[1:]
    else:
        pauli = stabilizer
    return coeff, pauli


def stabilizer_measure_diagonal(stabilizer):
    """Return the diagonal vector for a stabilizer measurement.

    Args:
        stabilizer (str): a stabilizer string

    Returns:
        numpy.ndarray: the diagonal for measurement in the stabilizer basis.
    """
    coeff, pauli = stabilizer_coeff_pauli(stabilizer)
    diag = numpy.array([1])
    for s in reversed(pauli):
        if s == 'I':
            tmp = numpy.array([1, 1])
        else:
            tmp = numpy.array([1, -1])
        diag = numpy.kron(tmp, diag)
    return coeff * diag


def measure_expval(result_lst, stab, meas_mitigator=None):
    result_num = len(result_lst)
    expvals = []
    diagonal = stabilizer_measure_diagonal(stab)
    for i in range(result_num):
        expval, stddev = mit.expectation_value(result_lst[i],
                diagonal=diagonal,
                meas_mitigator=meas_mitigator)
        expvals.append(expval)
    return numpy.asarray(expvals)


def calc_PYP_expval(results, backend, scale_factors, qubits_list,
        site_i, even_odd, meas_filter=None, Num=1,
        ):
    # in qiskit convention, P_i = 1 + Z_i
    L = len(qubits_list)
    Rzz_device_cir_result_counts = []
    for res in results:
        Rzz_device_cir_result_counts += res.get_counts()

    # Num twirlings
    num_result = len(Rzz_device_cir_result_counts)//Num
    result_list = Rzz_device_cir_result_counts
    # time steps
    num_scale = len(scale_factors)
    Original_num = num_result//num_scale

    expectation_values = 0
    if even_odd=='even':
        kstart = 0
    else:
        kstart = 1
    for k in range(kstart, L, 2):
        if k < 2:
            left = 0
        else:
            left = k-1
        if k > L-3:
            right = 0
        else:
            right = L-2-k
        if k == 0:
            stab = ['YI'+'I'*right,'YZ'+'I'*right]
        elif k==L-1:
            stab = ['I'*left+'IY','I'*left+'ZY']
        else:
            stab = ['I'*left+'IYI'+'I'*right,'I'*left+'ZYI'+'I'*right,
                    'I'*left+'IYZ'+'I'*right,'I'*left+'ZYZ'+'I'*right]
        expval = 0
        for stab_str in stab:
            expval1 = measure_expval(result_list, stab_str[::-1])
            expval += numpy.asarray(expval1)
        expval /= len(stab)
        expectation_values += ((-1)**(site_i+k))*expval
    y_data = expectation_values.reshape((Num, Original_num, num_scale))
    return y_data


def calc_hsave_data(num = 10):
    if os.path.isfile('results.h5'):
        print('data already saved!')
        return
    scale_factors = get_scale_factors()
    qubits_list = get_qubits_list()
    backend = get_backend()
    L = len(qubits_list)

    # ro mitigation
    jid = numpy.loadtxt('cal_circ.jobid', dtype=str, ndmin=1)[-1]
    res = jobs.get_result_by_id(jid)
    _, metadata = mit.expval_meas_mitigator_circuits(L, method='tensored')
    fitter = mit.ExpvalMeasMitigatorFitter(res, metadata).fit()

    data = []
    for i in range(1,L,2):
        data.append([])
        for j in ['even','odd']:
            data[-1].append([])
            ### retrieve real part and calculate the result from them (Project to plus)
            paths = ['./MG_Plus_pulse_id'+str(i)+j+'.dat',
                    './MG_minus_pulse_id'+str(i)+j+'.dat',
                    './G_Plus_pulse_id'+str(i)+j+'.dat',
                    './G_minus_pulse_id'+str(i)+j+'.dat',
                    ]
            for k, path in enumerate(paths):
                data[-1][-1].append([])
                jids = numpy.loadtxt(path, dtype=str, ndmin=1)
                assert(len(jids) == num)
                results = jobs.get_results_by_ids(jids)
                res = calc_PYP_expval(results,
                        backend, scale_factors, qubits_list, i, j,
                        meas_filter=fitter, Num=num)
                data[-1][-1][-1].append(res)

    with h5py.File('results.h5', 'w') as f:
        f['/data'] = data


def zne_linear(xs, ys):
    # xs = scale factors [1, 1.5, 2]
    # ys: shape (ntwirls, nscale)
    func = lambda x, a, b: a * x + b
    ymean = ys.mean(axis=0)
    ystd = ys.std(axis=0)
    if ys.shape[0] > 1:
        popt, pcov = curve_fit(func, xs, ymean,
                sigma=ystd,
                absolute_sigma=True,
                )
    else:
        popt, pcov = curve_fit(func, xs, ymean,
                )
    return popt[1], numpy.sqrt(pcov[1, 1])


def anaysis(
        T=15*2,
        ntwirls=None,
        ):
    with h5py.File('results.h5', 'r') as f:
        data = f['/data'][()]
    # /data                    Dataset {6, 2, 4, 1, 10, 30, 3}
    if ntwirls is not None:
        data = data[:,:,:,:,:ntwirls,:,:]
    nt = data.shape[5]
    scale_factors = get_scale_factors()
    corrs = numpy.zeros((nt, 2))
    errs = numpy.zeros((nt, 2))
    for i, di in enumerate(data):
        for j, dij in enumerate(di):
            for k, dijk in enumerate(dij):
                for l in range(nt):
                    val, err = zne_linear(scale_factors, dijk[0,:,l,:])
                    if k == 0:
                        corrs[l,0] += val/2
                    elif k == 1:
                        corrs[l,0] -= val/2
                    elif k == 2:
                        corrs[l,1] += val/2
                    else:
                        corrs[l,1] -= val/2
                    if k < 2:
                        errs[l, 0] += err**2
                    else:
                        errs[l, 1] += err**2
    cy = numpy.sqrt(numpy.sum(corrs**2, axis=1))
    cyerr = numpy.sqrt(numpy.sum(corrs**2*errs, axis=1))/cy
    times = numpy.linspace(0, T, nt)
    with open('YY_corr_imag_12site_scar.npy', 'rb') as f:
        cy_i = numpy.load(f)
    with open('YY_corr_real_12site_scar.npy', 'rb') as f:
        cy_r = numpy.load(f)
    cy_trotter = numpy.sqrt(cy_r**2 + cy_i**2)

    numpy.savetxt('cy_qmbs.dat',
            numpy.asarray([times, cy_trotter, cy, cyerr,
            corrs[:,0], numpy.sqrt(errs[:,0]),
            corrs[:,1], numpy.sqrt(errs[:,1])]).T,
            fmt='%10.6f',
            header='step  trotter  cy-device  err cy-real err cy-imag err',
            )

    plt.figure()
    plt.plot(times, cy_trotter, "go-",c='k',label='Ideal Trotter')
    plt.errorbar(times, cy, yerr=cyerr, fmt='bo--', color='r', label='$R_{ZX}$')
    plt.grid()
    plt.legend()

    plt.figure()
    plt.plot(times, cy_r, "go-",c='k',label='Ideal Trotter')
    plt.errorbar(times, corrs[:,0], yerr=numpy.sqrt(errs[:,0]),
            fmt='bo--', color='r', label='$R_{ZX}$')

    plt.figure()
    plt.plot(times, cy_i, "go-",c='k',label='Ideal Trotter')
    plt.errorbar(times, corrs[:,1], yerr=numpy.sqrt(errs[:,1]),
            fmt='bo--', color='r', label='$R_{ZX}$')

    plt.show()


if __name__ == "__main__":
    # calc_hsave_data()
    anaysis(ntwirls=None)
