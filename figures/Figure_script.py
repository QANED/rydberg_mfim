import numpy as np
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16})  # enlarge matplotlib fonts

# Import qubit states Zero (|0>) and One (|1>), and Pauli operators (X, Y, Z)
import math


# Suppress warnings
import warnings
warnings.filterwarnings('ignore')

from qiskit import QuantumCircuit, QuantumRegister, IBMQ, execute, transpile
from qiskit.providers.aer import QasmSimulator
from qiskit.tools.monitor import job_monitor
from qiskit.circuit import Parameter

# Import state tomography modules
from qiskit.ignis.verification.tomography import state_tomography_circuits, StateTomographyFitter

from qiskit.ignis.verification.tomography import process_tomography_circuits, ProcessTomographyFitter

from qiskit.quantum_info import state_fidelity

import math
import numpy as np
from scipy.special import erf

# Qiskit Pulse imports
import qiskit.pulse
from qiskit import pulse
from qiskit.pulse import library

from qiskit import *
from qiskit.pulse import Play, Schedule, DriveChannel, ControlChannel, Waveform, ShiftPhase
from qiskit.pulse.library import drag, GaussianSquare, Drag

import qiskit
import qiskit.quantum_info as qi


from qiskit.ignis.mitigation.measurement import complete_meas_cal, CompleteMeasFitter

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from qiskit.visualization.pulse_v2.stylesheet import IQXStandard
from qiskit import IBMQ
 
IBMQ.save_account("",overwrite=True)
IBMQ.load_account()

provider = IBMQ.get_provider(hub='', group='', project='')
backend = provider.get_backend('')
properties = backend.properties()

backend_config = backend.configuration()
ham_params = backend_config.hamiltonian['vars']
dt = backend_config.dt
print(f"Sampling time: {dt*1e9} ns")

backend_defaults = backend.defaults()
inst_sched_map = backend_defaults.instruction_schedule_map 
inst_sched_map.instructions

def g(d,ei,ef):
    """
    integration is written by myself with the Simpson's rule and 3/8 Simpson's rule. Unlike Scipy's, this code can directly deal
with array with initial point and final point.
    # d: the array before integrated
    # ei: start point
    # ef: ending point
    """
    N=d.shape[0]-1
    h=(ef-ei)/N
    return ig(d,N+1,h)

def ig(d,N,h): 
    #using the Simpson's rule
    """
    The integration is written by myself with the Simpson's rule and 3/8 Simpson's rule. Unlike Scipy's, this code can directly deal
with array with initial point and final point.
    """
    if N == 1:
        return 0
    elif N==0:
        return 0
    elif N == 2:
        return (d[0]+d[1])*h/2
    elif N == 3:
        return (d[0]+4*d[1]+d[2])*h/3
    elif N == 4:
        return (d[0]+3*d[1]+3*d[2]+d[3])*3*h/8
    elif (N+1)%2 == 0:
        return ((d[N-1]+d[0]+sum(d[1::2])*4+sum(d[2:N-1:2])*2))*h/3
    elif (N+1)%2 == 1:
        return (sum(d[1:N-3:2])*4+sum(d[2:N-5:2])*2+d[0]+d[N-4])*h/3+ig(d[-4:],4,h)

def cx_pulse_instructions(qc: int, qt: int):
    """Retrieve the CNOT pulse schedule for the given
    qubit pair from the backend defaults.
    
    Args:
      qc: control qubit index
      qt: target qubit index
    """
    if [qc, qt] not in backend_config.coupling_map:
        print('Qubit pair has no direct cross resonance!')
    else:
        cx = inst_sched_map.get('cx', qubits=[qc, qt])
        return cx

def Exract_GaussianSquare(q1, q2):
    """Retrieve the Gaussiacross resonance pulse waveform
    for the qubit pair from the cx (CNOT) schedule.
    
    Args:
      q1: control qubit index
      q2: target qubit index
    """
    cx = cx_pulse_instructions(q1, q2)
    cx1 = cx_pulse_instructions(q2, q1)
    if cx.instructions[-1][0] < cx1.instructions[-1][0]:
        cx=cx1
    idx = 0
    Dr_gs = []
    cr_gs = []
    control_channel=''
    Drive_Channel=''
    another_Drive_Channel=''
    #look for first Play instruction on a ControlChannel
    N = 1
    for i in range(len(cx.instructions)):
        if type(cx.instructions[i][1]) is ShiftPhase and type(cx.instructions[i][1].channel) is DriveChannel\
        and cx.instructions[i][1].phase == -math.pi/2 and N == 1 :
            
            another_Drive_Channel=cx.instructions[i][1].channels[0]
            
        if type(cx.instructions[i][1].channels[0]) is ControlChannel and type(cx.instructions[i][1]) is Play\
          and type(cx.instructions[i][1].pulse) is GaussianSquare:

            cr_gs.append(cx.instructions[i][1].pulse)

            control_channel=cx.instructions[i][1].channels[0]

        if type(cx.instructions[i][1].channels[0]) is DriveChannel and type(cx.instructions[i][1]) is Play \
          and type(cx.instructions[i][1].pulse) is GaussianSquare:

            Dr_gs.append(cx.instructions[i][1].pulse)

            Drive_Channel=cx.instructions[i][1].channels[0]
        if type(cx.instructions[i][1]) is Play:
            N=0

    return control_channel, cr_gs, Drive_Channel, Dr_gs, another_Drive_Channel


def Get_Shift_phase_CRTL_Chan(q1, q2):
    """Get q1 and q2 corresponding control channel
    
    Args:
      q1: small one
      qt: target qubit index
    """
    cx1 = cx_pulse_instructions(q1, q2)
    cx2 = cx_pulse_instructions(q2, q1)
    if cx1.instructions[-1][0] > cx2.instructions[-1][0]:
        cx1, cx2 = cx2, cx1
    control_chan0 = []
    control_chan1 = []
    for i in range(len(cx2.instructions)):
        if type(cx2.instructions[i][1]) is ShiftPhase and type(cx2.instructions[i][1].channel) is ControlChannel\
        and cx2.instructions[i][1].phase == -math.pi/2 :
            control_chan0.append(cx2.instructions[i][1].channel)
        if type(cx2.instructions[i][1]) is ShiftPhase and type(cx2.instructions[i][1].channel) is ControlChannel\
        and cx2.instructions[i][1].phase == -math.pi :
            control_chan1.append(cx2.instructions[i][1].channel)
        if type(cx2.instructions[i][1]) is Play:
            break

    return control_chan0, control_chan1


def Rzz_gate_schedule (q0,q1,theta):
    
    """
    Referenced the paper John P. T. Stenger, Nicholas T. Bronn, Daniel J. Egger, and David Pekker
    Phys. Rev. Research 3, 033171 
    
    Args:
      q1: qubit
      q2: qubit
      theta: rotating angle
      backend: quantum hardware Device
      
    out_put:
        
      Return Rzz Pulse
    """

    
    uchan, cr_pulse, Dchan, dr_pulse, another_Dchan = Exract_GaussianSquare(q0, q1)
    #Y_chan, X_chan = Get_XY_chan(q0,q1)
    control_chan0, control_chan1 = Get_Shift_phase_CRTL_Chan(q0, q1)
    
    Frac = 2*np.abs(theta)/math.pi
    
    Y_q = Dchan.index
    
    X_q = another_Dchan.index
    
    Y90p = inst_sched_map.get('u2', P0=0, P1=0, qubits=[Y_q]).instructions
    
    X_180 = inst_sched_map.get('x', qubits=[X_q]).instructions[0][1].pulse
    
    ### find out Y_90 pulse 
    
    for Y in Y90p:
        
        if type(Y[1]) is Play:
            
            Y_pulse = Y[1].pulse
            
    ###
            
    drive_samples = Y_pulse.duration
    
    cr_samples = cr_pulse[0].duration
    
    cr_width= cr_pulse[0].width
    
    cr_sigma = cr_pulse[0].sigma
    
    cr_amp = np.abs(cr_pulse[0].amp)
    
    number_std = (cr_samples-cr_width)/cr_sigma
    
    Area_g = cr_amp*cr_sigma*np.sqrt(2*np.pi)*erf(number_std) # paper
    
    Area_pi_2 = cr_width*cr_amp+Area_g 
    
    dr_sigma = dr_pulse[0].sigma
    
    Area_theta = Frac * Area_pi_2
    
    if Area_theta > Area_g:
        
        New_width = (Area_theta-Area_g)/cr_amp
        
        new_duration = math.ceil((New_width+number_std*cr_sigma)/16)*16
        
        cr_pulse[0] = GaussianSquare(duration=new_duration, amp=cr_pulse[0].amp, sigma=cr_sigma, width=New_width)
        
        cr_pulse[1] = GaussianSquare(duration=new_duration, amp=-cr_pulse[0].amp, sigma=cr_sigma, width=New_width)
        
        dr_pulse[0] = GaussianSquare(duration=new_duration, amp=dr_pulse[0].amp, sigma=dr_sigma, width=New_width)
        
        dr_pulse[1] = GaussianSquare(duration=new_duration, amp=-dr_pulse[0].amp, sigma=dr_sigma, width=New_width)
        
        
    else:
        
        New_amp_cr =  cr_pulse[0].amp*Area_theta/Area_g
        
        New_amp_dr= dr_pulse[0].amp*Area_theta/Area_g
        
        new_duration = number_std * cr_sigma
        
        cr_pulse[0] = GaussianSquare(duration=int(new_duration), amp=New_amp_cr, sigma=cr_sigma, width=0)
        
        cr_pulse[1] = GaussianSquare(duration=int(new_duration), amp=-New_amp_cr, sigma=cr_sigma, width=0)
        
        dr_pulse[0] = GaussianSquare(duration=int(new_duration), amp=New_amp_dr, sigma=dr_sigma, width=0)
        
        dr_pulse[1] = GaussianSquare(duration=int(new_duration), amp=-New_amp_dr, sigma=dr_sigma, width=0)
        
    # Set up the Rzz schedule
    if theta > 0 :
  
        RZZ_schedule = pulse.Schedule(name="RZZ gate pulse") 
    
        ### Y_-90 pulse
    
        RZZ_schedule |= ShiftPhase(-math.pi, Dchan) 
    
        for chan in control_chan1:
            
            RZZ_schedule |= ShiftPhase(-math.pi, chan) 
    
        RZZ_schedule |= Play(Y_pulse, Dchan)
    
        RZZ_schedule |= ShiftPhase(-math.pi, Dchan) << int(drive_samples)
    
        for chan in control_chan1: 
        
            RZZ_schedule |= ShiftPhase(-math.pi, chan) << int(drive_samples)
        
         ### 
    
         ### Cross resonant pulses and X rotation echo pulse
    
        RZZ_schedule |= Play(dr_pulse[1], Dchan) << int(drive_samples)
        RZZ_schedule |= Play(cr_pulse[1], uchan) << int(drive_samples)
    
        RZZ_schedule |= Play(X_180, another_Dchan) << int(new_duration+drive_samples)

        RZZ_schedule |= Play(dr_pulse[0], Dchan) << int(new_duration+2*drive_samples)
        RZZ_schedule |= Play(cr_pulse[0], uchan) << int(new_duration+2*drive_samples)
    
         ###
    
         ### X_180 pulse
    
        RZZ_schedule |= Play(X_180, another_Dchan) << int(2*new_duration+2*drive_samples)
    
         ## Y_90 pulse

        RZZ_schedule |= Play(Y_pulse, Dchan) << int(2*new_duration+2*drive_samples)
    
        return RZZ_schedule
    
    else:
        
        RZZ_schedule = pulse.Schedule(name="RZZ gate pulse") 
    
        ### Y_-90 pulse
    
        RZZ_schedule |= Play(Y_pulse, Dchan)
    
        
         ### 
    
         ### Cross resonant pulses and X rotation echo pulse
    
        RZZ_schedule |= Play(dr_pulse[1], Dchan) << int(drive_samples)
        RZZ_schedule |= Play(cr_pulse[1], uchan) << int(drive_samples)
    
        RZZ_schedule |= Play(X_180, another_Dchan) << int(new_duration+drive_samples)

        RZZ_schedule |= Play(dr_pulse[0], Dchan) << int(new_duration+2*drive_samples)
        RZZ_schedule |= Play(cr_pulse[0], uchan) << int(new_duration+2*drive_samples)
    
         ###
    
         ### X_180 pulse
    
        RZZ_schedule |= Play(X_180, another_Dchan) << int(2*new_duration+2*drive_samples)
    
         ## Y_90 pulse
        
        RZZ_schedule |= ShiftPhase(-math.pi, Dchan) << int(2*new_duration+2*drive_samples)
        
        for chan in control_chan1:
            
            RZZ_schedule |= ShiftPhase(-math.pi, chan) << int(2*new_duration+2*drive_samples)
        
        RZZ_schedule |= Play(Y_pulse, Dchan) << int(2*new_duration+2*drive_samples)
        
        RZZ_schedule |= ShiftPhase(-math.pi, Dchan) << int(2*new_duration+3*drive_samples)
    
        for chan in control_chan1: 
        
            RZZ_schedule |= ShiftPhase(-math.pi, chan) << int(2*new_duration+3*drive_samples)
    
        return RZZ_schedule
    

    
def draw_2cnot_pulse():
    qr=QuantumRegister(2)
    qc=QuantumCircuit(qr, name='Two cnot Rzz gate')
    qc.cx(qr[0],qr[1])
    qc.rz(2.0,qr[1])
    qc.cx(qr[0],qr[1])
    
    init_layout = {}
    
    for i,j in enumerate([1,3]):
    
        init_layout[qr[i]] = j
    
    qc=transpile(qc, backend, initial_layout=init_layout)
    qc_schedule=schedule(qc, backend)
    my_style = {
                'formatter.text_size.axis_label': 24,
        'formatter.text_size.annotate': 16,
        'formatter.text_size.frame_change': 24,
        'formatter.text_size.snapshot': 1,
        'formatter.text_size.fig_title': 24,
        'formatter.text_size.axis_break_symbol': 15
            }
    style = IQXStandard(**my_style)
    return qc_schedule.draw(style=style,filename='2_cnot.png')

def draw_Rzx_pulse():
    my_style = {
                'formatter.text_size.axis_label': 24,
        'formatter.text_size.annotate': 18,
        'formatter.text_size.frame_change': 24,
        'formatter.text_size.snapshot': 24,
        'formatter.text_size.fig_title': 24,
        'formatter.text_size.axis_break_symbol': 15
            }

    style = IQXStandard(**my_style)
    return Rzz_gate_schedule (1,3,2.0).draw(style=style)

def duration_plot():
    cnt_qc=[]
    pulse_qc=[]
    ang_list = [0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8,2.0,2.2,2.4]
    for ang in ang_list:
        qr=QuantumRegister(2)
        qc=QuantumCircuit(qr, name='Two cnot Rzz gate')
        qc.cx(qr[0],qr[1])
        qc.rz(ang,qr[1])
        qc.cx(qr[0],qr[1])
    
        init_layout = {}
    
        for i,j in enumerate([1,3]):
    
            init_layout[qr[i]] = j
    
        qc=transpile(qc, backend, initial_layout=init_layout)
        qc_schedule=schedule(qc, backend)
        #my_style = {'formatter.text_size.axis_label': 20,
        #            }
        
        cnt_qc.append(qc_schedule.duration*2/9)
        
        pulse_qc.append(Rzz_gate_schedule (1,3,ang).duration*2/9)
        
    plt.scatter(ang_list,cnt_qc,label='2-CNOT',color='r')
    plt.scatter(ang_list,pulse_qc,label='$R_{ZX}$',color='b')
    
    plt.grid()
    plt.legend()
    plt.show()
    plt.savefig('Duration_comparison.png')

#12_site_zpi
def site_12_zpi_plot_mit():
    times=np.linspace(0,39,40)
    f = open("12q_zlist_trotter.DAT", "r")
    Z_i = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_Trotter=0
    for site in range(1,13):
        Z_pi_Trotter+=((-1)**(site))*np.array(Z_i[site])
        
    f = open("12q_zlist_puls_zne_pauli_twilling_dd_error.DAT", "r")
    Z_i_puls_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_puls_error[j]=num_list
        num_string= f.readline()
        j=j+1
    
    f = open("12q_zlist_cnot_zne_pauli_twilling_error.DAT", "r")
    Z_i_cnt_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_cnt_error[j]=num_list
        num_string= f.readline()
        j=j+1
    # Error propagation
    Z_pi_cnt_error=0
    Z_pi_puls_error=0
    for site in range(1,13):
        Z_pi_cnt_error+=(np.array(Z_i_cnt_error[site])**2)
        Z_pi_puls_error+=(np.array(Z_i_puls_error[site])**2)
    Z_pi_cnt_error=np.sqrt(Z_pi_cnt_error)/12
    Z_pi_puls_error=np.sqrt(Z_pi_puls_error)/12
    
    f = open("12q_zpi_zne.DAT", "r")
    time=[]
    Z_pi_Trotter=[]
    Z_pi_cnt=[]
    Z_pi_puls=[]
    Z_pi_cnt_error=[]
    Z_pi_puls_error=[]
    print(f.readline())
    num_string = f.readline()
    while num_string != '':
        num = num_string.split()
        time.append(float(num[0]))
        Z_pi_Trotter.append(float(num[1]))
        Z_pi_cnt.append(float(num[2]))
        Z_pi_puls.append(float(num[3]))
        Z_pi_cnt_error.append(float(num[4]))
        Z_pi_puls_error.append(float(num[5]))
        num_string = f.readline()
    
    plt.plot(times,Z_pi_Trotter,'go-',c='k', label='Ideal Trotter')
    #plt.plot(time,Z_pi_Trotter/12,'go--',c='g', label='Trotter Simulation')
    #plt.plot(time,Z_pi_cnot,c='r')
    plt.errorbar(times,Z_pi_cnt, yerr=Z_pi_cnt_error,fmt='go-',color='r', ecolor='r')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(times,Z_pi_puls, yerr=Z_pi_puls_error,fmt='go--', color='b', ecolor='b')
    plt.legend(loc=4)
    plt.ylim(-1.15, 0.9)
    plt.xlim(-1, 41.0)
    plt.grid()
    plt.show()
    plt.savefig('12q_z_pi_mit.png')
    
def site_12_zpi_plot_mit_accumulate_err_m_mit_zi():
    times=np.linspace(0,39,40)
    f = open("12q_zlist_cnot_zne_pauli_twilling_dd.DAT", "r")
    Z_i_cnt = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_cnt[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_cnt=0
    for site in range(1,13):
        Z_pi_cnt+=((-1)**(site))*np.array(Z_i_cnt[site])
        
    f = open("12q_zlist_puls_zne_pauli_twilling_dd.DAT", "r")
    Z_i_puls = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_puls[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_puls=0
    for site in range(1,13):
        Z_pi_puls+=((-1)**(site))*np.array(Z_i_puls[site])
        
    f = open("12q_zlist_trotter.DAT", "r")
    Z_i = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_Trotter=0
    for site in range(1,13):
        Z_pi_Trotter+=((-1)**(site))*np.array(Z_i[site])
        
    f = open("12q_zlist_puls_zne_pauli_twilling_dd_error.DAT", "r")
    Z_i_puls_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_puls_error[j]=num_list
        num_string= f.readline()
        j=j+1
        
    f = open("12q_zlist_cnot_zne_pauli_twilling_dd_error.DAT", "r")
    Z_i_cnt_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_cnt_error[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Int_D_puls=[]
    Int_D_cnt=[]
    for i,j in enumerate(times):
        #print(i)
        D_puls=0
        D_cnt=0
        for k in range(1,13):
            D_puls+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_puls[k][:i+1]))**2
            D_cnt+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_cnt[k][:i+1]))**2
        D_puls=D_puls/12
        D_cnt=D_cnt/12
        if j == 0:
            Int_D_puls.append(0)
            Int_D_cnt.append(0)
        else:
            Int_D_puls.append(g(D_puls,0,j)/j)
            Int_D_cnt.append(g(D_cnt,0,j)/j)
            
    u_cnt=0
    u_puls=0
    for k in range(1,13):
        u_puls+=4*((np.array(Z_i_puls_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_puls_error[k])*np.array(Z_i_puls[k]))**2)
        u_cnt+=4*((np.array(Z_i_cnt_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_cnt_error[k])*np.array(Z_i_cnt[k]))**2)
    u_puls=np.sqrt(u_puls)/12
    u_cnt=np.sqrt(u_cnt)/12
    
    Int_D_puls_err=[0]
    Int_D_cnt_err=[0]
    A_puls=0
    A_cnt=0
    for i in range(len(times)-1):
        A_puls+=(u_puls[i]**2+u_puls[i+1]**2)
        A_cnt+=(u_cnt[i]**2+u_cnt[i+1]**2)
        Int_D_puls_err.append(np.sqrt(A_puls)*0.5/(times[i]+1.0))
        Int_D_cnt_err.append(np.sqrt(A_cnt)*0.5/(times[i]+1.0))
        
    plt.errorbar(times, Int_D_cnt,yerr=Int_D_cnt_err,fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(times,Int_D_puls, yerr=Int_D_puls_err,fmt='go--', color='b', ecolor='b', label='$R_{ZX}$')
    plt.legend(loc=4)
    plt.ylim(-0.0, 0.205)
    plt.xlim(-1, 41)
    plt.grid()
    plt.show()
    plt.savefig('12q_z_p_mit_acc_err.png')
    
    
    for i in range(1,13):
        #plt.title('Qubit'+ str(qubits_list[i-1]))
        plt.plot(times,Z_i[i],'go-', color='k', label='Ideal Trotter')
        plt.errorbar(times,Z_i_cnt[i],yerr=Z_i_cnt_error[i],fmt='go-',color='r', label='2-CNOT')
        plt.errorbar(times,Z_i_puls[i],yerr=Z_i_puls_error[i],fmt='go-', color='b', label='$R_{ZX}$')
        
        plt.ylim(-1.4,1.2)
        plt.legend()
        plt.grid()
        plt.show()
        plt.savefig('12q_z_p_mit'+str(i)+'.png')
    
    
def site_19_zpi_plot_mit():
    times=np.linspace(0,39,40)
    f = open("19_zlist_trotter.DAT", "r")
    Z_i_Trotter = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_Trotter[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_Trotter=0
    for site in range(1,20):
        Z_pi_Trotter+=((-1)**(site+1))*np.array(Z_i_Trotter[site])
        
    f = open("19q_zlist_puls_zne_pauli_twilling_dd_error.DAT", "r")
    Z_i_puls_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_puls_error[j]=num_list
        num_string= f.readline()
        j=j+1
        
    f = open("19q_zlist_cnot_zne_pauli_twilling_error.DAT", "r")
    Z_i_cnt_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_cnt_error[j]=num_list
        num_string= f.readline()
        j=j+1
    
    # Error propagation
    Z_pi_cnt_error=0
    Z_pi_puls_error=0
    for site in range(1,20):
        Z_pi_cnt_error+=np.array(Z_i_cnt_error[site])**2
        Z_pi_puls_error+=np.array(Z_i_puls_error[site])**2
    Z_pi_cnt_error=np.sqrt(Z_pi_cnt_error)/19
    Z_pi_puls_error=np.sqrt(Z_pi_puls_error)/19
    
    f = open("19q_zpi_zne.DAT", "r")
    time=[]
    Ed_Z_pi=[]
    Z_pi_cnot=[]
    Z_pi_pulse=[]
    Z_pi_cnt_error=[]
    Z_pi_puls_error=[]
    print(f.readline())
    num_string = f.readline()
    while num_string != '':
        num = num_string.split()
        time.append(float(num[0]))
        Ed_Z_pi.append(float(num[1]))
        Z_pi_cnot.append(float(num[2]))
        Z_pi_pulse.append(float(num[3]))
        Z_pi_cnt_error.append(float(num[4]))
        Z_pi_puls_error.append(float(num[5]))
        num_string = f.readline()
        
    plt.plot(times,Ed_Z_pi,'go-',c='k', label='Ideal Trotter')
    #plt.plot(time,Z_pi_Trotter/19,'go--',c='g', label='Trotter Simulation')
    #plt.plot(time,Z_pi_cnot,c='r')
    plt.errorbar(times,Z_pi_cnot, yerr=Z_pi_cnt_error,fmt='go-',color='r', ecolor='r')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(times,Z_pi_pulse, yerr=Z_pi_puls_error,fmt='go--', color='b', ecolor='b')
    plt.legend(loc=4)
    plt.ylim(-1.15, 0.9)
    plt.xlim(-1, 41)
    plt.grid()
    plt.show()
    plt.savefig('19q_z_pi_mit.png')
    
def site_19_zpi_plot_mit_accumulate_err_m_mit_zi():
    times=np.linspace(0,39,40)
    f = open("19q_zlist_cnot_zne_pauli_twilling_dd.DAT", "r")
    Z_i_cnt = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_cnt[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_cnt=0
    for site in range(1,20):
        Z_pi_cnt+=((-1)**(site))*np.array(Z_i_cnt[site])
        
    f = open("19_zlist_puls_zne_pauli_twilling_dd.DAT", "r")
    Z_i_puls = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_puls[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_puls=0
    for site in range(1,20):
        Z_pi_puls+=((-1)**(site))*np.array(Z_i_puls[site])
        
    f = open("19_zlist_trotter.DAT", "r")
    Z_i = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_Trotter=0
    for site in range(1,19):
        Z_pi_Trotter+=((-1)**(site))*np.array(Z_i[site])
        
    f = open("19q_zlist_puls_zne_pauli_twilling_dd_error.DAT", "r")
    Z_i_puls_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_puls_error[j]=num_list
        num_string= f.readline()
        j=j+1
        
    f = open("19q_zlist_cnot_zne_pauli_twilling_dd_error.DAT", "r")
    Z_i_cnt_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_cnt_error[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Int_D_puls=[]
    Int_D_cnt=[]
    for i,j in enumerate(times):
        #print(i)
        D_puls=0
        D_cnt=0
        for k in range(1,20):
            D_puls+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_puls[k][:i+1]))**2
            D_cnt+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_cnt[k][:i+1]))**2
        D_puls=D_puls/19
        D_cnt=D_cnt/19
        if j == 0:
            Int_D_puls.append(0)
            Int_D_cnt.append(0)
        else:
            Int_D_puls.append(g(D_puls,0,j)/j)
            Int_D_cnt.append(g(D_cnt,0,j)/j)
            
    u_cnt=0
    u_puls=0
    for k in range(1,20):
        u_puls+=4*((np.array(Z_i_puls_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_puls_error[k])*np.array(Z_i_puls[k]))**2)
        u_cnt+=4*((np.array(Z_i_cnt_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_cnt_error[k])*np.array(Z_i_cnt[k]))**2)
    u_puls=np.sqrt(u_puls)/19
    u_cnt=np.sqrt(u_cnt)/19
    
    Int_D_puls_err=[0]
    Int_D_cnt_err=[0]
    A_puls=0
    A_cnt=0
    for i in range(len(times)-1):
        A_puls+=(u_puls[i]**2+u_puls[i+1]**2)
        A_cnt+=(u_cnt[i]**2+u_cnt[i+1]**2)
        Int_D_puls_err.append(np.sqrt(A_puls)*0.5/(times[i]+1.0))
        Int_D_cnt_err.append(np.sqrt(A_cnt)*0.5/(times[i]+1.0))
        
    plt.errorbar(times[1:],Int_D_cnt[1:], yerr=Int_D_cnt_err[1:],fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(times[1:],Int_D_puls[1:], yerr=Int_D_puls_err[1:],fmt='go--', color='b', ecolor='b', label='$R_{ZX}$')
    plt.legend(loc=4)
    plt.xlim(-1, 41.0)
    plt.ylim(-0.0, 0.205)
    plt.grid()
    plt.show()
    plt.savefig('19q_z_p_mit_acc_err.png')
    
    for i in range(1,20):
    #plt.title('T')
        plt.plot(times,Z_i[i],'go-', color='k', label='Ideal Trotter')
        #plt.plot(times,Z_i_cnt[i],'go-',color='r')#, label='2-CNOT')
        #plt.plot(times,Z_i_puls[i],'go-', color='b')#, label='$R_{ZX}$')
        plt.errorbar(times,Z_i_cnt[i],yerr=Z_i_cnt_error[i],fmt='go-',color='r', label='2-CNOT')
        plt.errorbar(times,Z_i_puls[i],yerr=Z_i_puls_error[i],fmt='go-', color='b', label='$R_{ZX}$')
        plt.ylim(-1.4,1.2)
        plt.legend()
        plt.grid()
        plt.show()
        plt.savefig('19q_z_p_mit'+str(i)+'.png')

def site_12_zpi_plot_wo_mit():
    times=np.linspace(0,39,40)
    f = open("12q_zlist_trotter.DAT", "r")
    Z_i = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_Trotter=0
    for site in range(1,13):
        Z_pi_Trotter+=((-1)**(site))*np.array(Z_i[site])
        
    f = open("12q_zlist_cnot_no_em.DAT", "r")
    Z_i_cnt_nomit = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_cnt_nomit[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_cnt_nomit=0
    for site in range(1,13):
        Z_pi_cnt_nomit+=((-1)**(site))*np.array(Z_i_cnt_nomit[site])
        
    f = open("12q_zlist_puls_no_em.DAT", "r")
    Z_i_puls_nomit = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_puls_nomit[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_puls_nomit=0
    for site in range(1,13):
        Z_pi_puls_nomit+=((-1)**(site))*np.array(Z_i_puls_nomit[site])
        
    f = open("12q_zlist_cnot_no_em_error.DAT", "r")
    Z_i_cnt_nomit_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_cnt_nomit_error[j]=num_list
        num_string= f.readline()
        j=j+1
        
    f = open("12q_zlist_puls_no_em_error.DAT", "r")
    Z_i_puls_nomit_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_puls_nomit_error[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_cnt_nomit_error=0
    Z_pi_puls_nomit_error=0
    for site in range(1,13):
        Z_pi_cnt_nomit_error+=(np.array(Z_i_cnt_nomit_error[site])**2)
        Z_pi_puls_nomit_error+=(np.array(Z_i_puls_nomit_error[site])**2)
    Z_pi_cnt_nomit_error=np.sqrt(Z_pi_cnt_nomit_error)/12
    Z_pi_puls_nomit_error=np.sqrt(Z_pi_puls_nomit_error)/12
    
    plt.plot(np.array(times[:]),Z_pi_Trotter/12,'go-',c='k', label='Ideal Trotter')
    #plt.plot(time,Z_pi_cnt_nomit/12,'go--',c='r', label='Two Cnot gate')
    #plt.plot(time,Z_pi_cnot,c='r')
    plt.errorbar(np.array(times[:]),Z_pi_cnt_nomit/12, yerr=Z_pi_cnt_nomit_error,fmt='go-',color='r', ecolor='r')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(np.array(times[:]),Z_pi_puls_nomit/12, yerr=Z_pi_puls_nomit_error,fmt='go--', color='b', ecolor='b')
    plt.legend()
    plt.xlim(-1,41)
    plt.ylim(-1.15, 0.9)
    plt.grid()
    plt.show()
    plt.savefig('12q_z_pi_wo_mit.png')
    
    Int_D_puls=[]
    Int_D_cnt=[]
    for i,j in enumerate(times):
        #print(i)
        D_puls=0
        D_cnt=0
        for k in range(1,13):
            D_puls+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_puls_nomit[k][:i+1]))**2
            D_cnt+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_cnt_nomit[k][:i+1]))**2
        D_puls=D_puls/12
        D_cnt=D_cnt/12
        if j == 0:
            Int_D_puls.append(0)
            Int_D_cnt.append(0)
        else:
            Int_D_puls.append(g(D_puls,0,j)/j)
            Int_D_cnt.append(g(D_cnt,0,j)/j)
            
    u_cnt=0
    u_puls=0
    for k in range(1,13):
        u_puls+=4*((np.array(Z_i_puls_nomit_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_puls_nomit_error[k])*np.array(Z_i_puls_nomit[k]))**2)
        u_cnt+=4*((np.array(Z_i_cnt_nomit_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_cnt_nomit_error[k])*np.array(Z_i_cnt_nomit[k]))**2)
    u_puls=np.sqrt(u_puls)/12
    u_cnt=np.sqrt(u_cnt)/12
    
    Int_D_puls_err=[0]
    Int_D_cnt_err=[0]
    A_puls=0
    A_cnt=0
    for i in range(len(times)-1):
        A_puls+=(u_puls[i]**2+u_puls[i+1]**2)
        A_cnt+=(u_cnt[i]**2+u_cnt[i+1]**2)
        Int_D_puls_err.append(np.sqrt(A_puls)*0.5/(times[i]+1.0))
        Int_D_cnt_err.append(np.sqrt(A_cnt)*0.5/(times[i]+1.0))
        
    plt.errorbar(np.array(times[1:]),Int_D_cnt[1:], yerr=Int_D_cnt_err[1:],fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(np.array(times[1:]),Int_D_puls[1:], yerr=Int_D_puls_err[1:],fmt='go--', color='b', ecolor='b', label='$R_{ZX}$')
    plt.legend(loc=4)
    
    plt.xlim(-1,41)
    plt.ylim(-0.01,0.28)
    plt.grid()
    plt.show()
    plt.savefig('19q_z_acc_err.png')
    
def site_19_zpi_plot_wo_mit():
    times=np.linspace(0,39,40)
    f = open("19q_zlist_cnot_no_em.DAT", "r")
    Z_i_cnt_nomit = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_cnt_nomit[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_cnt_nomit=0
    for site in range(1,19):
        Z_pi_cnt_nomit+=((-1)**(site))*np.array(Z_i_cnt_nomit[site])
        
    f = open("19q_zlist_puls_no_em.DAT", "r")
    Z_i_puls_nomit = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_puls_nomit[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_puls_nomit=0
    for site in range(1,19):
        Z_pi_puls_nomit+=((-1)**(site))*np.array(Z_i_puls_nomit[site])
        
    f = open("19_zlist_trotter.DAT", "r")
    Z_i = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_Trotter=0
    for site in range(1,20):
        Z_pi_Trotter+=((-1)**(site))*np.array(Z_i[site])
        
    f = open("19q_zlist_cnot_no_em_error.DAT", "r")
    Z_i_cnt_nomit_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_cnt_nomit_error[j]=num_list
        num_string= f.readline()
        j=j+1
        
    f = open("19q_zlist_puls_no_em_error.DAT", "r")
    Z_i_puls_nomit_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))
            
        Z_i_puls_nomit_error[j]=num_list
        num_string= f.readline()
        j=j+1
        
    Z_pi_cnt_nomit_error=0
    Z_pi_puls_nomit_error=0
    for site in range(1,20):
        Z_pi_cnt_nomit_error+=np.array(Z_i_cnt_nomit_error[site])**2
        Z_pi_puls_nomit_error+=np.array(Z_i_puls_nomit_error[site])**2
    Z_pi_cnt_nomit_error=np.sqrt(Z_pi_cnt_nomit_error)/19
    Z_pi_puls_nomit_error=np.sqrt(Z_pi_puls_nomit_error)/19
    
    plt.plot(np.array(times[:]),-Z_pi_Trotter/19,'go-',c='k', label='Ideal Trotter')
    #plt.plot(time,Z_pi_cnt_nomit/12,'go--',c='r', label='Two Cnot gate')
    #plt.plot(time,Z_pi_cnot,c='r')
    plt.errorbar(np.array(times[:]),-Z_pi_cnt_nomit/19, yerr=Z_pi_cnt_nomit_error,fmt='go-',color='r', ecolor='r')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(np.array(times[:]),-Z_pi_puls_nomit/19, yerr=Z_pi_puls_nomit_error,fmt='go--', color='b', ecolor='b')
    plt.legend()
    plt.ylim(-1.15, 0.9)
    plt.xlim(-1.0, 41)
    plt.grid()
    plt.show()
    plt.savefig('19q_z_pi_wo_mit.png')
    
    Int_D_puls=[]
    Int_D_cnt=[]
    for i,j in enumerate(times):
        #print(i)
        D_puls=0
        D_cnt=0
        for k in range(1,20):
            D_puls+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_puls_nomit[k][:i+1]))**2
            D_cnt+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_cnt_nomit[k][:i+1]))**2
        D_puls=D_puls/19
        D_cnt=D_cnt/19
        if j == 0:
            Int_D_puls.append(0)
            Int_D_cnt.append(0)
        else:
            Int_D_puls.append(g(D_puls,0,j)/j)
            Int_D_cnt.append(g(D_cnt,0,j)/j)
            
    u_cnt=0
    u_puls=0
    for k in range(1,20):
        u_puls+=4*((np.array(Z_i_puls_nomit_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_puls_nomit_error[k])*np.array(Z_i_puls_nomit[k]))**2)
        u_cnt+=4*((np.array(Z_i_cnt_nomit_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_cnt_nomit_error[k])*np.array(Z_i_cnt_nomit[k]))**2)
    u_puls=np.sqrt(u_puls)/19
    u_cnt=np.sqrt(u_cnt)/19
    
    Int_D_puls_err=[0]
    Int_D_cnt_err=[0]
    A_puls=0
    A_cnt=0
    for i in range(len(times)-1):
        A_puls+=(u_puls[i]**2+u_puls[i+1]**2)
        A_cnt+=(u_cnt[i]**2+u_cnt[i+1]**2)
        Int_D_puls_err.append(np.sqrt(A_puls)*0.5/(times[i]+1.0))
        Int_D_cnt_err.append(np.sqrt(A_cnt)*0.5/(times[i]+1.0))
        
    #plt.plot(time,Int_D_cnt,'go-',c='r', label='Two Cnot gate')
    #plt.plot(time,Int_D_puls,'go-',c='b', label='Pulse')
    #plt.plot(time,Z_pi_cnot,c='r')
    plt.errorbar(np.array(times[1:]),Int_D_cnt[1:], yerr=Int_D_cnt_err[1:],fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(np.array(times[1:]),Int_D_puls[1:], yerr=Int_D_puls_err[1:],fmt='go--', color='b', ecolor='b', label='$R_{ZX}$')
    plt.legend()
    plt.xlim(-1.0, 41)
    plt.ylim(-0.01,0.28)
    plt.grid()
    plt.show()
    plt.savefig('19q_z_acc_err.png')
 
def loschmidt_echo_plot():
    times=np.linspace(0,39,40)
    f = open("12q_loschmidt.DAT", "r")
    time=[]
    Ed_Z_pi=[]
    Z_pi_cnot=[]
    Z_pi_pulse=[]
    Z_pi_err_cnot=[]
    Z_pi_err_pulse=[]
    #print(f.readline())
    num_string = f.readline()
    while num_string != '':
        num = num_string.split()
        time.append(float(num[0]))
        Ed_Z_pi.append(float(num[1]))
        Z_pi_cnot.append(float(num[2]))
        Z_pi_pulse.append(float(num[3]))
        Z_pi_err_cnot.append(float(num[4]))
        Z_pi_err_pulse.append(float(num[5]))
        num_string = f.readline()
        
    plt.figure(figsize=(8, 6), dpi=80)
    plt.plot(times,Ed_Z_pi,'go-',c='k', label='Ideal Trotter')
    #plt.plot(time,Z_pi_cnot,c='r')
    plt.errorbar(times,Z_pi_cnot, yerr=Z_pi_err_cnot,fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(times,Z_pi_pulse, yerr=Z_pi_err_cnot,fmt='go--', color='b', ecolor='b', label='$R_{ZX}$')
    plt.legend()
    #plt.ylim(0.97, 1.0)
    plt.legend(fontsize = 20)
    #plt.ylim(0.97, 1.0)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.grid()
    plt.show()
    plt.savefig('loschmidt_echo_plot.png')
    
def cy_chaotic():
    f = open("cy_chaotic.DAT", "r")
    time=[]
    Trotter_cy=[]
    cy_qpu=[]
    err_bar=[]
    print(f.readline())
    num_string = f.readline()
    while num_string != '':
        num = num_string.split()
        time.append(float(num[0]))
        Trotter_cy.append(float(num[1]))
        cy_qpu.append(float(num[2]))
        err_bar.append(float(num[3]))
        num_string = f.readline()
        
    time=np.linspace(0,4.8,30)
    plt.plot(time,Trotter_cy,'go-',c='k')#, label='Ideal Trotter')
    #plt.plot(time,Z_pi_cnot,c='r')
    #plt.errorbar(times,Z_cy_qpu, yerr=err_bar,fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(time,cy_qpu, yerr=err_bar,fmt='go--', color='b', ecolor='b')#, label='$R_{ZX}$')
    #plt.legend()
    #plt.ylim(0.97, 1.0)
    #plt.legend(fontsize = 20)
    #plt.ylim(0.97, 1.0)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.grid()
    plt.show()
    plt.savefig('cy_chaotic.png')
    
def cy_qmbs():
    f = open("cy_qmbs.DAT", "r")
    time=[]
    Trotter_cy=[]
    cy_qpu=[]
    err_bar=[]
    print(f.readline())
    num_string = f.readline()
    while num_string != '':
        num = num_string.split()
        time.append(float(num[0]))
        Trotter_cy.append(float(num[1]))
        cy_qpu.append(float(num[2]))
        err_bar.append(float(num[3]))
        num_string = f.readline()
        
    plt.plot(time,Trotter_cy,'go-',c='k', label='Ideal Trotter')
    #plt.plot(time,Z_pi_cnot,c='r')
    #plt.errorbar(times,Z_cy_qpu, yerr=err_bar,fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(time,cy_qpu, yerr=err_bar,fmt='go--', color='b', ecolor='b', label='$R_{ZX}$')
    #plt.legend()
    #plt.ylim(0.97, 1.0)
    #plt.legend(fontsize = 20)
    #plt.ylim(0.97, 1.0)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.grid()
    plt.show()
    plt.savefig('cy_qmbs.png')

if __name__ == "__main__":
    draw_2cnot_pulse().savefig('2cnot.png')
    draw_Rzx_pulse().savefig('Rzx.png')
    duration_plot()
    site_12_zpi_plot_mit()
    site_12_zpi_plot_mit_accumulate_err_m_mit_zi()
    site_19_zpi_plot_mit()
    site_19_zpi_plot_mit_accumulate_err_m_mit_zi()
    site_12_zpi_plot_wo_mit()
    site_19_zpi_plot_wo_mit()
    loschmidt_echo_plot()
    cy_chaotic()
    cy_qmbs()
    