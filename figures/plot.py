import numpy as np
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16})  # enlarge matplotlib fonts

# Import qubit states Zero (|0>) and One (|1>), and Pauli operators (X, Y, Z)
import math


# Suppress warnings
import warnings
warnings.filterwarnings('ignore')

from qiskit import QuantumCircuit, QuantumRegister, IBMQ, execute, transpile
from qiskit.providers.aer import QasmSimulator
from qiskit.tools.monitor import job_monitor
from qiskit.circuit import Parameter

# Import state tomography modules
from qiskit.ignis.verification.tomography import state_tomography_circuits, StateTomographyFitter

from qiskit.ignis.verification.tomography import process_tomography_circuits, ProcessTomographyFitter

from qiskit.quantum_info import state_fidelity

import math
import numpy as np
from scipy.special import erf

# Qiskit Pulse imports
import qiskit.pulse
from qiskit import pulse
from qiskit.pulse import library

from qiskit import *
from qiskit.pulse import Play, Schedule, DriveChannel, ControlChannel, Waveform, ShiftPhase
from qiskit.pulse.library import drag, GaussianSquare, Drag

import qiskit
import qiskit.quantum_info as qi


from qiskit.ignis.mitigation.measurement import complete_meas_cal, CompleteMeasFitter

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from qiskit.visualization.pulse_v2.stylesheet import IQXStandard
from qiskit import IBMQ

def g(d,ei,ef):
    """
    integration is written by myself with the Simpson's rule and 3/8 Simpson's rule. Unlike Scipy's, this code can directly deal
with array with initial point and final point.
    # d: the array before integrated
    # ei: start point
    # ef: ending point
    """
    N=d.shape[0]-1
    h=(ef-ei)/N
    return ig(d,N+1,h)

def ig(d,N,h):
    #using the Simpson's rule
    """
    The integration is written by myself with the Simpson's rule and 3/8 Simpson's rule. Unlike Scipy's, this code can directly deal
with array with initial point and final point.
    """
    if N == 1:
        return 0
    elif N==0:
        return 0
    elif N == 2:
        return (d[0]+d[1])*h/2
    elif N == 3:
        return (d[0]+4*d[1]+d[2])*h/3
    elif N == 4:
        return (d[0]+3*d[1]+3*d[2]+d[3])*3*h/8
    elif (N+1)%2 == 0:
        return ((d[N-1]+d[0]+sum(d[1::2])*4+sum(d[2:N-1:2])*2))*h/3
    elif (N+1)%2 == 1:
        return (sum(d[1:N-3:2])*4+sum(d[2:N-5:2])*2+d[0]+d[N-4])*h/3+ig(d[-4:],4,h)


#12_site_zpi
def site_12_zpi_plot_mit():
    times=np.linspace(0,39,40)
    f = open("12q_zlist_trotter.dat", "r")
    Z_i = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_Trotter=0
    for site in range(1,13):
        Z_pi_Trotter+=((-1)**(site))*np.array(Z_i[site])

    f = open("12q_zlist_puls_zne_pauli_twilling_dd_error.dat", "r")
    Z_i_puls_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_puls_error[j]=num_list
        num_string= f.readline()
        j=j+1

    f = open("12q_zlist_cnot_zne_pauli_twilling_error.dat", "r")
    Z_i_cnt_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_cnt_error[j]=num_list
        num_string= f.readline()
        j=j+1
    # Error propagation
    Z_pi_cnt_error=0
    Z_pi_puls_error=0
    for site in range(1,13):
        Z_pi_cnt_error+=(np.array(Z_i_cnt_error[site])**2)
        Z_pi_puls_error+=(np.array(Z_i_puls_error[site])**2)
    Z_pi_cnt_error=np.sqrt(Z_pi_cnt_error)/12
    Z_pi_puls_error=np.sqrt(Z_pi_puls_error)/12

    f = open("12q_zpi_zne.dat", "r")
    time=[]
    Z_pi_Trotter=[]
    Z_pi_cnt=[]
    Z_pi_puls=[]
    Z_pi_cnt_error=[]
    Z_pi_puls_error=[]
    print(f.readline())
    num_string = f.readline()
    while num_string != '':
        num = num_string.split()
        time.append(float(num[0]))
        Z_pi_Trotter.append(float(num[1]))
        Z_pi_cnt.append(float(num[2]))
        Z_pi_puls.append(float(num[3]))
        Z_pi_cnt_error.append(float(num[4]))
        Z_pi_puls_error.append(float(num[5]))
        num_string = f.readline()

    plt.plot(times,Z_pi_Trotter,'go-',c='k', label='Ideal Trotter')
    #plt.plot(time,Z_pi_Trotter/12,'go--',c='g', label='Trotter Simulation')
    #plt.plot(time,Z_pi_cnot,c='r')
    plt.errorbar(times,Z_pi_cnt, yerr=Z_pi_cnt_error,fmt='go-',color='r', ecolor='r')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(times,Z_pi_puls, yerr=Z_pi_puls_error,fmt='go--', color='b', ecolor='b')
    plt.legend(loc=4)
    plt.ylim(-1.15, 0.9)
    plt.xlim(-1, 41.0)
    plt.grid()
    plt.show()
    plt.savefig('12q_z_pi_mit.png')

def site_12_zpi_plot_mit_accumulate_err_m_mit_zi():
    times=np.linspace(0,39,40)
    f = open("12q_zlist_cnot_zne_pauli_twilling_dd.dat", "r")
    Z_i_cnt = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_cnt[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_cnt=0
    for site in range(1,13):
        Z_pi_cnt+=((-1)**(site))*np.array(Z_i_cnt[site])

    f = open("12q_zlist_puls_zne_pauli_twilling_dd.dat", "r")
    Z_i_puls = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_puls[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_puls=0
    for site in range(1,13):
        Z_pi_puls+=((-1)**(site))*np.array(Z_i_puls[site])

    f = open("12q_zlist_trotter.dat", "r")
    Z_i = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_Trotter=0
    for site in range(1,13):
        Z_pi_Trotter+=((-1)**(site))*np.array(Z_i[site])

    f = open("12q_zlist_puls_zne_pauli_twilling_dd_error.dat", "r")
    Z_i_puls_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_puls_error[j]=num_list
        num_string= f.readline()
        j=j+1

    f = open("12q_zlist_cnot_zne_pauli_twilling_dd_error.dat", "r")
    Z_i_cnt_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_cnt_error[j]=num_list
        num_string= f.readline()
        j=j+1

    Int_D_puls=[]
    Int_D_cnt=[]
    for i,j in enumerate(times):
        #print(i)
        D_puls=0
        D_cnt=0
        for k in range(1,13):
            D_puls+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_puls[k][:i+1]))**2
            D_cnt+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_cnt[k][:i+1]))**2
        D_puls=D_puls/12
        D_cnt=D_cnt/12
        if j == 0:
            Int_D_puls.append(0)
            Int_D_cnt.append(0)
        else:
            Int_D_puls.append(g(D_puls,0,j)/j)
            Int_D_cnt.append(g(D_cnt,0,j)/j)

    u_cnt=0
    u_puls=0
    for k in range(1,13):
        u_puls+=4*((np.array(Z_i_puls_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_puls_error[k])*np.array(Z_i_puls[k]))**2)
        u_cnt+=4*((np.array(Z_i_cnt_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_cnt_error[k])*np.array(Z_i_cnt[k]))**2)
    u_puls=np.sqrt(u_puls)/12
    u_cnt=np.sqrt(u_cnt)/12

    Int_D_puls_err=[0]
    Int_D_cnt_err=[0]
    A_puls=0
    A_cnt=0
    for i in range(len(times)-1):
        A_puls+=(u_puls[i]**2+u_puls[i+1]**2)
        A_cnt+=(u_cnt[i]**2+u_cnt[i+1]**2)
        Int_D_puls_err.append(np.sqrt(A_puls)*0.5/(times[i]+1.0))
        Int_D_cnt_err.append(np.sqrt(A_cnt)*0.5/(times[i]+1.0))

    plt.errorbar(times, Int_D_cnt,yerr=Int_D_cnt_err,fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(times,Int_D_puls, yerr=Int_D_puls_err,fmt='go--', color='b', ecolor='b', label='$R_{ZX}$')
    plt.legend(loc=4)
    plt.ylim(-0.0, 0.205)
    plt.xlim(-1, 41)
    plt.grid()
    plt.show()
    plt.savefig('12q_z_p_mit_acc_err.png')


    for i in range(1,13):
        #plt.title('Qubit'+ str(qubits_list[i-1]))
        plt.plot(times,Z_i[i],'go-', color='k', label='Ideal Trotter')
        plt.errorbar(times,Z_i_cnt[i],yerr=Z_i_cnt_error[i],fmt='go-',color='r', label='2-CNOT')
        plt.errorbar(times,Z_i_puls[i],yerr=Z_i_puls_error[i],fmt='go-', color='b', label='$R_{ZX}$')

        plt.ylim(-1.4,1.2)
        plt.legend()
        plt.grid()
        plt.show()
        plt.savefig('12q_z_p_mit'+str(i)+'.png')


def site_19_zpi_plot_mit():
    times=np.linspace(0,39,40)
    f = open("19_zlist_trotter.dat", "r")
    Z_i_Trotter = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_Trotter[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_Trotter=0
    for site in range(1,20):
        Z_pi_Trotter+=((-1)**(site+1))*np.array(Z_i_Trotter[site])

    f = open("19q_zlist_puls_zne_pauli_twilling_dd_error.dat", "r")
    Z_i_puls_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_puls_error[j]=num_list
        num_string= f.readline()
        j=j+1

    f = open("19q_zlist_cnot_zne_pauli_twilling_error.dat", "r")
    Z_i_cnt_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_cnt_error[j]=num_list
        num_string= f.readline()
        j=j+1

    # Error propagation
    Z_pi_cnt_error=0
    Z_pi_puls_error=0
    for site in range(1,20):
        Z_pi_cnt_error+=np.array(Z_i_cnt_error[site])**2
        Z_pi_puls_error+=np.array(Z_i_puls_error[site])**2
    Z_pi_cnt_error=np.sqrt(Z_pi_cnt_error)/19
    Z_pi_puls_error=np.sqrt(Z_pi_puls_error)/19

    f = open("19q_zpi_zne.dat", "r")
    time=[]
    Ed_Z_pi=[]
    Z_pi_cnot=[]
    Z_pi_pulse=[]
    Z_pi_cnt_error=[]
    Z_pi_puls_error=[]
    print(f.readline())
    num_string = f.readline()
    while num_string != '':
        num = num_string.split()
        time.append(float(num[0]))
        Ed_Z_pi.append(float(num[1]))
        Z_pi_cnot.append(float(num[2]))
        Z_pi_pulse.append(float(num[3]))
        Z_pi_cnt_error.append(float(num[4]))
        Z_pi_puls_error.append(float(num[5]))
        num_string = f.readline()

    plt.plot(times,Ed_Z_pi,'go-',c='k', label='Exact')
    #plt.plot(time,Z_pi_Trotter/19,'go--',c='g', label='Trotter Simulation')
    #plt.plot(time,Z_pi_cnot,c='r')
    plt.errorbar(times,Z_pi_cnot, yerr=Z_pi_cnt_error,fmt='go-',color='r', ecolor='r', label='Standard')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(times,Z_pi_pulse, yerr=Z_pi_puls_error,fmt='go--', color='b', ecolor='b', label='Pulse')
    plt.legend(loc=4)
    plt.ylim(-1.15, 0.9)
    plt.xlim(-1, 41)
    plt.xlabel(r'$Vt$')
    plt.ylabel(r'$Z_{\pi}/L$')
    plt.tight_layout()
    plt.grid()
    plt.show()
    plt.savefig('19q_z_pi_mit.png')

def site_19_zpi_plot_mit_accumulate_err_m_mit_zi():
    times=np.linspace(0,39,40)
    f = open("19q_zlist_cnot_zne_pauli_twilling_dd.dat", "r")
    Z_i_cnt = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_cnt[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_cnt=0
    for site in range(1,20):
        Z_pi_cnt+=((-1)**(site))*np.array(Z_i_cnt[site])

    f = open("19_zlist_puls_zne_pauli_twilling_dd.dat", "r")
    Z_i_puls = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_puls[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_puls=0
    for site in range(1,20):
        Z_pi_puls+=((-1)**(site))*np.array(Z_i_puls[site])

    f = open("19_zlist_trotter.dat", "r")
    Z_i = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_Trotter=0
    for site in range(1,19):
        Z_pi_Trotter+=((-1)**(site))*np.array(Z_i[site])

    f = open("19q_zlist_puls_zne_pauli_twilling_dd_error.dat", "r")
    Z_i_puls_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_puls_error[j]=num_list
        num_string= f.readline()
        j=j+1

    f = open("19q_zlist_cnot_zne_pauli_twilling_dd_error.dat", "r")
    Z_i_cnt_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_cnt_error[j]=num_list
        num_string= f.readline()
        j=j+1

    Int_D_puls=[]
    Int_D_cnt=[]
    for i,j in enumerate(times):
        #print(i)
        D_puls=0
        D_cnt=0
        for k in range(1,20):
            D_puls+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_puls[k][:i+1]))**2
            D_cnt+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_cnt[k][:i+1]))**2
        D_puls=D_puls/19
        D_cnt=D_cnt/19
        if j == 0:
            Int_D_puls.append(0)
            Int_D_cnt.append(0)
        else:
            Int_D_puls.append(g(D_puls,0,j)/j)
            Int_D_cnt.append(g(D_cnt,0,j)/j)

    u_cnt=0
    u_puls=0
    for k in range(1,20):
        u_puls+=4*((np.array(Z_i_puls_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_puls_error[k])*np.array(Z_i_puls[k]))**2)
        u_cnt+=4*((np.array(Z_i_cnt_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_cnt_error[k])*np.array(Z_i_cnt[k]))**2)
    u_puls=np.sqrt(u_puls)/19
    u_cnt=np.sqrt(u_cnt)/19

    Int_D_puls_err=[0]
    Int_D_cnt_err=[0]
    A_puls=0
    A_cnt=0
    for i in range(len(times)-1):
        A_puls+=(u_puls[i]**2+u_puls[i+1]**2)
        A_cnt+=(u_cnt[i]**2+u_cnt[i+1]**2)
        Int_D_puls_err.append(np.sqrt(A_puls)*0.5/(times[i]+1.0))
        Int_D_cnt_err.append(np.sqrt(A_cnt)*0.5/(times[i]+1.0))

    plt.errorbar(times[1:],Int_D_cnt[1:], yerr=Int_D_cnt_err[1:],fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(times[1:],Int_D_puls[1:], yerr=Int_D_puls_err[1:],fmt='go--', color='b', ecolor='b', label='$R_{ZX}$')
    plt.legend(loc=4)
    plt.xlim(-1, 41.0)
    plt.ylim(-0.0, 0.205)
    plt.grid()
    plt.show()
    plt.savefig('19q_z_p_mit_acc_err.png')

    for i in range(1,20):
    #plt.title('T')
        plt.plot(times,Z_i[i],'go-', color='k', label='Ideal Trotter')
        #plt.plot(times,Z_i_cnt[i],'go-',color='r')#, label='2-CNOT')
        #plt.plot(times,Z_i_puls[i],'go-', color='b')#, label='$R_{ZX}$')
        plt.errorbar(times,Z_i_cnt[i],yerr=Z_i_cnt_error[i],fmt='go-',color='r', label='2-CNOT')
        plt.errorbar(times,Z_i_puls[i],yerr=Z_i_puls_error[i],fmt='go-', color='b', label='$R_{ZX}$')
        plt.ylim(-1.4,1.2)
        plt.legend()
        plt.grid()
        plt.show()
        plt.savefig('19q_z_p_mit'+str(i)+'.png')

def site_12_zpi_plot_wo_mit():
    times=np.linspace(0,39,40)
    f = open("12q_zlist_trotter.dat", "r")
    Z_i = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_Trotter=0
    for site in range(1,13):
        Z_pi_Trotter+=((-1)**(site))*np.array(Z_i[site])

    f = open("12q_zlist_cnot_no_em.dat", "r")
    Z_i_cnt_nomit = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_cnt_nomit[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_cnt_nomit=0
    for site in range(1,13):
        Z_pi_cnt_nomit+=((-1)**(site))*np.array(Z_i_cnt_nomit[site])

    f = open("12q_zlist_puls_no_em.dat", "r")
    Z_i_puls_nomit = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_puls_nomit[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_puls_nomit=0
    for site in range(1,13):
        Z_pi_puls_nomit+=((-1)**(site))*np.array(Z_i_puls_nomit[site])

    f = open("12q_zlist_cnot_no_em_error.dat", "r")
    Z_i_cnt_nomit_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_cnt_nomit_error[j]=num_list
        num_string= f.readline()
        j=j+1

    f = open("12q_zlist_puls_no_em_error.dat", "r")
    Z_i_puls_nomit_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_puls_nomit_error[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_cnt_nomit_error=0
    Z_pi_puls_nomit_error=0
    for site in range(1,13):
        Z_pi_cnt_nomit_error+=(np.array(Z_i_cnt_nomit_error[site])**2)
        Z_pi_puls_nomit_error+=(np.array(Z_i_puls_nomit_error[site])**2)
    Z_pi_cnt_nomit_error=np.sqrt(Z_pi_cnt_nomit_error)/12
    Z_pi_puls_nomit_error=np.sqrt(Z_pi_puls_nomit_error)/12

    plt.plot(np.array(times[:]),Z_pi_Trotter/12,'go-',c='k', label='Ideal Trotter')
    #plt.plot(time,Z_pi_cnt_nomit/12,'go--',c='r', label='Two Cnot gate')
    #plt.plot(time,Z_pi_cnot,c='r')
    plt.errorbar(np.array(times[:]),Z_pi_cnt_nomit/12, yerr=Z_pi_cnt_nomit_error,fmt='go-',color='r', ecolor='r')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(np.array(times[:]),Z_pi_puls_nomit/12, yerr=Z_pi_puls_nomit_error,fmt='go--', color='b', ecolor='b')
    plt.legend()
    plt.xlim(-1,41)
    plt.ylim(-1.15, 0.9)
    plt.grid()
    plt.show()
    plt.savefig('12q_z_pi_wo_mit.png')

    Int_D_puls=[]
    Int_D_cnt=[]
    for i,j in enumerate(times):
        #print(i)
        D_puls=0
        D_cnt=0
        for k in range(1,13):
            D_puls+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_puls_nomit[k][:i+1]))**2
            D_cnt+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_cnt_nomit[k][:i+1]))**2
        D_puls=D_puls/12
        D_cnt=D_cnt/12
        if j == 0:
            Int_D_puls.append(0)
            Int_D_cnt.append(0)
        else:
            Int_D_puls.append(g(D_puls,0,j)/j)
            Int_D_cnt.append(g(D_cnt,0,j)/j)

    u_cnt=0
    u_puls=0
    for k in range(1,13):
        u_puls+=4*((np.array(Z_i_puls_nomit_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_puls_nomit_error[k])*np.array(Z_i_puls_nomit[k]))**2)
        u_cnt+=4*((np.array(Z_i_cnt_nomit_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_cnt_nomit_error[k])*np.array(Z_i_cnt_nomit[k]))**2)
    u_puls=np.sqrt(u_puls)/12
    u_cnt=np.sqrt(u_cnt)/12

    Int_D_puls_err=[0]
    Int_D_cnt_err=[0]
    A_puls=0
    A_cnt=0
    for i in range(len(times)-1):
        A_puls+=(u_puls[i]**2+u_puls[i+1]**2)
        A_cnt+=(u_cnt[i]**2+u_cnt[i+1]**2)
        Int_D_puls_err.append(np.sqrt(A_puls)*0.5/(times[i]+1.0))
        Int_D_cnt_err.append(np.sqrt(A_cnt)*0.5/(times[i]+1.0))

    plt.errorbar(np.array(times[1:]),Int_D_cnt[1:], yerr=Int_D_cnt_err[1:],fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(np.array(times[1:]),Int_D_puls[1:], yerr=Int_D_puls_err[1:],fmt='go--', color='b', ecolor='b', label='$R_{ZX}$')
    plt.legend(loc=4)

    plt.xlim(-1,41)
    plt.ylim(-0.01,0.28)
    plt.grid()
    plt.show()
    plt.savefig('19q_z_acc_err.png')

def site_19_zpi_plot_wo_mit():
    times=np.linspace(0,39,40)
    f = open("19q_zlist_cnot_no_em.dat", "r")
    Z_i_cnt_nomit = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_cnt_nomit[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_cnt_nomit=0
    for site in range(1,19):
        Z_pi_cnt_nomit+=((-1)**(site))*np.array(Z_i_cnt_nomit[site])

    f = open("19q_zlist_puls_no_em.dat", "r")
    Z_i_puls_nomit = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_puls_nomit[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_puls_nomit=0
    for site in range(1,19):
        Z_pi_puls_nomit+=((-1)**(site))*np.array(Z_i_puls_nomit[site])

    f = open("19_zlist_trotter.dat", "r")
    Z_i = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_Trotter=0
    for site in range(1,20):
        Z_pi_Trotter+=((-1)**(site))*np.array(Z_i[site])

    f = open("19q_zlist_cnot_no_em_error.dat", "r")
    Z_i_cnt_nomit_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_cnt_nomit_error[j]=num_list
        num_string= f.readline()
        j=j+1

    f = open("19q_zlist_puls_no_em_error.dat", "r")
    Z_i_puls_nomit_error = {}
    num_string = f.readline()
    j=1
    while num_string != '':
        num = num_string.split()
        num_list=[]
        for strs in num:
            num_list.append(float(strs))

        Z_i_puls_nomit_error[j]=num_list
        num_string= f.readline()
        j=j+1

    Z_pi_cnt_nomit_error=0
    Z_pi_puls_nomit_error=0
    for site in range(1,20):
        Z_pi_cnt_nomit_error+=np.array(Z_i_cnt_nomit_error[site])**2
        Z_pi_puls_nomit_error+=np.array(Z_i_puls_nomit_error[site])**2
    Z_pi_cnt_nomit_error=np.sqrt(Z_pi_cnt_nomit_error)/19
    Z_pi_puls_nomit_error=np.sqrt(Z_pi_puls_nomit_error)/19

    plt.plot(np.array(times[:]),-Z_pi_Trotter/19,'go-',c='k', label='Ideal Trotter')
    #plt.plot(time,Z_pi_cnt_nomit/12,'go--',c='r', label='Two Cnot gate')
    #plt.plot(time,Z_pi_cnot,c='r')
    plt.errorbar(np.array(times[:]),-Z_pi_cnt_nomit/19, yerr=Z_pi_cnt_nomit_error,fmt='go-',color='r', ecolor='r')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(np.array(times[:]),-Z_pi_puls_nomit/19, yerr=Z_pi_puls_nomit_error,fmt='go--', color='b', ecolor='b')
    plt.legend()
    plt.ylim(-1.15, 0.9)
    plt.xlim(-1.0, 41)
    plt.grid()
    plt.show()
    plt.savefig('19q_z_pi_wo_mit.png')

    Int_D_puls=[]
    Int_D_cnt=[]
    for i,j in enumerate(times):
        #print(i)
        D_puls=0
        D_cnt=0
        for k in range(1,20):
            D_puls+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_puls_nomit[k][:i+1]))**2
            D_cnt+=np.abs(np.array(Z_i[k][:i+1])-np.array(Z_i_cnt_nomit[k][:i+1]))**2
        D_puls=D_puls/19
        D_cnt=D_cnt/19
        if j == 0:
            Int_D_puls.append(0)
            Int_D_cnt.append(0)
        else:
            Int_D_puls.append(g(D_puls,0,j)/j)
            Int_D_cnt.append(g(D_cnt,0,j)/j)

    u_cnt=0
    u_puls=0
    for k in range(1,20):
        u_puls+=4*((np.array(Z_i_puls_nomit_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_puls_nomit_error[k])*np.array(Z_i_puls_nomit[k]))**2)
        u_cnt+=4*((np.array(Z_i_cnt_nomit_error[k])*np.array(Z_i[k]))**2)+2*((np.array(Z_i_cnt_nomit_error[k])*np.array(Z_i_cnt_nomit[k]))**2)
    u_puls=np.sqrt(u_puls)/19
    u_cnt=np.sqrt(u_cnt)/19

    Int_D_puls_err=[0]
    Int_D_cnt_err=[0]
    A_puls=0
    A_cnt=0
    for i in range(len(times)-1):
        A_puls+=(u_puls[i]**2+u_puls[i+1]**2)
        A_cnt+=(u_cnt[i]**2+u_cnt[i+1]**2)
        Int_D_puls_err.append(np.sqrt(A_puls)*0.5/(times[i]+1.0))
        Int_D_cnt_err.append(np.sqrt(A_cnt)*0.5/(times[i]+1.0))

    #plt.plot(time,Int_D_cnt,'go-',c='r', label='Two Cnot gate')
    #plt.plot(time,Int_D_puls,'go-',c='b', label='Pulse')
    #plt.plot(time,Z_pi_cnot,c='r')
    plt.errorbar(np.array(times[1:]),Int_D_cnt[1:], yerr=Int_D_cnt_err[1:],fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(np.array(times[1:]),Int_D_puls[1:], yerr=Int_D_puls_err[1:],fmt='go--', color='b', ecolor='b', label='$R_{ZX}$')
    plt.legend()
    plt.xlim(-1.0, 41)
    plt.ylim(-0.01,0.28)
    plt.grid()
    plt.show()
    plt.savefig('19q_z_acc_err.png')

def loschmidt_echo_plot():
    times=np.linspace(0,39,40)
    f = open("12q_loschmidt.dat", "r")
    time=[]
    Ed_Z_pi=[]
    Z_pi_cnot=[]
    Z_pi_pulse=[]
    Z_pi_err_cnot=[]
    Z_pi_err_pulse=[]
    #print(f.readline())
    num_string = f.readline()
    while num_string != '':
        num = num_string.split()
        time.append(float(num[0]))
        Ed_Z_pi.append(float(num[1]))
        Z_pi_cnot.append(float(num[2]))
        Z_pi_pulse.append(float(num[3]))
        Z_pi_err_cnot.append(float(num[4]))
        Z_pi_err_pulse.append(float(num[5]))
        num_string = f.readline()

    plt.figure(figsize=(8, 6), dpi=80)
    plt.plot(times,Ed_Z_pi,'go-',c='k', label='Ideal Trotter')
    #plt.plot(time,Z_pi_cnot,c='r')
    plt.errorbar(times,Z_pi_cnot, yerr=Z_pi_err_cnot,fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(times,Z_pi_pulse, yerr=Z_pi_err_cnot,fmt='go--', color='b', ecolor='b', label='$R_{ZX}$')
    plt.legend()
    #plt.ylim(0.97, 1.0)
    plt.legend(fontsize = 20)
    #plt.ylim(0.97, 1.0)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.grid()
    plt.show()
    plt.savefig('loschmidt_echo_plot.png')

def cy_chaotic():
    f = open("cy_chaotic.dat", "r")
    time=[]
    Trotter_cy=[]
    cy_qpu=[]
    err_bar=[]
    print(f.readline())
    num_string = f.readline()
    while num_string != '':
        num = num_string.split()
        time.append(float(num[0]))
        Trotter_cy.append(float(num[1]))
        cy_qpu.append(float(num[2]))
        err_bar.append(float(num[3]))
        num_string = f.readline()

    time=np.linspace(0,4.8,30)
    plt.figure()
    plt.plot(time,Trotter_cy,'go-',c='k', label='Exact')
    #plt.plot(time,Z_pi_cnot,c='r')
    #plt.errorbar(times,Z_cy_qpu, yerr=err_bar,fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(time,cy_qpu, yerr=err_bar,fmt='go--', color='b', ecolor='b', label='Device')
    plt.legend()
    #plt.ylim(0.97, 1.0)
    #plt.legend(fontsize = 20)
    #plt.ylim(0.97, 1.0)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.xlabel(r'$Vt$')
    plt.ylabel(r'$\left|\langle Y_{\pi}(t)Y_{\pi}(0) \rangle\right|$')
    plt.grid()
    plt.tight_layout()
    plt.show()
    plt.savefig('cy_chaotic.png')

def cy_qmbs():
    f = open("cy_qmbs.dat", "r")
    time=[]
    Trotter_cy=[]
    cy_qpu=[]
    err_bar=[]
    print(f.readline())
    num_string = f.readline()
    while num_string != '':
        num = num_string.split()
        time.append(float(num[0]))
        Trotter_cy.append(float(num[1]))
        cy_qpu.append(float(num[2]))
        err_bar.append(float(num[3]))
        num_string = f.readline()

    plt.plot(time,Trotter_cy,'go-',c='k', label='Ideal Trotter')
    #plt.plot(time,Z_pi_cnot,c='r')
    #plt.errorbar(times,Z_cy_qpu, yerr=err_bar,fmt='go-',color='r', ecolor='r', label='2-CNOT')
    #plt.plot(time,Z_pi_pulse,c='b')
    plt.errorbar(time,cy_qpu, yerr=err_bar,fmt='go--', color='b', ecolor='b', label='$R_{ZX}$')
    #plt.legend()
    #plt.ylim(0.97, 1.0)
    #plt.legend(fontsize = 20)
    #plt.ylim(0.97, 1.0)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.grid()
    plt.show()
    plt.savefig('cy_qmbs.png')

if __name__ == "__main__":
    # site_12_zpi_plot_mit()
    # site_12_zpi_plot_mit_accumulate_err_m_mit_zi()
    # site_19_zpi_plot_mit()
    # site_19_zpi_plot_mit_accumulate_err_m_mit_zi()
    # site_12_zpi_plot_wo_mit()
    # site_19_zpi_plot_wo_mit()
    # loschmidt_echo_plot()
    cy_chaotic()
    # cy_qmbs()

