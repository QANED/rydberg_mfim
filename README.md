[![Paper](https://img.shields.io/badge/paper-arXiv%3A2203.08291-B31B1B)](https://arxiv.org/abs/2203.08291)

# Error-Mitigated Simulation of Quantum Many-Body Scars on Quantum Computers with Pulse-Level Control

I-Chi Chen, Benjamin Burdick, [Yongxin Yao](https://www.ameslab.gov/directory/yongxin-yao), [Peter P. Orth](https://faculty.sites.iastate.edu/porth), [Thomas Iadecola](https://cm.physics.iastate.edu/people/thomas-iadecola).

### Abstract
Quantum many-body scars are an intriguing dynamical regime in which quantum systems exhibit coherent dynamics and long-range correlations when prepared in certain initial states. We use this combination of coherence and many-body correlations to benchmark the performance of present-day quantum computing devices by using them to simulate the dynamics of an antiferromagnetic initial state in mixed-field Ising chains of up to 19 sites. In addition to calculating the dynamics of local observables, we also calculate the Loschmidt echo and a nontrivial connected correlation function that witnesses long- range many-body correlations in the scarred dynamics. We find coherent dynamics to persist over up to 40 Trotter steps even  in the presence of various sources of error. To obtain these results, we leverage a variety of error mitigation techniques including noise tailoring, zero-noise extrapolation, dynamical decoupling, and physically motivated postselection of measurement results. Crucially, we also find that using pulse-level control to implement the Ising interaction yields a substantial improvement over the standard CNOT-based compilation of this interaction. Our results demonstrate the power of   error mitigation techniques and pulse-level control to probe many-body coherence and correlation effects on present-day quantum hardware.

### Description
This repository includes information, code, scripts, and data to generate the figures in the paper.

### Requirements
* [qiskit](https://github.com/Qiskit)
* [mitiq](https://github.com/unitaryfund/mitiq)

### Figures
All the codes and data used to create the figures in the paper are found in the **figures** folder. They are all written in Python, and use the matplotlib library.


### Data Generation
The main files to perform the algorithm detailed in the paper include ipython notebooks in folder **code-ipython** and restructured python codes in folder **code-python**. Generated data can be found in the **results** folder. 

#### Files in code-ipython
The python code in notebook version is in the **code-ipython** folder.

* ```Correlation_cirs_pulse_ZNE``` is used to create the job for the correlation circuits.
* ```Correlation_cirs_pulse_ZNE_Real_Extrap_err_bar.ipynb``` is used to retrieve the job and do analysis for the result. 
* ```Trotter_code_larger_dev.ipynb``` is used to directly execute trotter circuits with mitigation techniques. 
* ```Correlation_cir_test_no_ancilla_expectation_value_Y_4cirs.ipynb``` and ```Trotter_test.ipynb``` are used to all kind of circuits on simulator. ```Test_RZZ_gate_error_rate_qpt_pulse_folding_slope_w_readout_mit_casablanca_1.ipynb``` is used for the quantum process tomography.


#### Contents in code-python
* ```rmfim``` folder contains main python codes, which can be install by typing `make`. A few important files:
    * ```emlib.py``` contains the main circuit implementations.
    * ```calc.py``` dispatcher for readout calibration and Trotter dynamics simulations with different implementations.
    * ```analysis.py``` contains the local magnetization and Loschmidt analysis scripts.
    * ```database.py``` handles the raw calculation results.
    * ```conf.py``` read problem specific parameters.
* ```bin``` folder contains the main executables which link to the ```rmfim``` files. Some important ones include: 
    * `run_dynamics.py` perform Trotter dynamics simulations with two-CNOT implementation or scaled Rzx implementation. Type `run_dynamics.py -h` to see full options and explanations.
    * `randombenchmark.py` performs random bench calculations.
    * `run_analysis.py` performs local magnetization analysis or Loschmidt echo analysis at various levels of implementation and error mitigation. Type `run_analysis.py -h` for details.

#### Contents in results folder
* ```12_sites/dynamics/``` contains the dynamics simulations results further split to different submission dates. A sample slurm job submission script is located at `12_sites/dynamics/20211015_2/job.slurm`. In the subfolder `20211015_2` also contains plotting scripts for local magnetization (`check_z_cnot_pulse_mitiq.py`) and Loschmidt echo (`check_loschmidt.py`) analysis.
* ```12_sites/correlation_function/2022-03-09-Chaotic``` contains the results for connected correlation function calculations in the chaotic region with scaled Rzx implementation. The calculation script is `Correlation_cirs_pulse_ZNE.py`, and the analysis script is `corr_analysis.py`.
* ```12_sites/correlation_function/20220223_QMBS/``` contains the similar contents as the one above but for quantum many-body scar region.
* ```19_sites/dynamics``` contains the dynamics simulation results for 19-site model.

#### Specific steps to reproduce 12q results
* change to directory `rydberg_mfim/code-python` and install the code `rmfim` by typing `make` or explicitly `pip install -e . --user`.
* change to directory `rydberg_mfim/results/12_sites/dynamics/rerun` 	   
    * submit job for readout calibration by typing `../../../../code-python/bin/run_dynamics.py -m 0`. The qc job ids are stored in file `cal_circ.jobid`.
    * submit job for Trotter simulations with two-CNOT implementation by typing `../../../../code-python/bin/run_dynamics.py -m 1 -j 1`. The qc job ids are stored in file `TrotterCNOTExecutor_jobids.dat`.
    * submit job for Trotter simulations with scaled Rzx implementation by typing `../../../../code-python/bin/run_dynamics.py -m 2 -j 1`. The qc job ids are stored in file `TrotterExecutor_random_rzz_jobids.dat`.
    * With all the jobs done, type `../../../../code-python/bin/run_analysis.py -t 0 -m 0` for local magnetization analysis of the results using two-CNOT version. The local magnetix=zation <Z_i> results will be saved as dataset `cnot_roc_ps` in the hdf5 file `analysis_result.h5`, with dimensions [ntimesteps, nsites, npaulitwirl, nnoiselevels]. Use `h5ls -r analysis_result.h5` to confirm.
    * Type `../../../../code-python/bin/run_analysis.py -t 0 -m 1` for local magnetization analysis of the results using scaled Rzx version. The results will be saved as dataset `pulse_roc_ps` in the hdf5 file `analysis_result.h5`.
    * To check the staggered moment, type `python check_stagger_z_zne.py`.
    * The problem specific settings are located in file `config.json`.

### Support
This material is based upon work supported by the National Science Foundation under Grant No. DMR-2038010. Calculations for spin models with more than seven sites on quantum hardware, and part of the associated analyses by Y. Yao, are supported by the U.S. Department of Energy, Office of Science, National Quantum Information Science Research Centers, Co-design Center for Quantum Advantage (C2QA) under contract number DE-SC0012704.

<img width="100px" src="https://www.nsf.gov/images/logos/NSF_4-Color_bitmap_Logo.png">

<img width="500px" src="https://science.osti.gov/-/media/_/images/about/resources/logos/png/high-res/RGB_Color-Seal_Green-Mark_SC_Horizontal.png">
